Analyse du Modèle Conceptuel de Données (MCD)

Le graphique représente un modèle conceptuel de données (MCD) pour une base de données relative à des éléments de YouTube (chaînes, vidéos, playlists).
Entités et Associations

    search
        Associations :
            recherché par country
            recherché par element (channel, video, playlist)

    element (channel, video, playlist)
        Associations :
            organisé dans playlist
            associé à keywords
            classifié par topic
            dispose de Propriétés

    playlist
        Associations :
            organisé dans element

    country
        Associations :
            recherché par search

    keywords
        Associations :
            associé à element

    topic
        Associations :
            classifié par element

    Propriétés
        Associations :
            dispose de element

Analyse des Relations

    search est lié à country et element par une relation de type "recherché par".
    element peut être un channel, une video ou une playlist. Cet élément est central et possède plusieurs relations :
        Il est organisé dans une playlist.
        Il est associé à des keywords.
        Il est classifié par des topic.
        Il dispose de Propriétés spécifiques.

Diagrammes UML

    Entité search :
        Attributs : id, type
        Relations : recherché par country et element

    Entité element :
        Attributs : id, title, description, type (can be channel, video, playlist)
        Relations : organisé dans playlist, associé à keywords, classifié par topic, dispose de Propriétés

    Entité playlist :
        Attributs : id, title, description
        Relations : organisé dans element

    Entité country :
        Attributs : id, name
        Relations : recherché par search

    Entité keywords :
        Attributs : id, word
        Relations : associé à element

    Entité topic :
        Attributs : id, name
        Relations : classifié par element

    Entité Propriétés :
        Attributs : id, name, value
        Relations : dispose de element