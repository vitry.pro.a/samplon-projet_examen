# **Analyse des Besoins**  

1. **Identification des cas d'utilisation** : 
Présentez les scénarios dans lesquels votre projet sera utilisé.  

En ce qui concerne les cas d'utilisation, je vais les définir par des contextes où le besoin peut survenir.

- Situation 1:
Lors d'une discussion (entre amis, pour un hobby, lors de la création d'une entreprise) à propos du lancement d'une chaine youtube pour contruire la visibité public d'une thèmatique donnée (passion, organisme d'un hobby, travail).

On se renseigne sur les possibités qu'offre une chaine youtube (visibité, rémunaration, réseau via les abonnées), mais aussi sur les contraintes qui en résulte (publication d'éléments jusqu'alors privé, rythme de publication via nombre de vidéo mensuel, etc...)

- Situation 2:
Démystifier un discours par une personnalité publique "youtubeur", qui avance que tous les "youbuteurs" peuvent être des "youtubeurs à succès".

Vérifier les datas des "youtubeurs qui ont réussi pour obtenir les points qui ont contribué à leur réussite.

- Situation 3:
Un "youtubeur" qui souhaite obtenir des informations afin de comprendre les différents points d'amélioration qui pourront mis en oeuvre au travers des statistiques de publications.


2. **Définition des fonctionnalités principales** : 
Listez les fonctionnalités essentielles que votre projet doit inclure pour répondre aux besoins identifiés.  

Le projet doit contenir:
Pour les informations fixes:
- La liste des chaines youtube d'un pays (ici la France)
- La liste des vidéos des chaques chaines
- La liste des playlists de chaques chaines
- Les caractéristiques d'une chaine:
    - Le nombre d'abonnée
    - Le nombre de vidéo
    - le nombre de vue de la chaine
    - La description
    - La catégorie de référrencement
- Les caractéristiques d'une vidéo:
    - Les vues
    - Les likes
    - Les dislikes
    - Le nombre de commentaires
    - La description
    - La catégorie de référrencement

Pour les informations variables, les informations ci-dessus en fonction de la date. (une vérification de la fesabilité techniques est nécessaire au cours du projet)