# **Importation des Données**


1. **Préparation des données** : 
Décrivez les étapes de nettoyage et de transformation des données avant l'importation.  

2. **Processus d'importation** : 
Expliquez les outils et scripts utilisés pour importer les données dans la base de données.  

---

# **Importation des Données (Version ChatGPT)**  

## 1. Préparation de l'environnement   

1.1. Choix des outils et technologies  

Pour ce projet, nous avons utilisé Python en raison de ses puissantes bibliothèques 
pour la manipulation des données (pandas, requests) et pour l'interaction avec les 
bases de données (mysql-connector-python).
La base de données cible est MySQL, choisie pour sa robustesse et sa capacité à gérer 
des volumes de données importants.

1.2. Installation et configuration

Les outils et bibliothèques nécessaires ont été installés via pip :

```sh
pip install pandas requests mysql-connector-python
```

Une base de données MySQL a été configurée localement pour stocker les données importées.

## 2. Extraction des données

2.1. Connexion aux API et aux sources de données

Nous avons créé un projet sur la Google Developers Console pour obtenir une clé API 
nécessaire pour accéder aux API YouTube Data et YouTube Analytics.
Le script connexion_api_Youtube.py établit la connexion à l'API YouTube.

2.2. Requêtes et collecte des données

Le script extraction_api_Youtube.py envoie des requêtes à l'API YouTube et collecte 
les données en format JSON.
Exemple de code pour l'extraction des données :

```python
import requests

api_key = 'YOUR_API_KEY'
url = f'https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=UC_x5XG1OV2P6uZZ5FSM9Ttw&key={api_key}'
response = requests.get(url)
data = response.json()
```

## 3. Transformation des données

3.1. Nettoyage et filtrage

Les données collectées sont nettoyées et filtrées pour éliminer les doublons et les 
valeurs manquantes.
Nous utilisons pandas pour traiter et structurer les données :

```python
import pandas as pd

df = pd.json_normalize(data['items'])
df_clean = df.dropna()
```

3.2. Structuration des données

Les données brutes sont transformées en un format structuré pour être insérées dans 
la base de données.
Le script save_data_as_file.py sauvegarde les données transformées dans un fichier Excel 
pour une revue et une vérification manuelles.

## 4. Chargement des données

4.1. Modèle de données

Les données structurées sont mappées aux tables de la base de données MySQL.
Nous avons défini les tables pour stocker les données des vidéos, des chaînes, des 
playlists, etc.

4.2. Insertion des données

Le script migration_api_database.py insère les données dans la base de données.
Exemple de code pour l'insertion des données :

```python
import mysql.connector

conn = mysql.connector.connect(user='root', password='password', host='127.0.0.1', database='youtube_data')
cursor = conn.cursor()


for index, row in df_clean.iterrows():
    cursor.execute("INSERT INTO videos (videoId, title, description) VALUES (%s, %s, %s)", (row['id.videoId'], row['snippet.title'], row['snippet.description']))

conn.commit()
cursor.close()
conn.close()
```

## 5. Automatisation du processus

5.1. Scripts et outils d'automatisation

Les scripts sont automatisés pour s'exécuter périodiquement et collecter les données de 
manière continue.
Nous utilisons des tâches planifiées (cron jobs) pour automatiser l'exécution des scripts.

5.2. Surveillance et gestion des erreurs

Des mécanismes de journalisation sont mis en place pour surveiller le processus et gérer les 
erreurs.
Les erreurs sont enregistrées et des notifications sont envoyées pour une intervention rapide.

## 6. Exemple de flux de travail

6.1. Diagramme du flux de travail

Le diagramme ci-dessous illustre le flux de travail complet de l'importation des données, de l'extraction à partir des API YouTube jusqu'au chargement dans la base de données MySQL :

![Diagramme](/Projet/Rapport/Ressources/Architecture_API_BDD.png)

    Étape 1 : Connexion à l'API YouTube via connexion_api_Youtube.py.
    Étape 2 : Extraction des données via extraction_api_Youtube.py.
    Étape 3 : Sauvegarde des données dans un fichier Excel via save_data_as_file.py.
    Étape 4 : Migration des données vers la base de données via migration_api_database.py.
    Étape 5 : Insertion des données dans la base de données MySQL via extraction_database_mysql.py
    et connexion_database_mysql.py.