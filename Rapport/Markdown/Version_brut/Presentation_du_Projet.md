# **Présentation du Projet**  


1. **Description du projet** :  
Détaillez en quoi consiste le projet, son but principal, et les problèmes qu'il cherche à résoudre.  

le projet consiste à pouvoir analyser les informations des chaines youtube d'un pays, sur la base du nombre des abonnées, du nombre de vidéo publié, des vues par chaine et par vidéo.
L'analyse sera simple, constaté les corrélations entre les dimensions pris pour l'analyse.

L'objectif est vérifié s'il existe une conclusion vérifiable concernant la popularité des chaines (nombre d'abonnée).


2. **Enjeux et objectifs** : 
Expliquez pourquoi le projet est important et quels sont les objectifs spécifiques que vous visez.  

Le projet présente un intéret pour le débat dans la société. 

Il est commun de conclure sur la base d'information qui nous sont seulement accessible, peu importe leurs pertinances, leurs biais, même si ce ne sont que des corrélations. Nous avons tendance à les prendres pour acquis et en faire une généralité.

Actuellement, la plateforme youtube compte des millions d'utilisateurs et donc des millions de "youtubeur" potentiels qui du jour au lendemain pourront se lancer à la quête des "bénéfices" qu'offres le statut de "youtubeur".
Cependant, beaucoup d'avis différents existent concernant le lancement de "youtubeur" dont des vidéos de youtubeur eux-même.
Mais pas de consencus, clair.

Sur la base d'informations collecter sur la plateforme youtube, l'objectif est mettre en évidence les informations puis de les mettre en relation, afin d'en afficher un éventuel "pattern" qui sur les éléments qui permet aux "youtubeurs" de ce démarquer.

Ici pour des raisons techniques, on se contentera de collecter les données publiques de youtube, de les afficher, et de mettre en relations exclusivement ces informations.
