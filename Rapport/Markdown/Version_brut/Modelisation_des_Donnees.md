# **Modélisation des Données**


## 1. **Création du modèle de données** :  
Décrivez le processus de création du modèle, les concepts et les entités principales.  

Pour la création du modèle de données, il est important de faire le point sur les informations issues de l'API.

### Traitement des resultats:  

En effet, sur les 21 types de résultat possible et donc 21 types de collection au format JSON, nous utiliserons uniquement:
search, channel, video, playlist, playlistItem.  
Le reste ne dispose pas d'information necessaire poru ce projet.  
Ensuite, sur les 5 JSON obtenus, nous utiliserons seulement la méthode `list`.  
Pour terminer la mise au point des datas, nous avons besoin de sélectionner les blocs à envoyer dans la requète, afin de pouvoir sélectionner les champs à conserver un-par-un.  

Un format JSON se structure de la manière suivante:
```JSON
{
    "kind": "youtube#searchResult",
    "etag": etag,
    "id": {
        "kind": string,
        "videoId": string,
        "channelId": string,
        "playlistId": string
    },
    "snippet": {
        "publishedAt": datetime,
        "channelId": string,
        "title": string,
        "description": string,
        "thumbnails": {
        (key): {
                "url": string,
                "width": unsigned integer,
                "height": unsigned integer
            }
        },
        "channelTitle": string,
        "liveBroadcastContent": string
    }
}
```
Nous avons les clefs du JSON qui définit les blocs d'informations (`part`) et leurs valeurs (les informations dont nous avons besoins).
Dans cet exemple, il s'agit du JSON pour la requète search avec le `part` initialiser sur snippet.

Voici les informations dont nous auront besoin pour ce projet:

#### Collection résultat - Search:
"kind": "youtube#searchResult"  
"videoId": string  
"channelId": string  
"playlistId": string  
"publishedAt": datetime  
"title": string  
"description": string  

#### Collection résultat - Channel:
"kind": "youtube#channel"  
"id": string  
"country": string  
"likes": string    
"uploads": string  
"viewCount": unsigned long   
"subscriberCount": unsigned long  
"hiddenSubscriberCount": boolean  
"videoCount": unsigned long  
"topicIds": [string]   

#### Collection résultat - Playlist:  
"kind": "youtube#playlist"   
"id": string   
"publishedAt": datetime  
"channelId": string   
"title": string  
"description": string  

#### Collection résultat - PlaylistItem:  
"kind": "youtube#playlistItem"  
"id": string  
"publishedAt": datetime  
"channelId": string  
"playlistId": string  
"videoId": string  

#### Collection résultat - Vidéo:
"kind": "youtube#video"  
"id": string  
"publishedAt": datetime  
"channelId": string   
"title": string   
"description": string  
"tags": [string]  
"duration": string  
"privacyStatus": boolean   
"viewCount": string   
"likeCount": string  
"dislikeCount": string  
"favoriteCount": string  
"commentCount": string   
"relevantTopicIds": [string]  

## 2. **Diagrammes et schémas** :  
Incluez des diagrammes UML pour illustrer le modèle de données.  

### Analyse des resultats:

Le modèle de donnée est le plan de l'architecture de la base de donnée, on procède de la manière suivante pour le contruire:  
    1. Création du MCD modèle conceptuel de donnée  
    2. Transformation du MCD en MLD modèle logique de donnée  

#### MCD modèle conceptuel de donnée

- **l'entité:** elle regroupe l'information statique et durable.
    Par exemple, l’entité channel rassemble toutes les informations communes aux chaines youtube
    Une entité est représentée par un nom commun écrit en majuscules et au singulier.
    Exemples: SEARCH, CHANNEL, VIDEO, PLAYLIST, PLAYLISTITEM.  
- **l'association:** elle matérialise la dynamique du système et donc les relations entre les entités.
    Par exemple, l'entité channel est en relation avec l'entité video.
    L'association (ou relation) est représentée par un verbe d'action ou d'état à l'infinitif.
    Exemples: ASSOCIER, DIFFUSER, CONTIENT, RECHERCHER, etc.

**Graphique de l'association des entités de la recherche:**  
CHANNEL est recherché par PAYS  
VIDEO ou PLAYLISTITEM est recherché par CHANNEl  
PLAYLIST est recherché par VIDEO  

![MCD association entité](/Projet/Rapport/Ressources/MCD-association.png)  

**Graphique de l'association du type de la recherche aux champs JSON:**  
Recherche par type (search-pays, search-channel) aux champs JSON (element)  
Recherche par type (channel-video, channel-playlist, channel-playlistItem) aux champs JSON (element, Propriétes channel, Propriétes Video)  

![MCD association type aux champs](/Projet/Rapport/Ressources/MCD-relation.png)  

**Graphique de l'association des entités:**  

**Search** est recherché par **Element**.  
**Element** est organisé dans une **Playlist**.    
**Search** est recherché par **Country**.  
**Element** est associé à **Keywords**.  
**Element** est classifié par **Topic**.  
**Element** dispose de **Propriétés**.  

![MCD association des entités](/Projet/Rapport/Ressources/MCD-all_entity.png)  


#### MLD modèle logique de donnée

**Search** est recherché par **Element**.  

![MCD search](/Projet/Rapport/Ressources/MCD-search_relation.png)  

**Element** est organisé dans une **Playlist**.   

![MCD playlist](/Projet/Rapport/Ressources/MCD-playlist_relation.png)  

**Search** est recherché par **Country**.  

![MCD country](/Projet/Rapport/Ressources/MCD-country_relation.png)  

**Association réflexive:**
Un graphe non orienté est transformé en une table associative. Cette table associative doit contenir un couple de clés étrangères qui réfèrent à la table des nœuds du graphe.  

**Search** est recherché par **Element**. 

![MCD search table](/Projet/Rapport/Ressources/MCD-search_table.png)  

**Element** est organisé dans une **Playlist**.   

![MCD playlist table](/Projet/Rapport/Ressources/MCD-playlist_table.png)  

**Search** est recherché par **Country**.  

![MCD country table](/Projet/Rapport/Ressources/MCD-country_table.png)  


**Element** est associé à **Keywords**.  

![MCD keywords](/Projet/Rapport/Ressources/MCD-keyword_relation.png)  

![MCD keywords table](/Projet/Rapport/Ressources/MCD-keyword_table.png)  

**Element** est classifié par **Topic**.  

![MCD topic](/Projet/Rapport/Ressources/MCD-topic_relation.png)  

![MCD topic table](/Projet/Rapport/Ressources/MCD-topic_table.png)  

**Element** dispose de **Propriétés**.  

![MCD propriété](/Projet/Rapport/Ressources/MCD-propriete_relation.png)  

![MCD propriété table](/Projet/Rapport/Ressources/MCD-propriete_table.png)  