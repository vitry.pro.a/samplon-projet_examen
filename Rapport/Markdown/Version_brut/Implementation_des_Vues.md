---
output: pdf_document
editor_options: 
  markdown: 
    wrap: 80
---

# **Implémentation des Vues**

1. **Développement des vues** : 

La solution choisi pour mise à disposition des vues est Dash, une librairie permettant de généré des graphiques avec une prise en charge de composant html et d'un serveur intégré.

A l'aide d'un script python, un serveur local mettre à disposition, une ensemble de page html auquel sera affecté, un ou plusieurs composant graphique.

En suivant le guide pour bien débuter sur le site officiel de Dash, on obtient rapidement le code pour avoir une app fonctionnel, constitué d'un titre et d'un texte sur chacune des trois pages de l'application.

1.1. Script principal

En modifiant, la structure et les `callBack`, on obtient cette forme:

```python
import dash
import dash_bootstrap_components as dbc
from dash import Dash, html, dcc
```
Note: Import de la bibliothèque Dash et de ces composants
```python
BS = "https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css"

app = Dash(__name__, use_pages=True, external_stylesheets=[BS, dbc.icons.FONT_AWESOME])
```
Note: Création de l'APP
```python
app.layout = html.Div(children=[
    dbc.Row(children=[
        dbc.Col(children=[
            html.H2('Navigation:', style={"border": "1px black solid", 'textAlign': 'center', 
            'backgroundColor':'#ec7063'}),
            html.Div([
                dcc.Link(f"-[{page['name']}]", href=page["relative_path"], 
                style={'columnCount': 1}) 
                for page in dash.page_registry.values()
                ])
            ]),
```
Note: Composant html, représentant un bloc `Col` droit avec la navigation
```python
        dbc.Col(children=[
            html.H1('Projet: Youtube Statistique', style={"border": "1px black solid", 
            'height':"10vh", 'textAlign': 'center', 'backgroundColor':'#ec7063'})
            ], align='bottom', width={'size': 9}, style={"border": "1px black solid", 
            'backgroundColor':' #f1948a'})
        ], justify = "center", style={'backgroundColor':'#ff3f3f', 'height':"12vh"}),
```
Note: Composant html, représentant un bloc `Col` gauche avec le titre
```python
    dbc.Row(children=[
        dash.page_container
        ], justify = "center", style={"border": "1px black solid"})
    
], style={'backgroundColor': '#e8f8f5'})
```
Note: Composant html, représentant un bloc `Row` le container qui accueillera le body de chaque page
```python
if __name__ == '__main__':
    app.run(debug=True)
```
Note: Démarrage de l'APP


1.2. Script statistique

La partie layout est un peu différente
```python
# Mise en forme de la page
layout = html.Div(children=[
    html.Div([
        html.H1('Comparaison du Top 15 avec 1 chaine au choix', 
          style={'textAlign': 'center'}),
        # df.columns
        dcc.Dropdown(dataframe["element_id"], id='pandas-dropdown', 
        placeholder="Select une chaine Youtube", searchable=False, clearable=False),
        html.Div(id='dd-output-container')
        ]),
    dbc.Row(children=[
        dbc.Col(children=[
            dcc.Graph(figure=fig_1, id='shape-1-output')
            ]),
        dbc.Col(children=[
            dcc.Graph(figure=fig_2, id='shape-2-output')
            ])
        ]),
```
Note: Contient les blocs avec le titre, un dropdown, et l'emplacement pour 6 graphiques

```python
# Initialisation des figures
fig_1 = px.bar(df_1, x=df_1[columns_list[0]], y=df_1[columns_list[5]], 
                                    color='subscriberCount', text_auto='.2s')
fig_1.update_traces(textfont_size=12, textangle=0, textposition="outside", 
                                                            cliponaxis=False)
fig_1.add_shape(type="line", line_color="salmon", line_width=3, 
                opacity=1, line_dash="dot", xref="paper", 
                x0=0, x1=1, y0='shape-1-output', y1='shape-1-output', yref="y")
```
Note: l'initialisation des 6 figures avec un `shape` (forme par dessus la figure), ici la ligne qui représente la chaine choisi
```python
# Initialisation du dataframe
dataframe = show_dataframe(table, bdd_properties_config, mydb, mycursor)
df_1 = graphique_dataframe(table, columns_list[5], bdd_properties_config, mydb, mycursor)
```
Note:  l'initialisation des 6 dataframes pour les figures le dataframe pour la sélection de la chaine
```python
def graphique_dataframe(table: str, column: str, properties_config, db, cursor):
    column_list: list = column_name_in_properties(properties_config, 
    "Projet_Examen", table)
    # Récupération des data dans la BDD
    db["Projet_Examen"].reconnect()
    list_table = select_bdd_order_by_cond(cursor["Projet_Examen"], table, 
    column_list[0], column)
    print("Mise à jour Data:")
    db["Projet_Examen"].close()
    # Mise en forme des datas
    df = pd.DataFrame(list_table, columns=[column_list[0], column])
    df = df.sort_values(by=[column], axis=0, ascending = False)
    df = df.iloc[:15]
    # print(df)
    return df
```
Note: La fonction permet la mise à jour des données sur la page
```python
@callback(
    Output('shape-1-output', 'figure'),
    Input('pandas-dropdown', 'value'))
def update_fig_1(value):
    if value is None:
        raise PreventUpdate
    # Sélection de la chaine courante
    for row in dataframe.itertuples():
        if value == getattr(row, "element_id"):
            y_fig_1 = getattr(row, "subscriberCount")
    # Update Figure
    patched_figure = Patch()
    patched_figure['layout']['shapes'][0].update({
        "y0": y_fig_1, "y1": y_fig_1})
    return patched_figure
```
Note: L'une des 6 fonction `callback` qui permet la mise à jour des graphique en fonction du dropdown


1.3. Script Archives  

La fonction et les callback etant similaire le layout est la seul différence avec l'autre page

```python
layout = html.Div([
    html.Div([
        html.H1('Contenu de la base de donnée', style={'textAlign': 'center'}),
        "Selectionnez une table dans la base de donnée: ",
        dcc.RadioItems(options=list_radio, value='element', id='analytics-input', 
        inline=True, labelStyle={"display": "inline-block", 
        "align-items": "center", 'margin-left':'20px'}),
        html.Div(id='analytics-output')
        ]),
```
Note: la tête de page avec le titre et les selecteurs radio
```python
    html.Br(),
    dash_table.DataTable(
            id='data-table',
            data=dataframe.to_dict('records'), 
            columns=[{"name": i, "id": i} for i in dataframe.columns],
```
Note: le `DashTable` qui affiche le tableau avec en dessous ces paramètres
```python
            page_size=50,
            style_data={
            'whiteSpace': 'normal'
            },
            css=[{
                'selector': '.dash-spreadsheet td div',
                'rule': '''
                    line-height: 15px;
                    max-height: 30px; min-height: 10px; height: 15px;
                    display: block;
                    overflow-y: hidden;
                '''}],
            tooltip_data=[
                {
                    column: {'value': str(value), 'type': 'markdown'}
                    for column, value in row.items()
                } for row in dataframe.to_dict('records')
            ],
            tooltip_duration=None,
            style_table={'overflowX': 'auto'},
            style_cell={'textAlign': 'left'},
            style_cell_conditional=[
                    {'if': {'column_id': 'title'},
                    'width': '15%'},
                    {'if': {'column_id': 'id_element'},
                    'width': '15%'},
                    {'if': {'column_id': 'date'},
                    'width': '15%'}]
        )
], style={"border": "1px black solid", 'width': '92%', 'display': 'inline-block', 
'horizontal-align': 'center', 'margin-right': '3vw', 'margin-left': '3vw', 
'margin-top': '3vw', 'margin-bottom': '3vw'})
```

2. **Technologies et outils** : 
Mentionnez les frameworks, bibliothèques et autres outils que vous avez utilisés.  

En utilisant, `Dash` pour le serveur et la page web, mais aussi `Plotly` pour les graphique, `MySQL` pour la connexion à la base de donnée, `Pandas` pour la manupulation des données via dataframe.
On obtient, un dashboard complet qui permet de mettre à disposition les données de la base de donnée.