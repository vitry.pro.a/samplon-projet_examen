# **Collecte des Données**


1. **Recherche et sélection des sources de données** : 
Décrivez les sources de données que vous avez considérées et pourquoi vous avez choisi celles que vous avez utilisées.  

Lors des recherches de dataset de donnée youtube, j'ai constaté que les datdaset mis à disposition était principalement des extract de youtube, d'un site socialblade ou du résultat de scrapping de page.
Ces informations, ne contenant pas les informations dont j'avais besoin, je me suis tourné vers la source, l'API Youtube Data.

J'ai donc utilisé l'API Youtube pour extraire les datas nécessaire mais aussi un dataset creer sur la base d'un scrapping de site qui répertorier le Top 5000 des chaines youtube francaise.


2. **Méthodologie de collecte des données** : 
Expliquez comment vous avez collecté les données (méthodes, outils, etc.).  

L'API Youtube Data:
Comme le nom l'indique, il s'agit d'une API.  

>    Définition: Une API (application programming interface ou « interface de programmation d'application ») est une interface logicielle   qui permet de « connecter » un logiciel ou un service à un autre logiciel ou service afin d'échanger des données et des fonctionnalités.

En utilisant les services de youtube (modèles de script), il est possible d'extraire les données de l'API.
Ce qui nous donne un corps de requète Http avec un JSON en résultat.

Une fois ce JSON reçu, il faut analyse la collection de résultat, nettoyer des informations inutiles, puis le traiter et l'inserer dans une base de donnée.

L'APi Youtube dispose de plusieurs type de requètes, avec différentes "résultat de recherche" et de leurs méthodes, ainsi que de bloc d'information au sein d'une même requète.
Voici comment se structure une requète:
- L'adresse http: qui est le corps de l'adresse, il permet de savoir quel API ont interroge. (Youtube dispose de 2 API public)
Exemple: "https://www.googleapis.com/youtube/v3/"
- Les "résultats de recherche": qui peut être differente en fonction de ce qu'on cherche.
Toute les méthodes: activity, captions, channelbanners, channel, channelSection, comment, commentThread, i18nLanguage, i18nRegion, member, membershipsLevel, playlistItem, playlist, search, subscription, thumbnail, videoAbuseReportReason, videoCategory, video, watermark, guideCategory
Exemple requète: "https://www.googleapis.com/youtube/v3/search"
- Les méthodes associée: qui peut être list, update, insert, download, delete
Chaque résultat dispose de ses propres méthodes.
- Les blocs d'information peuvent être choisi pour spécifier quel type d'information, on souhaite obtenir dans une collection de résultat. (ici "part")
Exemple requète: "https://youtube.googleapis.com/youtube/v3/channels?part=snippet&part=statistics&part=id&id=UC0XmApkQJH-c29sZnbjbVFA&maxResults=50&key=[YOUR_API_KEY]"
