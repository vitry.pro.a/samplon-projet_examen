# **Introduction**  


1. **Contexte du projet** :  
Expliquez brièvement le domaine d'application et l'importance du projet.  

Le projet est réalisé dans le cadre de la partie 1 de la certification:
RNCP - Expert en infrastructures de données massives.

Il est nécessaire de faire valider les acquis obtenu au cours de la formation avec l'organisme Simplon.

2. **Objectifs du rapport** :  
Décrivez ce que le rapport vise à accomplir et à qui il est destiné.  


On cherche à valider les compétences pour le titre RNCP:

- Programmation de la collecte de données depuis plusieurs sources pour un projet
data

- Développement de la mise à disposition technique des données collectées pour 
un projet data

- Production de visualisations de données
