---
output: pdf_document
editor_options: 
  markdown: 
    wrap: 80
---

6.  **Création de la Base de Données**

    -   **Choix du SGBD** : Décrivez les critères de sélection et le type de SGBD 
    sélectionné.

    -   **Définition de la base de données** : Décrivez la configuration initiale, 
    et expliquez comment vous avez creer la base de données.

    -   **Création des tables** : Décrivez la définition des tables principales, 
    le définition des relations et l'indexation.



# **Création de la Base de Données**  

## **Choix du SGBD**  
Le choix du système de gestion de base de données SGBD se fait en fonction de plusieurs critères:
    -   **Facilité d'utilisation** : Il s'agit de pouvoir travailler avec le système sans avoir à mettre en place une gestion complexe. Dans notre cas, un système de gestion connu (MySQL ou SQLite) sera préférable afin de facilité la mise en place de la Base de donnée.
    -   **Cohérence des données** : Il s'agit de règle appelées *contraite d'intégrité*.  À la fin de chaque transaction, ou plus radicalement après chaque mise à jour, il est nécessaire de contrôler qu'aucune règle d'intégrité n'est violée. MySQL sera préférable à SQLite.
    -   **Sécurité des données** : Pour la sécurité, ici nous n'avons pas de critère car on ne déploira pas la base de donnée sur un serveur mais uniquement en local.
    -   **Performance et efficacité** : Ces termes désignent :
        Le temps de réponse
        L’intégration de vos autres logiciels
        La capacité d’évolution
        Le système de gestion de base de données doit permettre des accès simultanés par plusieurs utilisateurs.
    Pour la performance, on se préocupera de l'intégration avec un script Python qui sera notre language pour la migration dans la base de donnée. Grace à MySQL_connector, qui est une librairie Python permettant de faire la liaison avec MySQL.

## **Définition de la base de données**  
Une fois, le système qui permet de créer et administrer une base de données et sur lequel on peut effectuer des requêtes SQL, choisi.

On s'occupe d'effectuer son installation, via le terminal:
- Etape 1: On met à jour le dépôt des paquets  
```sh
sudo apt-get update
```

- Etape 2: On installe MySQL  
```sh
sudo apt-get install mysql-server
```

- Etape 3: Création de l'admin   
```sh
mysqladmin -u root -h localhost password 'new-password'
```

- Etape 4: Création du fichier init.sql pour créer la base de donnée  

- Etape 5: Exécution du fichier init.sql  
```sh
mysql init.sql
```

- Etape 6: Création d'un utilisateur et des permissions pour la base de donnée  
```sh
CREATE USER '[utilisateur]'@'localhost' IDENTIFIED BY '[mot_de_passe]';
GRANT [privileges] ON database.* TO '[utilisateur]'@'[host]';
```
Les types de privileges:  
    ALL -Donne tous les privilèges sur la base de données
    CREATE - Autorise l'utilisateur à créer des tables
    SELECT - Autorise l'utilisateur à interroger les tables
    INSERT - Autorise l'utilisateur à insérer des données dans la table
    SHOW DATABASES - Autorise l'utilisateur à voir la liste des tables
    USAGE - L'utilisateur n'a aucun privilège
    GRANT OPTION - Autorise l'utilisateur à octroyer des privilèges


## **Création des tables**  
Le fichier de création de base de donnée init.sql contient toutes les requètes SQL  nécessaire.
Chaque table est via la requète "CREATE TABLE" qui lui attribut des champs ainsi que des types aux champs.
Les relations sont creer via la requète "ADD FOREIGN KEY" en référencant les deux tables à lier.

- Etape 1: Création de la base de donnée "Projet_Examen"
```sql
DROP DATABASE IF EXISTS Projet_Examen;

CREATE DATABASE IF NOT EXISTS Projet_Examen;

USE Projet_Examen;
```

- Etape 2: Création de la table "element"
```sql
CREATE TABLE `element` (
  `id_element` varchar(255) PRIMARY KEY NOT NULL,
  `type` ENUM ('channel', 'video', 'playlist', 'pays') NOT NULL,
  `title` varchar(255),
  `publish_date` date,
  `description` text
);
```

- Etape 3: Création de la table "country"
```sql
CREATE TABLE `country` (
  `id_country` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `region_code` varchar(255) UNIQUE NOT NULL,
  `title` varchar(255) NOT NULL
);
```

- Etape 4: Création de la table "search"
```sql
CREATE TABLE `search` (
  `element_id` varchar(255) NOT NULL,
  `element_search_id` varchar(255) NOT NULL,
  `kind` varchar(255) NOT NULL
);
```

- Etape 5: Création de la table "playlist"
```sql
CREATE TABLE `playlist` (
  `playlist_id` varchar(255) NOT NULL,
  `channel_id` varchar(255) NOT NULL,
  `video_id` varchar(255) NOT NULL
);
```

- Etape 6: Création de la table "keywords"
```sql
CREATE TABLE `keywords` (
  `id_keyword` varchar(255) PRIMARY KEY NOT NULL,
  `keyword` varchar(255) NOT NULL
);
```

- Etape 7: Création de la table "element_keywords"
```sql
CREATE TABLE `element_keywords` (
  `element_id` varchar(255) NOT NULL,
  `keyword_id` varchar(255) NOT NULL
);
```

- Etape 8: Création de la table "topics"
```sql
CREATE TABLE `topics` (
  `id_topic` varchar(255) PRIMARY KEY NOT NULL,
  `topic` varchar(255) NOT NULL
);
```

- Etape 9: Création de la table "element_topics"
```sql
CREATE TABLE `element_topics` (
  `element_id` varchar(255) NOT NULL,
  `topic_id` varchar(255) NOT NULL
);
```

- Etape 10: Création de la table "proprety"
```sql
CREATE TABLE `proprety` (
  `element_id` varchar(255) NOT NULL,
  `viewCount` int,
  `likesCount` int,
  `videoCount` int,
  `commentCount` int,
  `duration` int,
  `hiddenSubscridenCount` int,
  `subscriberCount` int,
  `privacyStatus` ENUM ('private', 'public', 'unlisted') NOT NULL
);
```

- Etape 11: Création des index pour la vérication de la cohérence des données
```sql
CREATE UNIQUE INDEX `country_index_0` ON `country` (`id_country`, `region_code`);

CREATE UNIQUE INDEX `search_index_1` ON `search` (`element_id`, `element_search_id`);

CREATE UNIQUE INDEX `playlist_by_channel_index_2` ON `playlist_by_channel` (`playlist_id`, `channel_id`, `video_id`);
```

- Etape 12: Création des clés étrangères pour la relations des tables
```sql
ALTER TABLE `search` ADD FOREIGN KEY (`element_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `search` ADD FOREIGN KEY (`element_search_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `playlist_by_channel` ADD FOREIGN KEY (`channel_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `playlist_by_channel` ADD FOREIGN KEY (`playlist_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `playlist_by_channel` ADD FOREIGN KEY (`video_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `element_topics` ADD FOREIGN KEY (`element_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `element_topics` ADD FOREIGN KEY (`topic_id`) REFERENCES `topics` (`id_topic`);

ALTER TABLE `element_keywords` ADD FOREIGN KEY (`element_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `element_keywords` ADD FOREIGN KEY (`keyword_id`) REFERENCES `keywords` (`id_keyword`);

ALTER TABLE `proprety` ADD FOREIGN KEY (`element_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `element` ADD FOREIGN KEY (`id_element`) REFERENCES `country` (`id_country`);

```