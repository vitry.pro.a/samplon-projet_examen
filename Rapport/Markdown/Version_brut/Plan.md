# plan vite fait:

1. Le projet consiste en quoi ?  
2. Les cas d'utilisation / fonctionnalités  
3. Comment j'ai trouvé les données de base  
4. Comment j'ai fait mon modele de donnée (+ le modele de donnée)  
5. Comment j'ai importé les données dans ma BDD  
6. Comment j'ai mis à dispo les donénes avec un API (+ specification de l'API)  
7. Comment j'ai défini mes vues pour répondre à mes cas d'utilisation.  
8. Comment j'ai coder mes vues en utilisant mes données.  
9. conclusion  
10. remerciments 

# Plan après modifications:  

1. **Introduction**  
    - Contexte du projet   
    - Objectifs du rapport    

2. **Présentation du Projet**  
    - Description du projet  
    - Enjeux et objectifs du projet  

3. **Analyse des Besoins**   
    - Identification des cas d'utilisation  
    - Définition des fonctionnalités principales  

4. **Collecte des Données**  
    - Recherche et sélection des sources de données  
    - Méthodologie de collecte des données  

5. **Modélisation des Données**  
    - Création du modèle de données  
    - Diagrammes et schémas du modèle de données  

6. **Importation des Données**  
    - Préparation des données pour l'importation  
    - Processus d'importation dans la base de données  

7. **Développement de l'API**  
    - Conception de l'API  
    - Spécifications techniques de l'API  

8. **Mise en Place des Vues**  
    - Définition des vues nécessaires pour les cas d'utilisation  
    - Conception des vues (maquettes, wireframes)  

9. **Implémentation des Vues**  
    - Développement des vues avec les données disponibles  
    - Technologies et outils utilisés  

10. **Tests et Validation**  
    - Stratégie de test (unitaires, intégration, etc.)  
    - Résultats des tests et validation des fonctionnalités  

11. **Conclusion**  
    - Résumé des réalisations  
    - Difficultés rencontrées et solutions apportées  
    - Perspectives d'amélioration   

12. **Remerciements**  
    - Remerciements aux personnes et institutions ayant contribué au projet  

13. **Annexes**  
    - Documents complémentaires (code source, schémas détaillés, etc.)      

14. **Bibliographie**  
    - Références et ressources utilisées tout au long du projet  
