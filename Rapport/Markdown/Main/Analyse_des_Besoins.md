---
output: pdf_document
editor_options: 
  markdown: 
    wrap: 80
---

# **Analyse des Besoins**

## 1. Identification des cas d'utilisation  

**Scénarios d'utilisation** 

- Situation 1 :  
Lors d'une discussion (entre amis, pour un hobby, lors de la création d'une entreprise) sur le lancement d'une chaîne YouTube pour construire la visibilité publique d'une thématique donnée (passion, organisme d'un hobby, travail). On se renseigne sur les possibilités qu'offre une chaîne YouTube (visibilité, rémunération, réseau via les abonnés), mais aussi sur les contraintes (publication d'éléments jusqu'alors privés, rythme de publication via le nombre de vidéos mensuelles, etc.).  

- Situation 2 :  
Démystifier un discours par une personnalité publique "youtubeur", qui avance que tous les "youtubeurs" peuvent être des "youtubeurs à succès". Vérifier les données des youtubeurs qui ont réussi pour obtenir les points qui ont contribué à leur réussite.  

- Situation 3 :  
Un youtubeur qui souhaite obtenir des informations pour comprendre les différents points d'amélioration à mettre en œuvre à travers les statistiques de publications.  


## 2. Définition des fonctionnalités principales  

**Fonctionnalités essentielles**  
Pour répondre aux besoins identifiés, le projet doit inclure les fonctionnalités suivantes :  

**Informations fixes**  

    - Liste des chaînes YouTube d'un pays (ici la France)
    - Liste des vidéos de chaque chaîne
    - Liste des playlists de chaque chaîne

Caractéristiques d'une chaîne :  

    - Nombre d'abonnés
    - Nombre de vidéos
    - Nombre de vues de la chaîne
    - Description
    - Catégorie de référencement

Caractéristiques d'une vidéo :  

    - Vues
    - Likes
    - Dislikes
    - Nombre de commentaires
    - Description
    - Catégorie de référencement