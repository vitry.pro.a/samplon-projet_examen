---
output: pdf_document
editor_options: 
  markdown: 
    wrap: 80
---

# **Mise en Place des Vues**

## 1. **Définition des vues** : 

Dans le contexte des bases de données et de l'interface utilisateur, une vue peut avoir plusieurs significations. Voici les principales :

  Vues de Base de Données :
  Une vue dans une base de données est une requête SQL enregistrée. Elle permet de présenter les données d'une ou plusieurs tables sous une forme personnalisée sans modifier les données réelles. Les vues simplifient les requêtes, assurent la sécurité en restreignant l'accès aux données sensibles et facilitent la gestion des données complexes.

  Vues d'Interface Utilisateur :
  Dans le développement d'applications, une vue fait référence à l'interface visible par l'utilisateur. Elle affiche les données récupérées de la base de données sous forme de graphiques, tableaux, formulaires, etc. Les vues sont conçues pour être intuitives et user-friendly.


## 2. **Conception des vues** : 

L'objectif est de créer une interface intuitive et informative pour les utilisateurs, permettant de visualiser les statistiques des chaînes YouTube et d'accéder à l'ensemble des informations sous forme de tableaux. 

Voici les composants clés de cette interface :

![Conception des vues](/home/antony/Projet_Examen/Rapport/Ressources/Vue_conception.png)

Ces graphiques permettront de visualiser les 20 chaînes ayant le plus d'abonnés en fonction de différents critères. Chaque graphique mettra en évidence une chaîne sélectionnée par l'utilisateur pour faciliter la comparaison.  

Les graphiques incluront :  
- Les 20 chaines ayant le plus d'abonnés en fonction du nombre d'abonnés  
- Les 20 chaines ayant le plus d'abonnés en fonction du nombre de vues  
- Les 20 chaines ayant le plus d'abonnés en fonction du nombre de likes  
- Les 20 chaines ayant le plus d'abonnés en fonction du nombre de dislikes  
- Les 20 chaines ayant le plus d'abonnés en fonction du nombre de commentaires  
- Les 20 chaines ayant le plus d'abonnés en fonction du nombre de vidéos  

Sur ces graphiques sera affiché la chaine sélectionner au préalable.

**Tableau d'Informations**

Un tableau affichera toutes les informations disponibles sur les chaînes YouTube, permettant une vue détaillée et exhaustive des données. Les utilisateurs pourront choisir la table de la base de données à afficher, ce qui facilitera l'exploration et l'analyse des données.

**Exemple de Conception des Vues**

Ce diagramme montre la structure générale de la page avec les graphiques en haut et le tableau en bas, permettant une visualisation cohérente et structurée des données.

