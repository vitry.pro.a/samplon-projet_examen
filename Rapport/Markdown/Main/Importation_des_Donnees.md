---
output: pdf_document
editor_options: 
  markdown: 
    wrap: 80
---

# **Importation des Données**  

## 1. Préparation de l'environnement   

1.1. Choix des outils et technologies  

Pour ce projet, nous avons utilisé Python en raison de ses puissantes bibliothèques 
pour la manipulation des données (pandas, requests) et pour l'interaction avec les 
bases de données (mysql-connector-python).
La base de données cible est MySQL, choisie pour sa robustesse et sa capacité à gérer 
des volumes de données importants.

1.2. Installation et configuration

Les outils et bibliothèques nécessaires ont été installés via pip :

```sh
pip install pandas requests mysql-connector-python
```

Une base de données MySQL a été configurée localement pour stocker les données importées.

## 2. Extraction des données

2.1. Connexion aux API et aux sources de données

Nous avons créé un projet sur la Google Developers Console pour obtenir une clé API 
nécessaire pour accéder aux API YouTube Data et YouTube Analytics.
Le script connexion_api_Youtube.py établit la connexion à l'API YouTube.

2.2. Requêtes et collecte des données

Le script extraction_api_Youtube.py envoie des requêtes à l'API YouTube et collecte 
les données en format JSON.
Exemple de code pour l'extraction des données :

```python
import requests

part = "snippet&channelId"
id = "UC_x5XG1OV2P6uZZ5FSM9Ttw"
api_key = 'YOUR_API_KEY'
url = f'https://www.googleapis.com/youtube/v3/search?part={part}={id}&key={api_key}'
response = requests.get(url)
data = response.json()
```
La méhodologie se découpe de cette manière:  
    - Etape 1: Initialisation les options de requètes d'API  
    - Etape 2: Mise en forme de l'url  
    - Etape 3: Utilisation de la bibliothèque `requests`  
    - Etape 4: Récupération du JSON dans une variable  

Pour les conditions à prendre en compte, il est necessaire de pouvoir gérer:  
    - Toutes les options nécessaire à la récupération des datas souhaité  
    - Les differentes requètes: `search`, `channel`, `video`, etc....  
    - Le traitement différentes en fonction du type de JSON obtenu  

Nous obtenons cette fonction:
```python
search_response = request_API_Youtube(options, type_request)
```

```python
def request_API_Youtube(options_argparse, type_function):
    debug_print("-> Lancement de la fonction request API Youtube !", debug_tool)
    """["search_channel", "search_video", "search_playlist", "channel", "video", 
    "playlist", "playlistItem"]"""
    def option_select(dico, argparse):
        for key in vars(argparse):
            dico[key] = getattr(argparse, str(key))
```
Note: Cette partie récupère les options dans un argument parseur en fonction du type de recherche
```python
    # Création de l'API Youtube
    youtube_api_data = connexion_API_Youtube_Data()
```
Note: Cette partie créé la connexion API avec la clé d'API
```python
    if type_function in ["search_channel", "search_video", "search_playlist"]:
        option_select(dico_option_search, options_argparse)
```
Note: Condition pour la recherche du type `search`
```python
        try:
            response_JSON = youtube_api_data.search().list(
                    part =              dico_option_search["part"], 
                    channelId =         dico_option_search["channelId"], 
                    channelType =       dico_option_search["channelType"],  
                    publishedAfter =    dico_option_search["publishedAfter"], 
                    regionCode =        dico_option_search["regionCode"], 
                    type =              dico_option_search["type"], 
                    publishedBefore =   dico_option_search["publishedBefore"],  
                    maxResults =        dico_option_search["maxResults"], 
                    videoType =         dico_option_search["videoType"], 
                    pageToken =         dico_option_search["pageToken"], 
                    q =                 dico_option_search["q"], 
                    order =             dico_option_search["order"]
                ).execute()
```
Note: On insère les arguments contenu dans un dictionnaire pour option
```python
        except errors.HttpError as e:
            print('An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))
            response_JSON = None
```
Note: On gère les erreurs lié au requète http
```python
    elif type_function == "channel":
```
Note: Condition pour un nouveau type de requète, similaire à la partie au-dessus.
```python
return response_JSON
```
Note: Retour du JSON via l'appel de la fonction


## 3. Traitement des données JSON en fonction des types de requète

3.1 Gestion des résultats

Une fois le JSON obtenu, nous avons beaucoup de donnée et un problème.

Cette problèmatique est présenté dans la documentation comme un système de pagination pour les requètes.
Autrement dit, la 1ère requète renvoie seulement 50 résulat et un `token` (clé pour la requète).
Il faut donc itérer sur la même requète jusqu'à ne plus obtenir de `token`.

Pour cela une autre fonction aura pour objectif, d'envoyé de nouvelle requète avec le token, et d'en stoker le résultat.

```python
def youtube_search(parser, options, type_request, nbr_résultat, dico_list_of_result):
    debug_print("-> Lanchement de la fonction youtube_search !", debug_tool)
    # Requète de recherche:
    search_response = request_API_Youtube(options, type_request)
```
Note: Il s'agit d'une fonction récursive qui se rappel elle-même tant que la condition est valide
Les requètes sont bien itérer.
```python
    # Traitement du résultat JSON:
    if search_response != None:
```
Note: Vérification du JSON
```python
        debug_print(f"Lecture des datas de la requète http:", debug_tool)
        # Boucle sur les dimensions du JSON:
        for head in search_response.keys():
            debug_print(f"Dimension du Json: {head}", debug_tool)
```
Note: Itération sur le JSON lui-même
```python
            # Vérification de la pagination:
            if "nextPageToken" in search_response.keys():
                token_tool = True
            else:
                token_tool = False
            # Incrémentation du nombre de recherche à chaque appel de la fonction récusive:
```
Note: Vérification de la présence d'un token
```python
        if head == "pageInfo":
            nbr_résultat["nbr_current"] += int(search_response[head]["resultsPerPage"])
            nbr_résultat["nbr_total"] = int(search_response[head]["totalResults"])
        debug_print('Nombre de résultat en cours: %s / %s' % (nbr_résultat["nbr_current"], 
        nbr_résultat["nbr_total"]), debug_tool)
```
Note: Gestion du nombre de résultat
```python
            # Vérification du header de la requète:
            if head != "items":
                debug_print(f"Key token: {search_response[head]}", debug_tool)
                if token_tool == True and head == "nextPageToken":
                    key_token_page = search_response[head]
                    debug_print(f"Key token pour pagination: {key_token_page}", debug_tool)
                elif token_tool == False:
                    key_token_page = None
                    debug_print(f"Key token pour pagination: {key_token_page}", debug_tool)
                debug_print(None, debug_tool)
```
Note: Récupération de la valeur du token
```python
# Vérification du corps de la requète:
else:
    # Boucle sur les dimensions du corps (items) du JSON:
    for search_result in search_response[head]:
        debug_print(f"-> Insertion {search_result['id']} dans une liste !", debug_tool)
        # Recherche des vidéos:
        if search_result['id']['kind'] == 'youtube#video':
            type_search: str = (search_result['id']['kind']).split('#')[1]
            dico_list_of_result["video"].append([search_result['id']['videoId'],
                                                type_search,
                                                search_result['snippet']['title'],
                                                search_result['snippet']['publishedAt'],
                                                search_result['snippet']['description'],
                                                search_result['snippet']['channelId']])
```
Note: Traitement des informations souhaités, que l'on stocke dans un dictionnaire en fonction du type de recherche
```python
        # Recherche des chaines:
        elif search_result['id']['kind'] == 'youtube#channel':
            type_search: str = (search_result['id']['kind']).split('#')[1]
            dico_list_of_result["channel"].append([search_result['id']['channelId'],
                                                type_search,
                                                search_result['snippet']['title'],
                                                search_result['snippet']['publishedAt'],
                                                search_result['snippet']['description'],
                                                search_result['snippet']['channelId']])
```
Note: Idem, pour `channel`
```python
        # Recherche des playlists:
        elif search_result['id']['kind'] == 'youtube#playlist':
            type_search: str = (search_result['id']['kind']).split('#')[1]
        dico_list_of_result["playlist"].append([search_result['id']['playlistId'],
                                                type_search,
                                                search_result['snippet']['title'],
                                                search_result['snippet']['publishedAt'],
                                                search_result['snippet']['description'],
                                                search_result['snippet']['channelId']])
```
Note: Idem, pour `playlist`
```python
                # Vérification de la fin de la pagination:
                if key_token_page != None:
                    # print(options)
                    parser.set_defaults(pageToken=key_token_page)
                    options = parser.parse_args()
                    # print(options)
                    debug_print(None, debug_tool)
                    dico_list_of_result = youtube_search(parser, options, type_request, 
                                                    nbr_résultat, dico_list_of_result)
```
Note: Condition pour la récusivité en fonction du token
```python
    # Affichage du nombre de résultat obtenus dans la requète:
    debug_print('Nombre de résultat traité: %s / %s' % (nbr_résultat["nbr_current"], 
    nbr_résultat["nbr_total"]), debug_tool)
    return dico_list_of_result
```
Note: Retour de tous les résultats après de multiple requète

3.2 Gestion des options

Une autre fonction indispensable est la récupération des options

```python
def gestion_option(type_of_argument):
    debug_print(f"""-> Lancement de la fonction option arguments - 
                                            {type_of_argument} !""", debug_tool)
    """["search_channel", "search_video", "search_playlist", "channel", "video", 
    "playlist", "playlistItem"]"""
    if type_of_argument == "search_channel":
        arguments = arg_search_channel
    elif type_of_argument == "search_video":
        arguments = arg_search_video
    elif type_of_argument == "search_playlist":
        arguments = arg_search_playlist
    elif type_of_argument == "channel":
        arguments = arg_channel
    elif type_of_argument == "video":
        arguments = arg_video
    elif type_of_argument == "playlist":
        arguments = arg_playlist
    elif type_of_argument == "playlistItem":
        arguments = arg_playlistItem
```
Note: Assignation d'un dictionnaire en fonction du type de recherche
```python
    # Création du parseur pour les arguments:
    parser = ArgumentParser()
    for key in arguments.keys():
        if arguments[key] != None:
            parser.add_argument(f"--{key}", help=f"{key}", default=arguments[key])
        else:
            parser.add_argument(f"--{key}", help=f"{key}")
    args = parser.parse_args()
```
Note: Parseur qui initialise un argument en fonction d'un élément du dictionnaire
```python
    return parser, args
```
Note: Retour du parseur et des options

Comme vu au dessus, la fontion ne s'occupe que d'initialisé les options.
Pour les options, un fichier contenant les dictionnaires a été créer pour stocker toutes les options possible.
Voici un exemple:
```python
dico_option_playlistItem: dict = {
                    "part": None, 
                    "onBehalfOfContentOwner": None, 
                    "pageToken": None, 
                    "playlistId": None, 
                    "videoId": None, 
                    "maxResults": None, 
                    "id": None
                    }
```
Note: Dict pour toutes options de la recherche
```python
arg_playlistItem = {
                    "part": "id,snippet,contentDetails", 
                    "playlistId": '', 
                    "maxResults": 50,
                    "pageToken": None
                    }
```
Note: Dict pour les options spécifiques à la recherche

3.3 Gestions des fonctions

Pour pouvoir, faire fonctionner toutes ces fontions ensemble selon les bonnes conditions.
Une fontion conteneur s'occupe de faire l'initialisation, les conditions, la récupération de donnée, 
et de retourne uniquement les résultats souhaité.

```python
def request_list(path, type_of_request, list_id=None):
```
Note: Récupération de la recherche souhaité
```python
    debug_print(f"""
    -> Lancement de la fonction request_{type_of_request} !""", debug_tool)
    """["search_channel", "search_video", "search_playlist", "channel", "video", 
    "playlist", "playlistItem"]"""
    # Création du conteneur du nombre de résultats pour la requète de recherche:
    nbr_resultat = {"nbr_current": 0, "nbr_total": 0}
    dico_of_result: dict = {}
    # Création des arguments pour la requète de recherche:
    parser, arguments = gestion_option(type_of_request)
```
Note: Fonction des arguments
```python
    if type_of_request in ["search_video", "search_playlist", "channel", 
                            "video", "playlist", "playlistItem"] and list_id != None:
        # Création du conteneur des résultats pour la requète "search_video", 
        # "search_playlist", "channel", "video", "playlist","playlistItem":
        dico_of_result.update({"video": [], "playlist": [], "result": []})
        for id in list_id:
            parser, arguments = option_set_if(parser, arguments, id)
            print(f"Modification {id} argument", arguments)
```
Note: Modification des arguments dans le cas ou l'ID d'une chaine (ou video, etc) est nécéssaire
```python
            # Requète de channel pour les chaines youtube:
            dico_of_result = quotas_of_request(type_of_request, 10, parser, arguments, 
                                                            nbr_resultat, dico_of_result)
```
Note: Gestion des quotas qui itere n fois la requète de la recherche
C'est aussi, cette fonction qui appel la fonction "youtube_search"
```python
        # Insertion et affichage des résultats:
        export(path, dico_of_result, type_of_request)
```
Note: Volet important, l'export de tous résultats dans fichier excel, de tel sortes à sauvegarder les datas de requètes si il y a une erreur.
```python
    elif type_of_request in ["search_channel", 
                            "search_video", "search_playlist"] and list_id == None:
        # Création du conteneur des résultats pour la requète de recherche:
        dico_of_result.update({"channel": [], "video": [], "playlist": []})
        # Requète de recherche pour les chaines youtube vu en France:
        dico_of_result = quotas_of_request(type_of_request, 1, parser, arguments, 
                                                        nbr_resultat, dico_of_result)
        # Insertion et affichage des résultats:
        export(path, dico_of_result, type_of_request)
    return dico_of_result
```
Note: Même chose mais pour des types de quètes différentes.


## 4. Transformation des données

La récupération des données via une API Youtube est quantifié par un système de quotas.
Il a été nécessaire de mettre en place rapidement la récupération de donnée.

Mais l'insertion dans la base de donnée, peut se faire en différent via les fichiers excels générés.
Dans cette optique, la transformation s'est fait de tel sorte, à pouvoir traiter les données venant directement via l'APi pour aussi via l'importation des fichiers excel.

Une fonction par type de recherche est créé, comprenant l'importation (API ou Excel), la transformation des données, et l'insertion par table dans la base de donnée.

```python
def transfert_Search_to_BDD(dataframe: pd.DataFrame = None, shunt: bool = False):
    debug_print("### Start migration Search ###", debug_tool)
    # Appel fonction sans requète http: ---------------------------------------
    if shunt == False:
```
Note: Vérication si on insere via API ou Excel
```python
# {"video": [], "playlist": [], "result": []} or {"channel": [], "video": [], 
# "playlist": []}
        list_channel = []
        list_video = []
        list_playlist = []
        list_result = []
        # Récupération des listes dans un dictionnaire
        path_export = '/home/antony/Projet_Examen/Projet/Ressources/DATA/Export_via_BDD/'
            # Type de requète possible:
        """["search_channel", "search_video", "search_playlist", "channel", 
        "video", "playlist", "playlistItem"]"""
        dico_search = request_list(path_export, "search_channel")
```
Note: Import API
```python
        # --------------------------------------
        columns_search = ["id_element", "kind", "title", "publish_date", 
                            "description", "element_search_id"]
        # --------------------------------------
        # Keys: video, channel, playlist
        for list_type in dico_search.keys():
            if dico_search[list_type] != []:
                if list_type == "channel":
                    list_channel = dico_search[list_type]
                    # Création DataFrame:
                    df_request = pd.DataFrame(list_channel, columns=columns_search)
                if list_type == "video":
                    list_video = dico_search[list_type]
                    # Création DataFrame:
                    df_request = pd.DataFrame(list_video, columns=columns_search)
                if list_type == "playlist":
                    list_playlist = dico_search[list_type]
                    # Création DataFrame:
                    df_request = pd.DataFrame(list_playlist, columns=columns_search)
                if list_type == "result":
                    list_result = dico_search[list_type]
                    # Création DataFrame:
                    df_request = pd.DataFrame(list_result, columns=columns_search)
```
Note: Formatage des données en dataframe
```python
        else:
            # --------------------------------------
            print(f"Le type {list_type} contient une liste vide: ", dico_search[list_type])
```
Note: Si la réponse de la requète est vide
```python
    else:
        df_request = dataframe
    # -------------------------------------------------------------------------
    df_request['publish_date'] = pd.to_datetime(df_request['publish_date'])
    # --------------------------------------
    # Modication des dataframes:
    df_element = df_request[['id_element', 'kind', 'title', 'publish_date', 
                                'description', 'status']]
    df_element = df_element.rename(columns = {'kind': 'type'})
    df_element["status"] = 0
    df_search = df_request[['id_element', 'element_search_id', 'kind']]
    df_search = df_search.rename(columns = {'id_element': 'element_id'})
    # --------------------------------------
```
Note: Formatage en plusieurs dataframe et nettoyage de donnée
```python
    # Insertion dans la table:
    """element        ['id_element', 'type', 'title', 'publish_date', 
                            'description', 'status']"""
    list_element: list = wrapper("insert", "element", dataframe=df_element)
```
Note: Insertion dans la table `element`
```python
    # --------------------------------------
    # Insertion dans la table:
    """search         ['element_id', 'element_search_id', 'kind']"""
    list_search: list = wrapper("insert", "search", dataframe=df_search)
```
Note: Insertion dans la table `search`
```python
    # --------------------------------------
    print()
    # --------------------------------------
    debug_print("### Fin migration Search ###", debug_tool)
```

4.1. Nettoyage, filtrage, Structuration des données

Les données collectées sont nettoyées et filtrées afin de correspondre au type de la table mais aussi d'évité les problèmes de synthaxe dans le fonctionnement du code.
Pour des raisons de cohérence et de représentation, les champs comme le titre et la description sont conservé comme l'original.
Nous utilisons pandas pour traiter et structurer les données :

Le script migration_api_database.py Structure les données à insérer dans la base de données.

4.1.1. Code de la fonction `transfert_Search_to_BDD`,vu plus haut:

```python
    df_request['publish_date'] = pd.to_datetime(df_request['publish_date'])
```
Note: Formatage de la date en type date
```python
    # --------------------------------------
    # Modication des dataframes:
    df_element = df_request[['id_element', 'kind', 'title', 'publish_date', 
                                                    'description', 'status']]
```
Note: Création d'un nouveau dataframe correspondant à la table `element` à partir du dataframe principal
```python
    df_element = df_element.rename(columns = {'kind': 'type'})
```
Note: Modification de libellé pour correspondance avec la table `element`
```python
    df_element["status"] = 0
```
Note: Initialisation de la colonne `status` à `False`, ici 0
```python
    df_search = df_request[['id_element', 'element_search_id', 'kind']]
```
Note: Création d'un nouveau dataframe correspondant à la table `search` à partir du dataframe principal
```python
    df_search = df_search.rename(columns = {'id_element': 'element_id'})
```
Note: Modification de libellé pour correspondance avec la table `search`


4.1.2 Code de la fonction `transfert_Channel_to_BDD`:

```python
    # -------------------------------------------------------------------------
    df_result['publish_date'] = pd.to_datetime(df_result['publish_date'])
    df_result = df_result.replace({pd.NaT: None})
```
Note: Remplacement des valeurs `NaT` spécifique à pandas par `None` valide pour MySQL
```python
    df_result['description'] = df_result['description'].str.replace("'", " ", regex=True)
    df_result['description'] = df_result['description'].str.replace('"', " ", regex=True)
```
Note: Remplacement des caractères `"` et `'` par ` ` pour éviter les problèmes de chaine `str`
```python
    df_result['title'] = df_result['title'].str.replace("'", " ", regex=True)
    df_result['title'] = df_result['title'].str.replace('"', " ", regex=True)
    # --------------------------
    # Modication des dataframes:
    df_element = df_result[['id_element', 'type', 'title', 'publish_date', 'description']]
    df_element['title'] = df_element['title'].str.replace("\\", " ", regex=True)
```
Note: Remplacement des caractères `\` par ` ` pour éviter les problèmes de chaine `str`
```python
    # --------------------------
    df_proprety = df_result[['id_element', 'viewCount', 'likesCount', 'videoCount', 
                                                    'subscriberCount', 'privacyStatus']]
    df_proprety = df_proprety.rename(columns = {'id_element': 'element_id'})
    # Gestion champs privacyStatus vide en "private"
    df_proprety['privacyStatus'] = df_proprety['privacyStatus'].fillna("private")
```
Note: Remplissage des champs `None` par `private` car les chaine public retourne le statut mais pas celle qui sont privé
```python
    df_proprety_private = df_proprety[(df_result['viewCount'].isnull()) & 
                                    (df_result['likesCount'].isnull()) & 
                                    (df_result['videoCount'].isnull()) & 
                                    (df_result['subscriberCount'].isnull())]
```
Note: Récupération des chaines qui n'ont aucune donnée pour création d'un dataframe spécifique
```python
    df_proprety = df_proprety.dropna(subset=["viewCount", "likesCount", "videoCount", 
                                                "subscriberCount"], how='all')
    df_proprety_private = df_proprety_private.drop(['viewCount', 'likesCount', 
                                            'videoCount', 'subscriberCount'], axis=1)
    # --------------------------
    df_playlist_element = df_result[['playlist_id']]
    df_playlist_element["status"] = 0
    df_playlist_element.insert(loc=1, column='type', value="playlist")
```
Note: Insertion d'une nouvelle colonne avec la valeur `playlist` par défaut
```python
    df_playlist_element = df_playlist_element.rename(
                                    columns = {'playlist_id': 'id_element'})
    # --------------------------
    df_playlist_search = df_result[['id_element', 'playlist_id']]
    df_playlist_search = df_playlist_search.rename(
                                    columns = {'playlist_id': 'element_id'})
    df_playlist_search = df_playlist_search.rename(
                                    columns = {'id_element': 'element_search_id'})
    # Gestion champs kind vide en "playlist"
    df_playlist_search.insert(loc=1, column='kind', value="playlist")
    # --------------------------
    df_keyword = df_result[['id_element', 'keyword']]
    df_keyword = df_keyword.rename(columns = {'id_element': 'element_id'})
    df_keyword = df_keyword.explode('keyword')
```
Note: Le dataframe `keyword` contient une liste dans ces champs, la fonction `explode()` permet d'extraire chaque élément de la liste en conservant l'attribution des autres champs, ici `element_id`
```python
    df_keyword['keyword'] = df_keyword['keyword'].str.replace("'", " ", regex=True)
    # --------------------------
    df_topic = df_result[['id_element', 'topic']]
    df_topic = df_topic.rename(columns = {'id_element': 'element_id'})
    df_topic = df_topic.explode('topic')
    df_topic['topic'] = df_topic['topic'].str.replace("'", " ", regex=True)
```


## 5. Chargement des données

5.1. Modèle de données

Les données structurées sont mappées aux tables de la base de données MySQL.
Nous avons défini les tables pour stocker les données des vidéos, des chaînes, des 
playlists, etc.

5.2. Insertion des données

Le script insertion_database_mysql.py insère les données dans la base de données.

Exemple de code pour l'insertion des données :
```python
import mysql.connector

conn = mysql.connector.connect(
              user='root', 
              password='password', 
              host='127.0.0.1', 
              database='youtube_data')
```
Note: Création de la connection MySQL
```python
cursor = conn.cursor()
```
Note: Création du curseur MySQL
```python
for index, row in df_clean.iterrows():
    sql = "INSERT INTO videos (videoId, title, description) VALUES (%s, %s, %s)"
    cursor.execute(sql, (row['id.videoId'], row['snippet.title'], row['snippet.description']))
```
Note: Exécution du curseur pour chaque ligne du dataframe via la requète SQL
```python
conn.commit()
cursor.close()
conn.close()
```
Note: Envoie des données et cloture de la connexion

5.2.1 Fonction wrapper

Chacune des ces étaque sont réalisé dans le code suivant mais la gestion est faite en fonction de la table, et du type de requète SQL `insert`, `update`, `select`.

```python
def wrapper(interaction_BDD: str, table_name: str, dataframe: pd.DataFrame | None = None, 
            value: str | list | None = None, cond_column: str | list | None = None, 
            cond_limit: str | None = None):
    debug_print(f"-> Lancement de la fonction wrapper - Init BDD !", debug_tool)
    # Prépa insertion: -------------------------------------------
    data_config, bdd_properties = preparation_of_connexion()
```
Note: Récupération des informations pour la base de donnée
```python
    # Initialisation valeurs pour insertion data: ----------------
    nb_database: int = 0 #  ['Projet_Examen']
    # Récupération des data au format liste: ---------------------
    database_list: list = database_name_in_properties(bdd_properties)
```
Note: Récupération de la liste des base de données
```python
    # Initialisation connexion SQL: ------------------------------
    mydb, mycursor = init_SQL(data_config)
```
Note: Création de la connection et du curseur
```python
    database_name: str = database_list[nb_database]
    cursor_Projet_Examen: str = mycursor[database_list[nb_database]]
    mydb_Projet_Examen: str = mydb[database_list[nb_database]]
    mydb[database_list[nb_database]].reconnect()
```
Note: Reconnection car l'insertion dans une varible, déconnecte la connexion
```python
    # ------------------------------------------------------------
    if interaction_BDD == "id":
        column_table: list = column_name_in_properties(bdd_properties, 
                                                database_name, table_name)
        for item in column_table:
            if item.startswith("id"): 
                list_in_bdd = select_bdd_with_cond_and_limit(cursor_Projet_Examen, 
                                table_name, item, value, cond_column, cond_limit)
```
Note: Récupération des id (`select`) avec condition
```python
    elif interaction_BDD == "update":
        list_in_bdd = update_in_table(dataframe, mydb_Projet_Examen, 
        cursor_Projet_Examen, bdd_properties, database_name, table_name, cond_column)
```
Note: Mise à jour de la table
```python
    elif interaction_BDD == "select":
        list_in_bdd = select_in_table(dataframe, cursor_Projet_Examen, 
                            bdd_properties, database_name, table_name, cond_column)
```
Note: Récupération des données (`select`) avec condition
```python
    elif interaction_BDD == "insert":
        list_in_bdd = insert_in_table(dataframe, mydb_Projet_Examen, 
                    cursor_Projet_Examen, bdd_properties, database_name, table_name)
    # ------------------------------------------------------------
    return list_in_bdd
```
Note: Insertion dans la base de donnée

5.2.2 Fonction insert_in_table

```python
def insert_in_table(dataframe: pd.DataFrame, mydatabase, mycursor, bdd_properties, 
                                                name_database: str, name_table: str):
    debug_print(f"-> Insertion dans la table {name_table} !", debug_tool)
    # Initialisation de variable de la fontion: ------------------------------
    nb_column_id: int = 0 # id de la table
    list_duplicate_bdd: list = []
    list_insert_bdd: list = []
    list_id_bdd: list = []
    # Initialisation de liste de la table: -----------------------------------
    table_list: list = table_name_in_properties(bdd_properties, name_database)
```
Note: Récupération de la liste de table
```python
    if name_table in table_list:
        name_of_table: str = name_table
    else:
        return print("Erreur de table !")
    column_table: list = column_name_in_properties(bdd_properties, name_database, 
                                                                    name_table)
```
Note: Récupération de la liste des colonnes de la table
```python
    # Lancement de la boucle d'insertion: ------------------------------------
    for row in dataframe.itertuples():
        # Coordination liste de la table et liste des valeurs: ---------------
        print(f"-- {row} --")
        value_table = adapt_to_table_value_column(row, column_table)
```
Note: Formatage des valeurs pour insertion dans la requèe SQL
```python
        print("--insert in table:--")
        print(column_table, value_table)
        print("--------------------")
        id_of_insert = check_for_insertion(mydatabase, mycursor, column_table, 
                            value_table, name_database, name_of_table, nb_column_id)
```
Note: Insertion des colonnes et valeurs au bon format dans la fonction d'insertion
```python
        # Vérification du retour: id ou liste de valeur
        if type(id_of_insert) == list and id_of_insert != []:
            list_duplicate_bdd.append(id_of_insert)
        elif type(id_of_insert) == str:
            list_insert_bdd.append(id_of_insert)
        list_id_bdd.append(value_table)
    # Création du rapport d'insertion: ---------------------------------------
    report_insertion("insert", name_table, list_insert_bdd)
    report_insertion("duplicate", name_table, list_duplicate_bdd)
    return list_id_bdd
```

5.2.3 Fonction check for insertion

```python
def check_for_insertion(mydb, cursor, column_list: list, value_list: list,
                    database_name: str, table_name: str, nb_table_column_id: int):
    print(f"-> Utile / Check for insertion in the table {table_name} !")
    dico: dict = list_to_dict(column_list, value_list)
    columns_many_table: str = many_column_str(dico)
    format_columns_many_table: str = format_many_column_str(dico)
    try:
        sql = f"""INSERT INTO {table_name} ({columns_many_table}) 
                    VALUES ({format_columns_many_table})"""
        print("Value: ", dico)
        print("SQL: ", sql)
        cursor.execute(sql, dico)
        mydb.commit()
```
Note: Insertion dans la base de donnée
```python
    except IntegrityError as error:
```
Note: Gestion des doublons
```python
        print("Except: ", error)
        sql = f"SELECT {columns_many_table} FROM {table_name} 
                WHERE {column_list[nb_table_column_id]} = %s;"
        val = [value_list[nb_table_column_id]]
        print("Value: ", dico)
        print("SQL: ", sql)
        cursor.execute(sql, val)
        data_select = cursor.fetchall()
        return data_select
    except DataError as error_data:
        print("Erreur Inconnu: \n", error_data)
    return cursor.lastrowid
```

## 6. Automatisation du processus

6.1. Scripts et outils d'automatisation

Les scripts sont automatisés pour s'exécuter périodiquement et collecter les données de 
manière continue.
Nous utilisons des tâches planifiées (cron jobs) pour automatiser l'exécution des scripts.

6.2. Surveillance et gestion des erreurs

Des mécanismes de journalisation sont mis en place pour surveiller le processus et gérer les 
erreurs.
Les erreurs sont enregistrées et des notifications sont envoyées pour une intervention rapide, grace au fichier excel.

## 7. Exemple de flux de travail

7.1. Diagramme du flux de travail

Le diagramme ci-dessous illustre le flux de travail complet de l'importation des données, de l'extraction à partir des API YouTube jusqu'au chargement dans la base de données MySQL :

    Étape 1 : Connexion à l'API YouTube via connexion_api_Youtube.py.
    Étape 2 : Extraction des données via extraction_api_Youtube.py.
    Étape 3 : Sauvegarde des données dans un fichier Excel via save_data_as_file.py.
    Étape 4 : Migration des données vers la base de données via migration_api_database.py.
    Étape 5 : Insertion des données dans la base de données MySQL via extraction_database_mysql.py
    et connexion_database_mysql.py.

7.2. Diagramme des fonctions de migration

![Diagramme](/home/antony/Projet_Examen/Rapport/Ressources/Architecture_API_BDD.png)