---
output: pdf_document
editor_options: 
  markdown: 
    wrap: 80
---

# **Création de la Base de Données**  

## **Choix du SGBD**  

Le choix du système de gestion de base de données (SGBD) se fait en fonction de plusieurs critères :  
- **Facilité d'utilisation** :  
    Il est crucial de pouvoir travailler avec le système sans une gestion complexe. Un système de gestion connu (comme MySQL ou SQLite) sera préférable pour faciliter la mise en place de la base de données.  
- **Cohérence des données** :  
    Les contraintes d'intégrité sont essentielles. MySQL est préféré à SQLite pour ce critère.  
- **Sécurité des données** :  
    Ici, ce critère n'est pas prioritaire car la base de données ne sera déployée que localement.  
- **Performance et efficacité** :  
    La performance concerne le temps de réponse, l’intégration avec d'autres logiciels, la capacité d’évolution, et le support des accès simultanés par plusieurs utilisateurs. MySQL, en combinaison avec MySQL Connector pour Python, est un bon choix pour ce projet.  


## **Définition de la base de données**  

Une fois le SGBD choisi, nous procédons à son installation et configuration via le terminal.

- Etape 1: Mise à jour du dépôt des paquets 
```sh
sudo apt-get update
```

- Etape 2: Installation de MySQL 
```sh
sudo apt-get install mysql-server
```

- Etape 3: Création de l'admin   
```sh
mysqladmin -u root -h localhost password 'new-password'
```

- Etape 4: Création du fichier `init_BDD.sql` pour créer la base de données  

- Etape 5: Exécution du fichier `init_BDD.sql ` 
```sh
mysql init_BDD.sql
```

- Etape 6: Création d'un utilisateur et attribution des permissions pour la base de données  
```sh
CREATE USER '[utilisateur]'@'localhost' IDENTIFIED BY '[mot_de_passe]';
GRANT [privileges] ON database.* TO '[utilisateur]'@'[host]';
```
Les types de privilèges comprennent : ALL, CREATE, SELECT, INSERT, SHOW DATABASES, USAGE, GRANT OPTION. 


## **Création des tables**  
Le fichier de création de la base de données `init_BDD.sql` contient toutes les requêtes SQL nécessaires. Chaque table est créée via la requête CREATE TABLE qui attribue des champs ainsi que des types aux champs. Les relations sont créées via la requête ADD FOREIGN KEY en référencant les tables à lier.  

- Etape 1: Création de la base de données `Projet_Examen`  
```sql
DROP DATABASE IF EXISTS Projet_Examen;

CREATE DATABASE IF NOT EXISTS Projet_Examen;

USE Projet_Examen;
```

- Etape 2: Création de la table `element`  
```sql
CREATE TABLE `element` (
  `id_element` varchar(255) UNIQUE PRIMARY KEY NOT NULL,
  `type` ENUM ('channel', 'video', 'playlist', 'pays') NOT NULL,
  `title` varchar(255),
  `publish_date` date,
  `description` text,
  `status` bool DEFAULT 0
);
```

- Etape 4: Création de la table `search`  
```sql
CREATE TABLE `search` (
  `element_id` varchar(255) NOT NULL,
  `element_search_id` varchar(255) NOT NULL,
  `kind` varchar(255) NOT NULL
);
```

- Etape 5: Création de la table `playlist`  
```sql
CREATE TABLE `playlist` (
  `playlist_id` varchar(255) NOT NULL,
  `channel_id` varchar(255) NOT NULL,
  `video_id` varchar(255) NOT NULL
);
```

- Etape 6: Création de la table `keywords`  
```sql
CREATE TABLE `keywords` (
  `id_keyword` varchar(255) PRIMARY KEY NOT NULL,
  `keyword` varchar(255) NOT NULL
);
```

- Etape 8: Création de la table `topics`  
```sql
CREATE TABLE `topics` (
  `id_topic` varchar(255) PRIMARY KEY NOT NULL,
  `topic` varchar(255) NOT NULL
);
```

- Etape 10: Création de la table `proprety`  
```sql
CREATE TABLE `proprety` (
  `element_id` varchar(255) UNIQUE NOT NULL,
  `viewCount` int,
  `likesCount` varchar(255),
  `videoCount` int,
  `commentCount` int,
  `subscriberCount` int,
  `duration` int,
  `privacyStatus` ENUM ('private', 'public', 'unlisted') NOT NULL
);
```

- Etape 11: Création des index pour la vérication de la cohérence des données  
```sql
CREATE UNIQUE INDEX `search_index_0` ON `search` (`element_id`, `element_search_id`);

CREATE UNIQUE INDEX `playlist_index_1` ON `playlist` (`playlist_id`, `channel_id`, `video_id`);
```

- Etape 12: Création des clés étrangères pour les relations des tables  
```sql
ALTER TABLE `search` ADD FOREIGN KEY (`element_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `search` ADD FOREIGN KEY (`element_search_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `playlist` ADD FOREIGN KEY (`channel_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `playlist` ADD FOREIGN KEY (`playlist_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `playlist` ADD FOREIGN KEY (`video_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `proprety` ADD FOREIGN KEY (`element_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `keywords` ADD FOREIGN KEY (`element_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `topics` ADD FOREIGN KEY (`element_id`) REFERENCES `element` (`id_element`);
```