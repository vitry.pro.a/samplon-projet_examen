---
output: pdf_document
editor_options: 
  markdown: 
    wrap: 80
---

# **Collecte des Données**

## 1. **Recherche et sélection des sources de données**

Lors de mes recherches de datasets de données YouTube, j'ai constaté que ceux mis à disposition étaient principalement des extraits de YouTube, des données issues de sites comme SocialBlade, ou des résultats de scrapping de pages web. Ces informations ne contenaient pas les données spécifiques dont j'avais besoin, j'ai donc décidé de me tourner vers la source : l'API YouTube Data.

J'ai utilisé l'API YouTube pour extraire les données nécessaires. En complément, j'ai créé un dataset basé sur le scrapping d'un site répertoriant le Top 5000 des chaînes YouTube françaises.

## 2. **Méthodologie de collecte des données**

### Utilisation de l'API YouTube Data

L'API YouTube Data est une interface logicielle qui permet de "connecter" un logiciel ou un service à un autre pour échanger des données et des fonctionnalités.

#### Processus de collecte :

1. **Configuration de l'API** :
   - Définition de l'URL de base pour l'API : `https://www.googleapis.com/youtube/v3/`
   - Sélection des types de résultats de recherche : `search`, `channels`, `videos`, etc.
   - Utilisation des méthodes associées : `list`, `update`, `insert`, `delete`, etc.
   - Spécification des blocs d'information (paramètre `part`) : `snippet`, `statistics`, `id`, etc.

2. **Exécution des requêtes** :
   - Exemple de requête pour récupérer les informations de chaînes :
     ```plaintext
     https://youtube.googleapis.com/youtube/v3/channels?part=snippet,statistics&id=
     UC0XmApkQJH-c29sZnbjbVFA&maxResults=50&key=[YOUR_API_KEY]
     ```
   - Cette requête retourne un JSON contenant les données demandées.

3. **Traitement des données** :
   - Analyse du JSON reçu pour extraire les informations pertinentes.
   - Nettoyage des données pour éliminer les informations inutiles.
   - Insertion des données nettoyées dans une base de données pour une utilisation ultérieure.

### Scraping d'un site web

En complément de l'API YouTube, j'ai réalisé un scrapping (via un logiciel de scrapping intégrer à Google Chrome) d'un site répertoriant le Top 5000 des chaînes YouTube françaises. Les données obtenues ont été traitées et intégrées dans le dataset principal pour enrichir l'analyse.