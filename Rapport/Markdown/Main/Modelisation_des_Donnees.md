---
output: pdf_document
editor_options: 
  markdown: 
    wrap: 80
---

# **Modélisation des Données:**  

## 1. Création du modèle de données  

Traitement des résultats :  

Pour la création du modèle de données, il est essentiel d'examiner les informations issues de l'API YouTube Data.  

Parmi les 21 types de résultats possibles, nous utiliserons uniquement :  

    search  
    channel  
    video  
    playlist  
    playlistItem  

Ensuite, sur les 5 JSON obtenus, nous utiliserons uniquement la méthode list. Enfin, nous devons sélectionner les blocs à envoyer dans la requête pour obtenir les champs nécessaires.  

Un format JSON se structure de la manière suivante :  

```JSON  
{
    "kind": "youtube#searchResult",
    "etag": etag,
    "id": {
        "kind": string,
        "videoId": string,
        "channelId": string,
        "playlistId": string
    },
    "snippet": {
        "publishedAt": datetime,
        "channelId": string,
        "title": string,
        "description": string,
        "thumbnails": {
            (key): {
                "url": string,
                "width": unsigned integer,
                "height": unsigned integer
            }
        },
        "channelTitle": string,
        "liveBroadcastContent": string
    }
}
```  

Nous avons les clés du JSON qui définissent les blocs d'informations (part) et leurs valeurs (les informations dont nous avons besoin). Dans cet exemple, il s'agit du JSON pour la requête search avec le part initialisé sur snippet.  

Informations nécessaires pour le projet :   
Collection résultat - Search:  

    "kind": "youtube#searchResult"  
    "videoId": string  
    "channelId": string  
    "playlistId": string  
    "publishedAt": datetime  
    "title": string  
    "description": string  

Collection résultat - Channel:  

    "kind": "youtube#channel"  
    "id": string  
    "country": string  
    "likes": string  
    "uploads": string   
    "viewCount": unsigned long  
    "subscriberCount": unsigned long  
    "hiddenSubscriberCount": boolean  
    "videoCount": unsigned long  
    "topicIds": [string]  
    "keywords": string 

Collection résultat - Playlist:  

    "kind": "youtube#playlist"  
    "id": string  
    "publishedAt": datetime  
    "channelId": string  
    "title": string  
    "description": string  

Collection résultat - PlaylistItem:  

    "kind": "youtube#playlistItem"  
    "id": string  
    "publishedAt": datetime  
    "channelId": string  
    "playlistId": string  
    "videoId": string  

Collection résultat - Video:  

    "kind": "youtube#video"  
    "id": string  
    "publishedAt": datetime  
    "channelId": string  
    "title": string  
    "description": string  
    "tags": [string]  
    "duration": string  
    "privacyStatus": string  
    "viewCount": string  
    "likeCount": string   
    "dislikeCount": string  
    "favoriteCount": string  
    "commentCount": string  
    "relevantTopicIds": [string]  

## 2. Diagrammes et schémas  

### **Analyse des résultats :**  

Le modèle de données est le plan de l'architecture de la base de données. Pour le construire, nous procédons comme suit :  

    Création du MCD (Modèle Conceptuel de Données)  
    Transformation du MCD en MLD (Modèle Logique de Données)  

1. MCD (Modèle Conceptuel de Données)  

    - Entité : regroupe l'information statique et durable.  
        Par exemple, l’entité channel rassemble toutes les informations communes aux chaînes YouTube.  
        Une entité est représentée par un nom commun écrit en majuscules et au singulier.  
        Exemples : SEARCH, CHANNEL, VIDEO, PLAYLIST, PLAYLISTITEM.  

    - Association : matérialise la dynamique du système et les relations entre les entités.  
        Par exemple, l'entité channel est en relation avec l'entité video.  
        L'association est représentée par un verbe d'action ou d'état à l'infinitif.  
        Exemples : ASSOCIER, DIFFUSER, CONTIENT, RECHERCHER.  

**Graphique de l'association des entités de la recherche :**  

    CHANNEL est recherché par PAYS  
    VIDEO ou PLAYLISTITEM ou PLAYLIST est recherché par CHANNEL  

- Association des éléments par recherche:  
![MCD association entité](/home/antony/Projet_Examen/Rapport/Ressources/MCD-association.png)  

**Graphique de l'association du type de la recherche aux champs JSON :**   

    Recherche par type (search-pays, search-channel) aux champs JSON (element)  
    Recherche par type (channel-video, channel-playlist, channel-playlistItem) aux 
    champs JSON (element, Propriétes channel, Propriétes Video)  

- Association par type de collection:  
![MCD association type aux champs](/home/antony/Projet_Examen/Rapport/Ressources/MCD-relation.png){width=90%}  

**Graphique de l'association des entités :**   

- Search est recherché par Element:  
![MCD search](/home/antony/Projet_Examen/Rapport/Ressources/MCD-search_relation.png){width=60%}  

- Element est organisé dans une Playlist:  
![MCD playlist](/home/antony/Projet_Examen/Rapport/Ressources/MCD-playlist_relation.png){width=60%} 

- Search est recherché par Country:  
![MCD country](/home/antony/Projet_Examen/Rapport/Ressources/MCD-country_relation.png){width=60%}  

- Element est associé à Keywords:  
![MCD keywords](/home/antony/Projet_Examen/Rapport/Ressources/MCD-keyword_relation.png){width=60%}  

- Element est classifié par Topic:  
![MCD topic](/home/antony/Projet_Examen/Rapport/Ressources/MCD-topic_relation.png){width=60%}   

- Element dispose de Propriétés:  
![MCD propriété](/home/antony/Projet_Examen/Rapport/Ressources/MCD-propriete_relation.png){width=60%}   

- Récapitulatif des associations:  
![MCD association des entités](/home/antony/Projet_Examen/Rapport/Ressources/MCD-all_entity.png){width=90%}  

2. MLD (Modèle Logique de Données)  

**Association réflexive :**  
*Un graphe non orienté est transformé en une table associative. Cette table associative doit contenir un couple de clés étrangères qui réfèrent à la table des nœuds du graphe.*   

- Search est recherché par Element:  
![MCD search table](/home/antony/Projet_Examen/Rapport/Ressources/MCD-search_table.png){width=60%}  

- Element est organisé dans une Playlist:  
![MCD playlist table](/home/antony/Projet_Examen/Rapport/Ressources/MCD-playlist_table.png){width=60%}  

- Search est recherché par Country:  
![MCD country table](/home/antony/Projet_Examen/Rapport/Ressources/MCD-country_table.png){width=70%}   

- Element est associé à Keywords:  
![MCD keywords table](/home/antony/Projet_Examen/Rapport/Ressources/MCD-keyword_table.png){width=70%}   

- Element est classifié par Topic:  
![MCD topic table](/home/antony/Projet_Examen/Rapport/Ressources/MCD-topic_table.png){width=70%}   

- Element dispose de Propriétés:  
![MCD propriété table](/home/antony/Projet_Examen/Rapport/Ressources/MCD-propriete_table.png){width=70%}   

Une fois, toutes les entités et les associations sont transformé sous forme de table.  
On obtient le modèle logique de donnée, qu'on va pouvoir utilisé pour la création de la base de donnée.


**MLD final via dbDiagram.io:**  
![MLD](/home/antony/Projet_Examen/Rapport/Ressources/MLD.png){width=100%}  