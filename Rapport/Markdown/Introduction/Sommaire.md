---
output: pdf_document
editor_options: 
  markdown: 
    wrap: 80
---

# Sommaire:

1.  **Introduction**
    -   **Contexte du projet** : 
    -   **Objectifs du rapport** : 

2.  **Présentation du Projet**
    -   **Description du projet** : 
    -   **Enjeux et objectifs** : 
    -   **Limites techniques** :

3.  **Analyse des Besoins**
    -   **Identification des cas d'utilisation** :  
    -   **Définition des fonctionnalités principales** :  

4.  **Collecte des Données**
    -   **Recherche et sélection des sources de données** : 
    -   **Méthodologie de collecte des données** : 

5.  **Modélisation des Données**
    -   **Création du modèle de données** : 
    -   **Diagrammes et schémas** : 

6.  **Création de la Base de Données**
    -   **Choix du SGBD** :  
    -   **Définition de la base de données** : 
    -   **Création des tables** :  


7.  **Importation des Données**
    -   **Préparation des données** :
    -   **Processus d'importation** :
    -   **Traitement des données JSON en fonction des types de requète** :
    -   **Transformation des données** : 
    -   **Chargement des données** : 
    -   **Automatisation du processus** : 
    -   **Exemple de flux de travail** : 

8.  **Mise en Place des Vues**
    -   **Définition des vues** : 
    -   **Conception des vues** : 

9.  **Implémentation des Vues**
    -   **Développement des vues** : 
    -   **Technologies et outils** : 

10. **Conclusion**
    -   **Résumé des réalisations** : 
    -   **Difficultés rencontrées et solutions** : 
    -   **Perspectives d'amélioration** : 

11. **Remerciements**

12. **Annexes**

13. **Bibliographie**

