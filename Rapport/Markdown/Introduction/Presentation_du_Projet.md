---
output: pdf_document
editor_options: 
  markdown: 
    wrap: 80
---

# **Présentation du Projet**  

1. Description du projet

Le projet consiste à analyser les informations des chaînes YouTube d'un pays donné en se basant sur plusieurs métriques : le nombre d'abonnés, le nombre de vidéos publiées, et les vues par chaîne et par vidéo. L'objectif principal est de constater les corrélations entre ces dimensions pour identifier des patterns éventuels.

L'objectif spécifique de ce projet est de vérifier s'il existe une relation vérifiable concernant la popularité des chaînes, mesurée par le nombre d'abonnés.


2. Enjeux et objectifs

**Enjeux**

Ce projet présente un intérêt significatif pour le débat public, car il traite de la manière dont nous interprétons et généralisons les informations disponibles. Dans notre société actuelle, il est courant de tirer des conclusions sur la base d'informations accessibles, sans toujours tenir compte de leur pertinence ou des biais qu'elles peuvent contenir. Les corrélations trouvées dans ces informations sont souvent prises pour des faits acquis et généralisés.

Avec des millions d'utilisateurs et de créateurs de contenu, YouTube est une plateforme où beaucoup aspirent à devenir des "youtubeurs" à succès. Cependant, les opinions sur ce qu'il faut pour réussir sur YouTube varient considérablement, et il n'y a pas de consensus clair, même parmi les youtubeurs eux-mêmes.
Objectifs

L'objectif de ce projet est de collecter des données publiques sur YouTube, de les analyser et de les mettre en relation pour identifier des patterns. Plus précisément, nous cherchons à :

    - Mettre en évidence des informations clés : 
    Collecter des données sur les chaînes YouTube, telles que le nombre d'abonnés, 
    le nombre de vidéos publiées, et le nombre de vues.

    - Analyser les corrélations : 
    Examiner les relations entre ces différentes métriques pour identifier des 
    tendances et des patterns qui pourraient expliquer la popularité d'une chaîne.

    - Fournir une base de réflexion : 
    Utiliser les résultats pour contribuer au débat sur ce qui fait le succès 
    d'un youtubeur, en apportant des éléments de réponse basés sur des données 
    concrètes plutôt que sur des opinions.

3. Limites techniques

Pour des raisons techniques, ce projet se limitera à l'analyse de données publiques disponibles sur YouTube. Les données seront collectées, affichées, et analysées exclusivement sur ces bases. Les conclusions tirées de cette analyse seront donc limitées par la nature et la qualité des données accessibles.