---
output: pdf_document
editor_options: 
  markdown: 
    wrap: 80
---

# **Introduction**

1. Contexte du projet

Le projet est réalisé dans le cadre de la certification RNCP - Expert en infrastructures de données massives. Cette certification vise à valider les compétences acquises au cours de la formation dispensée par l'organisme Simplon.

Ce projet se situe à l'intersection de plusieurs domaines clés du data management, notamment la collecte, le traitement, et l'analyse de grandes quantités de données. Dans un monde où les données jouent un rôle crucial dans la prise de décision, maîtriser ces compétences est essentiel pour tout professionnel des infrastructures de données.  

2. Objectifs du rapport

Ce rapport a pour objectif de démontrer et valider les compétences acquises au cours de la formation pour l'obtention du titre RNCP. Plus spécifiquement, il se focalise sur trois domaines principaux :

    - Programmation de la collecte de données : 
    Démontrer la capacité à collecter des données depuis plusieurs sources de 
    manière efficace et automatisée pour un projet data.

    - Développement de la mise à disposition technique des données collectées : 
    Montrer comment les données collectées peuvent être rendues accessibles et 
    utilisables via des moyens techniques appropriés, tels que la création d'API.

    - Production de visualisations de données : 
    Illustrer la compétence en création de visualisations de données, 
    permettant d'analyser et de présenter les informations de manière claire 
    et informative.

Ce rapport est destiné aux évaluateurs de Simplon ainsi qu'aux professionnels intéressés par l'application pratique des compétences en gestion des données massives.