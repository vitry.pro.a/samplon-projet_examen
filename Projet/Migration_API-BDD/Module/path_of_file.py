from datetime import datetime
import os

# /home/antony/Projet_Examen/Projet/Migration_API-BDD/Module/utile_save_data.py
path_excel: str = '/home/antony/Projet_Examen/Projet/Ressources/DATA/Export_BDD/get_channel_id-search_2024-05-11.xlsx'

# /home/antony/Projet_Examen/Projet/Migration_API-BDD/Module/save_data_as_file.py
path: str = "/home/antony/Projet_Examen/Projet/Ressources/DATA/"

# /home/antony/Projet_Examen/Projet/Migration_API-BDD/Module/insertion_database_mysql.py
    # Init Module
path_module: str = "/home/antony/Projet_Examen/Projet/Migration_API-BDD/Module/"
    # Config
path_config: str = "/home/antony/Projet_Examen/Projet/Migration_API-BDD/Module/Properties/"
file_name_config: str = "bdd_config.json"
file_name_properties: str = "bdd_properties.json"
    # Report
path_report: str = "/home/antony/Projet_Examen/Projet/Ressources/DATA/Report/"
dico: str = "search"
file_api_import: str = f"API_extract_{dico}_{datetime.now().date()}.xlsx"
file_insert_report: str = f"{datetime.now().date()}_Insertion_insert_"
file_dupli_report: str = f"{datetime.now().date()}_Duplicate_"
ext_name_report: str = ".json"
    # Excel
path_data_excel: str = "/home/antony/Projet_Examen/Projet/Ressources/DATA/"
    # Country
path_file_code_country: str = "/home/antony/Projet_Examen/Projet/Ressources/Sources/countries-codes.xlsx"
    # Channel FR
path_file_top_FR: str = "/home/antony/Projet_Examen/Projet/Ressources/Sources/"
file_name_top_FR: str = "top_5000_FR.xlsx"
    # __main__
Export_file_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/Export_file/')
# à remplacer par /home/antony/Projet_Examen/Ressources/DATA

# /home/antony/Projet_Examen/Projet/Migration_API-BDD/Module/connexion_database_mysql.py
data_config: dict = {
    "path_config": path_config, 
    "list_database": ['Projet_Examen'], 
    "name_config": file_name_config, 
    "name_properties": file_name_properties,
    "return_dictionnary": False # dictionary = True, return dict / dictionary = False, return liste
    }