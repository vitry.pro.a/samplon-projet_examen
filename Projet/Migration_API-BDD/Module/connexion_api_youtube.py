import os
from argparse import ArgumentParser
from googleapiclient import discovery, errors
from utile_api_youtube import *
from data_argument import *

from YOUR_CLIENT_SECRET_FILE import api_key

debug_tool = True

def create_API_Youtube_Data():
    debug_print("-> Création du 'build' pour l'API !", debug_tool)
    API_SERVICE_NAME = "youtube"
    API_VERSION = "v3"
    # Get credentials and create an API client
    return discovery.build(API_SERVICE_NAME, API_VERSION, developerKey=api_key)


def connexion_API_Youtube_Data():
    debug_print("-> Connexion à l'API Youtube Data !", debug_tool)
    # Disable OAuthlib's HTTPS verification when running locally.
    # *DO NOT* leave this option enabled in production.
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"
    return create_API_Youtube_Data()


def request_API_Youtube(options_argparse, type_function):
    debug_print("-> Lancement de la fonction request API Youtube !", debug_tool)
    """["search_channel", "search_video", "search_playlist", "channel", "video", "playlist", "playlistItem"]"""
    def option_select(dico, argparse):
        for key in vars(argparse):
            dico[key] = getattr(argparse, str(key))
    # Création de l'API Youtube
    youtube_api_data = connexion_API_Youtube_Data()
    if type_function in ["search_channel", "search_video", "search_playlist"]:
        option_select(dico_option_search, options_argparse)
        try:
            response_JSON = youtube_api_data.search().list(
                    part =              dico_option_search["part"], 
                    channelId =         dico_option_search["channelId"], 
                    channelType =       dico_option_search["channelType"],  
                    publishedAfter =    dico_option_search["publishedAfter"], 
                    regionCode =        dico_option_search["regionCode"], 
                    type =              dico_option_search["type"], 
                    publishedBefore =   dico_option_search["publishedBefore"],  
                    maxResults =        dico_option_search["maxResults"], 
                    videoType =         dico_option_search["videoType"], 
                    pageToken =         dico_option_search["pageToken"], 
                    q =                 dico_option_search["q"], 
                    order =             dico_option_search["order"]
                ).execute()
        except errors.HttpError as e:
            print('An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))
            response_JSON = None
    elif type_function == "channel":
        try:
            option_select(dico_option_channel, options_argparse)
            response_JSON = youtube_api_data.channels().list(
                    part =                   dico_option_channel["part"], 
                    hl =                     dico_option_channel["hl"], 
                    mine =                   dico_option_channel["mine"], 
                    mySubscribers =          dico_option_channel["mySubscribers"], 
                    id =                     dico_option_channel["id"],
                    managedByMe =            dico_option_channel["managedByMe"], 
                    onBehalfOfContentOwner = dico_option_channel["onBehalfOfContentOwner"], 
                    forUsername =            dico_option_channel["forUsername"], 
                    pageToken =              dico_option_channel["pageToken"], 
                    categoryId =             dico_option_channel["categoryId"], 
                    maxResults =             dico_option_channel["maxResults"]
                ).execute()
        except errors.HttpError as e:
            print('An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))
            response_JSON = None
    elif type_function == "video":
        try:
            option_select(dico_option_video, options_argparse)
            response_JSON = youtube_api_data.videos().list(
                    part=                   dico_option_video["part"], 
                    hl=                     dico_option_video["hl"], 
                    maxWidth=               dico_option_video["maxWidth"], 
                    locale=                 dico_option_video["locale"], 
                    id=                     dico_option_video["id"], 
                    onBehalfOfContentOwner= dico_option_video["onBehalfOfContentOwner"], 
                    regionCode=             dico_option_video["regionCode"], 
                    pageToken=              dico_option_video["pageToken"], 
                    maxResults=             dico_option_video["maxResults"], 
                    chart=                  dico_option_video["chart"], 
                    myRating=               dico_option_video["myRating"], 
                    maxHeight=              dico_option_video["maxHeight"], 
                    videoCategoryId=        dico_option_video["videoCategoryId"]
                ).execute()
        except errors.HttpError as e:
            print('An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))
            response_JSON = None
    elif type_function == "playlist":
        try:
            option_select(dico_option_playlist, options_argparse)
            response_JSON = youtube_api_data.playlists().list(
                    part=                           dico_option_playlist["part"], 
                    onBehalfOfContentOwner=         dico_option_playlist["onBehalfOfContentOwner"], 
                    pageToken=                      dico_option_playlist["pageToken"], 
                    onBehalfOfContentOwnerChannel=  dico_option_playlist["onBehalfOfContentOwnerChannel"], 
                    hl=                             dico_option_playlist["hl"], 
                    channelId=                      dico_option_playlist["channelId"], 
                    mine=                           dico_option_playlist["mine"], 
                    maxResults=                     dico_option_playlist["maxResults"], 
                    id=                             dico_option_playlist["id"]
                ).execute()
        except errors.HttpError as e:
            print('An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))
            response_JSON = None
    elif type_function == "playlistItem":
        try:
            option_select(dico_option_playlistItem, options_argparse)
            response_JSON = youtube_api_data.playlistItems().list(
                    part=                   dico_option_playlistItem["part"], 
                    onBehalfOfContentOwner= dico_option_playlistItem["onBehalfOfContentOwner"], 
                    pageToken=              dico_option_playlistItem["pageToken"], 
                    playlistId=             dico_option_playlistItem["playlistId"], 
                    videoId=                dico_option_playlistItem["videoId"], 
                    maxResults=             dico_option_playlistItem["maxResults"], 
                    id=                     dico_option_playlistItem["id"]
                ).execute()
        except errors.HttpError as e:
            print('An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))
            response_JSON = None
    return response_JSON


if __name__=="__main__":
    # from extration_api_youtube import *
    
    # Création du parseur pour les arguments:
    def gestion_option(type_of_argument):
        debug_print(f"-> Lancement de la fonction option arguments - {type_of_argument} !", debug_tool)
        ["search_channel", "search_video", "search_playlist", "channel", "video", "playlist", "playlistItem"]
        if type_of_argument == "search_channel":
            arguments = arg_search_channel
        elif type_of_argument == "channel":
            arguments = arg_channel
        # Création du parseur pour les arguments:
        parser = ArgumentParser()
        for key in arguments.keys():
            if arguments[key] != None:
                parser.add_argument(f"--{key}", help=f"{key}", default=arguments[key])
            else:
                parser.add_argument(f"--{key}", help=f"{key}")
        args = parser.parse_args()
        return parser, args
    
    type_of_request: str = "channel"
    parser, args = gestion_option(type_of_request)
    print(args)
    
    id: str = "UCu8imFFd1IQQMk6znGL481w"
    # parser, arguments = option_set_if(parser, args, id)
    # print(f"Modification {id} argument", arguments)
    
    print("test argument")
    JSON_search = request_API_Youtube(args, type_of_request)
    print(JSON_search)