from mysql.connector import *
from mysql.connector.errors import *


# Transformation data: ------------------------------------------------------------------
def list_to_dict(list_column, list_value):
    """Transforme une liste en dictionnaire
    Args:
        list_column (_type_): _description_
        list_value (_type_): _description_
    Returns:
        _type_: dictionnaire
    """
    # dict_from_list = dict(zip(list_column, list_value))
    dictionnary = {}
    for key, value in zip(list_column,list_value):
        dictionnary[key] = value
    return dictionnary


def many_column_str(list_column):
    # Récupération nom de colonne sous forme str
    # Résultat: customerNumber, customerName, contactLastName, 
    # contactFirstName, phone, addressLine1, addressLine2, city, 
    # state, postalCode, country, salesRepEmployeeNumber, creditLimit
    columns_many_list = []
    for cle in list_column.keys():
        columns_many_list.append(cle)
    return ", ".join(columns_many_list)


def format_many_column_str(list_column):
    # Récupération nom de colonne sous forme str
    # Résultat: %(name)s, %(gender)s
    columns_many_list = []
    for cle in list_column.keys():
        columns_many_list.append(f"%({cle})s")
    return ", ".join(columns_many_list)


def update_column_str(column_list: list, value_list: list):
    """colonne_1 = 'valeur 1', colonne_2 = 'valeur 2', colonne_3 = 'valeur 3'"""
    set_col_val: list = []
    for col, val in zip(column_list, value_list):
        if type(val) == int:
            set_col_val.append(f'{col} = {val}')
        else:
            set_col_val.append(f"{col} = '{val}'")
    return ', '.join(set_col_val)


# Connection MySQL: ------------------------------------------------------------------
def create_connexion_by_bdd(connector, list_of_bdd: list):
    """Créer un dictionnaire avec les différentes connections
    Args:
        connector (_type_): connector
        list_of_bdd (list): liste des noms des databases à connecté
    Returns:
        _type_: dictionnaire avec les différentes connections
    """
    db_dict = {}
    for name in list_of_bdd:
        db_dict[name] = connector.connect(host="localhost", user="antony", 
                                                password="choupette", database=name)
    return db_dict


# Cusrseur MySQL: ------------------------------------------------------------------
def create_cursor_by_bdd(listing_bdd, connexion_in_dict, return_dictionary):
    cursor_in_dict = {}
    for database in listing_bdd: # item: str, boucle: list
        # Database: Projet_Examen
        mydatabase = connexion_in_dict[database]
        # Connexion à la database:
        mydatabase.reconnect()
        # Option du curseur:
        cursor_in_dict.update({f"{database}": mydatabase.cursor(dictionary=return_dictionary)})
        # Déconnexion de la database:
        mydatabase.close()
    return cursor_in_dict


# Informations Base de donnée: ------------------------------------------------------------------
def list_of_table_bdd(cursor):
    """Interroge la BDD pour avoir la liste des tables de la BDD
    Args:
        cursor (_type_): _description_
        BDD (str): _description_
    Returns:
        _type_: liste des tables
    """
    sql = "Show tables;"
    cursor.execute(sql)
    list_tables_bdd = cursor.fetchall()
    list_table = []
    for ind, val in enumerate(list_tables_bdd):
        list_table.insert(ind, val[0])
    return list_table


def list_of_column_bdd(cursor, BDD: str, table: str) -> list:
    """Interroge la BDD pour avoir la liste des colonnes d'une table
    Args:
        cursor (_type_): _description_
        BDD (str): _description_
        table (str): _description_
    Returns:
        _type_: liste des colonnes
    """
    sql = f"""SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS
                WHERE TABLE_SCHEMA = %s
                AND TABLE_NAME = %s;"""
    cursor.execute(sql, [BDD, table])
    list_column_bdd = cursor.fetchall()
    list_column = []
    for ind, val in enumerate(list_column_bdd):
        list_column.insert(ind, val[0])
    return list_column


def database_properties_extraction(list_of_bdd, db_dict, cursor_dict, cond_dictionary):
    table_dict: dict = {}
    bdd_info_dict: dict = {}
    # Pour chaque database:
    for database in list_of_bdd: # item: str, boucle: list
        # Database: Projet_Examen
        mydb = db_dict[database]
        # Connexion à la database:
        mydb.reconnect()
        # Option du curseur:
        cursor_dict.update({f"{database}": mydb.cursor(dictionary=cond_dictionary)})
        # Création d'un dictionnaire des tables par database
        table_dict.update({f"{database}": list_of_table_bdd(cursor_dict[database])})
        # Pour chaque table:
        column_dict: dict = {}
        for table in table_dict[database]: # item: str, boucle: list
            column_dict.update({f"{table}": list_of_column_bdd(cursor_dict[database], database, table)})
        bdd_info_dict.update({f"{database}": column_dict})
        mydb.commit()
    mydb.close()
    return bdd_info_dict


# Extrations Base de donnée: ------------------------------------------------------------------
def select_bdd(cursor, table: str, column: str):
    print(f"-> Utile / Récupération id dans la table {table} !")
    sql = f"SELECT {column} FROM {table};"
    print(sql)
    cursor.execute(sql)
    return cursor.fetchall()


def select_bdd_with_limit(cursor, table: str, column: str, value: list, cond_column: str, limit: str):
    print(f"-> Utile / Récupération id par type {cond_column} dans la table {table} avec {limit} element!")
    sql = f"SELECT {column} FROM {table} WHERE {cond_column} = %s LIMIT {limit};"
    print(sql, [value])
    cursor.execute(sql, [value])
    return cursor.fetchall()


def select_bdd_with_cond(cursor, table: str, column: str, value: list, cond_column: str):
    print(f"-> Utile / Récupération id dans la table {table} !")
    sql = f"SELECT {column} FROM {table} WHERE {cond_column} = %s;"
    print(sql, [value])
    cursor.execute(sql, [value])
    return cursor.fetchall()


def select_bdd_with_cond_and_limit(cursor, table: str, column: str, value: list, cond_column: list, limit: str):
    print(f"-> Utile / Récupération id par type {cond_column} dans la table {table} avec {limit} element!")
    sql = f"SELECT {column} FROM {table} WHERE {cond_column[0]} = %s AND {cond_column[1]} = %s LIMIT {limit};"
    print(sql, [value[0], value[1]])
    cursor.execute(sql, [value[0], value[1]])
    return cursor.fetchall()


# Insertions Base de donnée: ------------------------------------------------------------------
def insert_bdd(conn, cursor, BDD: str, table: str, column: str, value: list):
    print(f"-> Utile / Insertion dans la table {table} !")
    sql = f"INSERT INTO {BDD}.{table} ({column}) VALUES (%s);"
    cursor.execute(sql, value)
    conn.commit()


def update_bdd_with_cond(mydb, cursor, table: str, column: list, value: list, cond_column: str, cond_value: str):
    print(f"-> Utile / Modification champs dans la table {table} !")
    set_col_val_str: str = update_column_str(column, value)
    sql = f"UPDATE {table} SET {set_col_val_str} WHERE {cond_column} = %s;"
    print(sql, cond_value)
    cursor.execute(sql, [cond_value])
    mydb.commit()
    return cursor.lastrowid


# Exception pour insertion Base de donnée: ------------------------------------------------------------------
def check_for_insertion(mydb, cursor, column_list: list, value_list: list, database_name: str, table_name: str, nb_table_column_id: int):
    print(f"-> Utile / Check for insertion in the table {table_name} !")
    dico: dict = list_to_dict(column_list, value_list)
    columns_many_table: str = many_column_str(dico)
    format_columns_many_table: str = format_many_column_str(dico)
    try:
        sql = f"""INSERT INTO {table_name} ({columns_many_table}) VALUES ({format_columns_many_table})"""
        print("Value: ", dico)
        print("SQL: ", sql)
        cursor.execute(sql, dico)
        mydb.commit()
    except IntegrityError as error:
        print("Except: ", error)
        sql = f"SELECT {columns_many_table} FROM {table_name} WHERE {column_list[nb_table_column_id]} = %s;"
        val = [value_list[nb_table_column_id]]
        print("Value: ", dico)
        print("SQL: ", sql)
        cursor.execute(sql, val)
        data_select = cursor.fetchall()
        return data_select
    except DataError as error_data:
        print("Erreur Inconnu: \n", error_data)
    return cursor.lastrowid



