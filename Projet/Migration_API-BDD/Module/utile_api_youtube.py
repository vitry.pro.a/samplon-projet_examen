import pandas as pd


def debug_print(chaine: str | None | pd.DataFrame , debug: bool = False):
    if debug == True:
        if chaine != None:
            print(chaine)
        else:
            print()


def create_dataframe_id_status(list_id: list):
    """ "id_element", "kind", "title", "publish_date", "description", "status" """
    columns_element = ["id_element", "kind", "title", "publish_date", "description", "status"]
    # --------------------------------------
    # Création DataFrame:
    df_id_status = pd.DataFrame(list_id, columns=[columns_element[0]])
    for col in columns_element:
        if not col.startswith("id_"):
            if col == "status":
                df_id_status[col] = 1
            else:
                df_id_status[col] = None
    return df_id_status


if __name__=="__main__":
    tool_debug = True
    nbr_résultat = {"nbr_current": 50, "nbr_total": 1000}
    data_debug = 'nbr de résultat: %s / %s' % (nbr_résultat["nbr_current"], nbr_résultat["nbr_total"])
    
    # Fonction:
    debug_print(data_debug, tool_debug)
    # Mise en forme print:    
    print('nbr de résultat: %s / %s' % (nbr_résultat["nbr_current"], nbr_résultat["nbr_total"]))