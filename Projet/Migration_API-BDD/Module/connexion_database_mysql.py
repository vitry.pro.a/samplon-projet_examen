import mysql.connector
from utile_mysql import *
import path_of_file as PF


# Création data configuration: ----------------------------------------------------------
def create_config() -> dict:
    write_JSON(PF.data_config["path_config"], PF.data_config["name_config"], PF.data_config, option_indent=4)
    return PF.data_config


# Initialisation SQL: ------------------------------------------------------------------
def init_SQL(data: dict) -> tuple:
    mydatabase: dict = create_connexion_by_bdd(mysql.connector, data["list_database"])
    mycursor: dict = create_cursor_by_bdd(data["list_database"], mydatabase, data["return_dictionnary"])
    return mydatabase, mycursor


# Création data properties: ----------------------------------------------------------
def create_properties(data: dict, mydatabase: dict, mycursor: dict) -> dict:
    bdd_properties: dict = database_properties_extraction(data["list_database"], mydatabase, mycursor, data["return_dictionnary"])
    write_JSON(data["path_config"], data["name_properties"], bdd_properties, 4)
    return bdd_properties

def database_name_in_properties(data: dict):
    database: list = []
    for key in data.keys():
        database.append(key)
    return database

def table_name_in_properties(data: dict, database_name: str):
    table: list = []
    for key in data[database_name].keys():
        table.append(key)
    return table

def column_name_in_properties(data: dict, database_name: str, table_name: str):
    return data[database_name][table_name]

if __name__=="__main__":
    # Import de module Export_file: ------------------------------------------------------------------
    import sys
        # Gestion dossier module
    sys.path.append(PF.Export_file_dir)
        # Import module
    from utile_save_data import *
    # ------------------------------------------------------------------------------------------------
    data_config: dict = create_config()
    mydb, mycursor = init_SQL(data_config)
    bdd_properties_config: dict = create_properties(data_config, mydb, mycursor)
    print(data_config)
    print(bdd_properties_config)
    database_list: list = database_name_in_properties(bdd_properties_config)
    table_list: list = table_name_in_properties(bdd_properties_config, database_list[0])
    column_list: list = column_name_in_properties(bdd_properties_config, database_list[0], table_list[0])
    print(database_list)
    print(table_list)
    print(column_list)

