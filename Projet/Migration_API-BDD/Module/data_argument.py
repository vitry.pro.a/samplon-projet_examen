# Choix des arguments en fonction des options prédéfini de l'API pour search:
"""
'--part' --> snippet
'--channelType' --> any,show
'--order' --> date,rating,relevance,title,videoCount,viewCount
'--type' --> channel,playlist,video
'--videoType' --> any, episode, movie
"""
# -----------------------------------------------------------------------------
dico_option_search: dict = {
                    "part": None,
                    "channelId": None,
                    "channelType": None,
                    "publishedAfter": None,
                    "regionCode": None,
                    "type": None,
                    "publishedBefore": None,
                    "maxResults": None,
                    "videoType": None,
                    "pageToken": None,
                    "q": None,
                    "order": None}
# Création des arguments pour la requète de recherche:
arg_search_channel = {
                    "q": '', 
                    "part": "id,snippet", 
                    "channelId": None, 
                    "channelType": "any", 
                    "maxResults": 50,
                    "pageToken": None, 
                    "order": "date", 
                    "regionCode": "FR", 
                    "type": "channel"
                    }

arg_search_video = {
                    "q": '', 
                    "part": "id,snippet",  
                    "channelId": '', 
                    "channelType": "any", 
                    "maxResults": 50,
                    "pageToken": None, 
                    "order": "date", 
                    "type": "video ",
                    "videoType": "any"
                    }

arg_search_playlist = {
                    "q": '', 
                    "part": "id,snippet", 
                    "channelId": '', 
                    "channelType": "any", 
                    "maxResults": 50,
                    "pageToken": None, 
                    "order": "date", 
                    "type": "playlist"
                    }

# -----------------------------------------------------------------------------
dico_option_channel: dict = {
                    "part": None, 
                    "hl": None, 
                    "mine": None, 
                    "mySubscribers": None, 
                    "id": None,
                    "managedByMe": None, 
                    "onBehalfOfContentOwner": None, 
                    "forUsername": None, 
                    "pageToken": None, 
                    "categoryId": None, 
                    "maxResults": None
                    }
# Création des arguments pour la requète de channel:
arg_channel = {
                "part": "id,snippet,contentDetails,statistics,topicDetails,status,brandingSettings", 
                "id": '', 
                "maxResults": 50,
                "pageToken": None
                }

# -----------------------------------------------------------------------------
dico_option_video: dict = {
                    "part": None, 
                    "hl": None, 
                    "maxWidth": None, 
                    "locale": None, 
                    "id": None, 
                    "onBehalfOfContentOwner": None, 
                    "regionCode": None, 
                    "pageToken": None, 
                    "maxResults": None, 
                    "chart": None, 
                    "myRating": None, 
                    "maxHeight": None, 
                    "videoCategoryId": None
                    }
# Création des arguments pour la requète de video:
arg_video = {
                    "part": "id,snippet,contentDetails,status,statistics,topicDetails,suggestions", 
                    "id": '', 
                    "maxResults": 50,
                    "pageToken": None
                    }

# -----------------------------------------------------------------------------
dico_option_playlist: dict = {
                    "part": None, 
                    "onBehalfOfContentOwner": None, 
                    "pageToken": None, 
                    "onBehalfOfContentOwnerChannel": None, 
                    "hl": None, 
                    "channelId": None, 
                    "mine": None, 
                    "maxResults": None, 
                    "id": None
                    }
# Création des arguments pour la requète de playlist:
arg_playlist = {
                    "part": "id,snippet,status,contentDetails", 
                    "channelId": '', 
                    "maxResults": 50,
                    "pageToken": None
                    }

# -----------------------------------------------------------------------------
dico_option_playlistItem: dict = {
                    "part": None, 
                    "onBehalfOfContentOwner": None, 
                    "pageToken": None, 
                    "playlistId": None, 
                    "videoId": None, 
                    "maxResults": None, 
                    "id": None
                    }
# Création des arguments pour la requète de playlistItem:

# Attention: pas \n dans le part, tous sur une ligne
arg_playlistItem = {
                    "part": "id,snippet,contentDetails", 
                    "playlistId": '', 
                    "maxResults": 50,
                    "pageToken": None
                    }

# -----------------------------------------------------------------------------
# Liste des options possibles search:
"""
part =                  dico_option["part"], 
channelId =             dico_option["channelId"], 
channelType =           dico_option["channelType"],  
publishedAfter =        dico_option["publishedAfter"], 
regionCode =            dico_option["regionCode"], 
type =                  dico_option["type"], 
publishedBefore =       dico_option["publishedBefore"],  
maxResults =            dico_option["maxResults"], 
videoType =             dico_option["videoType"], 
pageToken =             dico_option["pageToken"], 
q =                     dico_option["q"], 
order =                 dico_option["order"]
"""
# Liste des options possibles channel:
"""
part =                  None, 
hl=                     None, 
mine=                   None, 
mySubscribers=          None, 
id=                     None,
managedByMe=            None, 
onBehalfOfContentOwner= None, 
forUsername=            None, 
pageToken=              None, 
categoryId=             None, 
maxResults=             None
"""
# Liste des options possibles video:
"""
part=                   None, 
hl=                     None, 
maxWidth=               None, 
locale=                 None, 
id=                     None, 
onBehalfOfContentOwner= None, 
regionCode=             None, 
pageToken=              None, 
maxResults=             None, 
chart=                  None, 
myRating=               None, 
maxHeight=              None, 
videoCategoryId=        None
"""
# Liste des options possibles playlist:
"""
part=                           None, 
onBehalfOfContentOwner=         None, 
pageToken=                      None, 
onBehalfOfContentOwnerChannel=  None, 
hl=                             None, 
channelId=                      None, 
mine=                           None, 
maxResults=                     None, 
id=                             None
"""
# Liste des options possibles playlistItem:
"""
part=                   None, 
onBehalfOfContentOwner= None, 
pageToken=              None, 
playlistId=             None, 
videoId=                None, 
maxResults=             None, 
id=                     None
"""