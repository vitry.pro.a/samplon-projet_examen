from connexion_database_mysql import *
import path_of_file as PF

import sys
sys.path.append(PF.path_module)
from utile_save_data import *
from utile_api_youtube import *

# Liste de database:
"""['Projet_Examen']"""
# Liste de table:
"""['country', 'element', 'element_keywords', 'element_topics', 'keywords', 
'playlist_by_channel', 'propretie_channel', 'propretie_video', 'search', 'topics']"""
debug_tool = True

def preparation_of_connexion():
    debug_print(f"-> Lancement de la fonction Prépa_connexion BDD !", debug_tool)
    # Ouverture fichier config: ----------------------------------
    data_config: dict = read_JSON(PF.path_config, PF.file_name_config)
    # Ouverture fichier properties: ------------------------------
    bdd_properties: dict = read_JSON(data_config["path_config"], data_config["name_properties"])
    return data_config, bdd_properties


def report_insertion(type_insert, table, list_of_duplicate):
    debug_print(f"-> Création du rapport d'insertion BDD - {type_insert} !", debug_tool)
    if list_of_duplicate != []:
        if type_insert == "insert":
            write_JSON(PF.path_report, PF.file_insert_report + f"{table}" + PF.ext_name_report, list_of_duplicate, default = str)
        elif type_insert == "duplicate":
            write_JSON(PF.path_report, PF.file_dupli_report + f"{table}" + PF.ext_name_report, list_of_duplicate, default = str)
    else:
        print("Liste vide par de report nécessaire !!!", list_of_duplicate)


def adapt_to_table_value_column(row_object, list_colunm: list):
    debug_print(f"-> Lancement de la fonction mise en forme des valeurs pour la table !", debug_tool)
    list_of_value = []
    for column in list_colunm:
        if column in row_object._fields:
            if column.startswith("id"):
                list_of_value.append(getattr(row_object, f"{column}"))
            elif column.endswith("id"):
                list_of_value.append(getattr(row_object, f"{column}"))
            else:
                list_of_value.append(getattr(row_object, f"{column}"))
        else:
            print("adapt: assigne valeur None", column)
            list_of_value.append(None)
    return list_of_value


def select_in_table(dataframe: pd.DataFrame, mycursor, bdd_properties, name_database: str, name_table: str, condition_column):
    debug_print(f"-> Récupération dans la table {name_table} !", debug_tool)
    # Initialisation de variable de la fontion: ------------------------------
    list_id_bdd: list = []
    # Initialisation de liste de la table: -----------------------------------
    table_list: list = table_name_in_properties(bdd_properties, name_database)
    if name_table in table_list:
        name_of_table: str = name_table
    else:
        return print("Erreur de table !")
    column_table: list = column_name_in_properties(bdd_properties, name_database, name_table)
    # Lancement de la boucle d'insertion: ------------------------------------
    for row in dataframe.itertuples():
        # Coordination liste de la table et liste des valeurs: ---------------
        value_table = adapt_to_table_value_column(row, column_table)
        # print(column_table, value_table)
        for value in value_table:
            if value != None:
                # print(value)
                list_of_column: str = ", ".join(column_table)
                row_of_select = select_bdd_with_cond(mycursor, name_of_table, list_of_column, value, condition_column)
                tmp_list: list = [item for item_row in row_of_select for item in item_row]
                list_id_bdd.append(tmp_list)
    return list_id_bdd


def update_in_table(dataframe: pd.DataFrame, mydatabase, mycursor, bdd_properties, name_database: str, name_table: str, 
                    condition_column: str):
    debug_print(f"-> Mise à jour de la table {name_table} !", debug_tool)
    # Initialisation de variable de la fontion: ------------------------------
    list_id_bdd: list = []
    tmp_list: list = []
    # Initialisation de liste de la table: -----------------------------------
    table_list: list = table_name_in_properties(bdd_properties, name_database)
    if name_table in table_list:
        name_of_table: str = name_table
    else:
        return print("Erreur de table !")
    column_table: list = column_name_in_properties(bdd_properties, name_database, name_table)
    # Lancement de la boucle d'insertion: ------------------------------------
    for row in dataframe.itertuples():
        # Coordination liste de la table et liste des valeurs: ---------------
        value_table = adapt_to_table_value_column(row, column_table)
        column_list = []
        value_list = []
        for column, value in zip(column_table, value_table):
            print(column, value)
            if column.startswith("id_"):
                condition_value: str = value
            elif value != None and not column.startswith("id_"):
                column_list.append(column)
                value_list.append(value)
        print(condition_column, condition_value)
        row_of_select = update_bdd_with_cond(mydatabase, mycursor, name_of_table, column_list, value_list, condition_column, condition_value)
        if row_of_select != None:
            list_id_bdd.append(row_of_select)
    return list_id_bdd


def insert_in_table(dataframe: pd.DataFrame, mydatabase, mycursor, bdd_properties, name_database: str, name_table: str):
    debug_print(f"-> Insertion dans la table {name_table} !", debug_tool)
    # Initialisation de variable de la fontion: ------------------------------
    nb_column_id: int = 0 # id de la table
    list_duplicate_bdd: list = []
    list_insert_bdd: list = []
    list_id_bdd: list = []
    # Initialisation de liste de la table: -----------------------------------
    table_list: list = table_name_in_properties(bdd_properties, name_database)
    if name_table in table_list:
        name_of_table: str = name_table
    else:
        return print("Erreur de table !")
    column_table: list = column_name_in_properties(bdd_properties, name_database, name_table)
    # Lancement de la boucle d'insertion: ------------------------------------
    for row in dataframe.itertuples():
        # Coordination liste de la table et liste des valeurs: ---------------
        print(f"-- {row} --")
        value_table = adapt_to_table_value_column(row, column_table)
        print("--insert in table:--")
        print(column_table, value_table)
        print("--------------------")
        id_of_insert = check_for_insertion(mydatabase, mycursor, column_table, value_table, name_database, name_of_table, nb_column_id)
        # Vérification du retour: id ou liste de valeur
        if type(id_of_insert) == list and id_of_insert != []:
            list_duplicate_bdd.append(id_of_insert)
        elif type(id_of_insert) == str:
            list_insert_bdd.append(id_of_insert)
        list_id_bdd.append(value_table)
    # Création du rapport d'insertion: ---------------------------------------
    report_insertion("insert", name_table, list_insert_bdd)
    report_insertion("duplicate", name_table, list_duplicate_bdd)
    return list_id_bdd


def wrapper(interaction_BDD: str, table_name: str, dataframe: pd.DataFrame | None = None, 
            value: str | list | None = None, cond_column: str | list | None = None, 
            cond_limit: str | None = None):
    debug_print(f"-> Lancement de la fonction wrapper - Init BDD !", debug_tool)
    # Prépa insertion: -------------------------------------------
    data_config, bdd_properties = preparation_of_connexion()
    # Initialisation valeurs pour insertion data: ----------------
    nb_database: int = 0 #  ['Projet_Examen']
    # Récupération des data au format liste: ---------------------
    database_list: list = database_name_in_properties(bdd_properties)
    # Initialisation connexion SQL: ------------------------------
    mydb, mycursor = init_SQL(data_config)
    database_name: str = database_list[nb_database]
    cursor_Projet_Examen: str = mycursor[database_list[nb_database]]
    mydb_Projet_Examen: str = mydb[database_list[nb_database]]
    mydb[database_list[nb_database]].reconnect()
    # ------------------------------------------------------------
    if interaction_BDD == "id":
        column_table: list = column_name_in_properties(bdd_properties, database_name, table_name)
        for item in column_table:
            if item.startswith("id"): 
                list_in_bdd = select_bdd_with_cond_and_limit(cursor_Projet_Examen, table_name, item, value, 
                                                    cond_column, cond_limit)
    elif interaction_BDD == "update":
        list_in_bdd = update_in_table(dataframe, mydb_Projet_Examen, cursor_Projet_Examen, bdd_properties, 
                                        database_name, table_name, cond_column)
    elif interaction_BDD == "select":
        list_in_bdd = select_in_table(dataframe, cursor_Projet_Examen, bdd_properties, database_name, 
                                        table_name, cond_column)
    elif interaction_BDD == "insert":
        list_in_bdd = insert_in_table(dataframe, mydb_Projet_Examen, cursor_Projet_Examen, bdd_properties, 
                                        database_name, table_name)
    # ------------------------------------------------------------
    return list_in_bdd


def insert_excel_to_table():
    debug_print(f"-> Insertion de fichier Excel dans la table element !", debug_tool)
    # Prépa insertion: -------------------------------------------
    data_config, bdd_properties = preparation_of_connexion()
    # Initialisation valeurs pour insertion data: ----------------
    nb_database = 0 #  ['Projet_Examen']
    nb_table = 1
    nb_column_id = 0
    list_duplicate_bdd: list = []
    list_insert_bdd: list = []
    # Récupération des data au format liste: ---------------------
    database_list: list = database_name_in_properties(bdd_properties)
    # Initialisation connexion SQL: ------------------------------
    mydb, mycursor = init_SQL(data_config)
    database_name: str = database_list[nb_database]
    cursor_Projet_Examen = mycursor[database_list[nb_database]]
    mydb_Projet_Examen = mydb[database_list[nb_database]]
    mydb[database_list[nb_database]].reconnect()
    # Récupération des data au format liste: ---------------------
    table_list: list = table_name_in_properties(bdd_properties, database_name)
    column_table: list = column_name_in_properties(bdd_properties, database_name, table_list[nb_table])
    # Ouverture du fichier Excel à inserer dans la database: -----
    column_bdd: list = ["id", "type", "title", "date", "description"]
    # list_column_excel: list = ["title", "id", "date", "description", "id_channel", "date_import", "type"]
    # Boucle d'insertion: ----------------------------------------
    date = [11, 13, 14, 15, 16]
    for nb in date:
        file_name_data_excel: str = f"get_channel_id-search_2024-05-{nb}.xlsx"
        path_file_data_excel: str = PF.path_data_excel + file_name_data_excel
        dataframe = extract_data_excel(path_file_data_excel, sheet=0)
        dataframe["date"] = pd.to_datetime(dataframe["date"], format='%Y-%m-%dT%H:%M:%S.%f%z')
        # list_column_bdd: list = ["id", "type", "title", "date", "description", "id_channel", "date_import"]  # doublon code
        dataframe = dataframe.reindex(columns=column_bdd) #list_column_bdd)
        for row in dataframe.itertuples():
            # liste_valeur: list = [row.id, row.type, row.title, row.date, row.description]
            liste_valeur = adapt_to_table_value_column(row, column_bdd)
            id_of_insert = check_for_insertion(mydb_Projet_Examen, cursor_Projet_Examen, column_table, liste_valeur, database_list[nb_database], table_list[nb_table], nb_column_id)
            # Vérification du retour: id ou liste de valeur
            if type(id_of_insert) == list and id_of_insert != []:
                list_duplicate_bdd.append(id_of_insert)
            elif type(id_of_insert) == str:
                list_insert_bdd.append(id_of_insert)
    # Création du rapport d'insertion: ---------------------------------------
    report_insertion("insert", table_list[nb_table], list_insert_bdd)
    report_insertion("duplicate", table_list[nb_table], list_duplicate_bdd)


def insert_country_code(): # Table country retiré
    debug_print(f"-> Insertion de fichier Excel dans la table country !", debug_tool)
    dataframe = extract_data_excel(PF.path_file_code_country, sheet=0)
    dataframe = dataframe.drop(['OFFICIAL LANG CODE', 'ISO3 CODE', 
                                'ONU CODE', 'IS ILOMEMBER', 'IS RECEIVING QUEST', 
                                'LABEL EN', 'LABEL SP', 'Geo Shape', 
                                'geo_point_2d'], axis=1)
    dataframe = dataframe.rename(columns = {'ISO2 CODE': 'region_code', 'LABEL FR': 'title'})
    dataframe = dataframe.dropna(how='any', axis=0)
    return dataframe


def insert_channel_FR():
    debug_print(f"-> Insertion de fichier Excel dans la table element pour le top FR !", debug_tool)
    dataframe = extract_data_excel(PF.path_file_top_FR + PF.file_name_top_FR, sheet=0)
    dataframe = dataframe.drop(['id', 'subscribercount', 'viewcount'], axis=1)
    dataframe['country'] = dataframe['country'].replace("Франція", "France")
    dataframe['country'] = dataframe['country'].replace("France", "FR")
    dataframe = dataframe.rename(columns = {'channelId': 'id_element', 'country': 'title_country'})
    dataframe["id_element"] = dataframe['id_element'].str.replace("@","")
    dataframe["type"] = "channel"
    dataframe["publish_date"] = datetime.now().date()
    return dataframe


if __name__=="__main__":
    # Import de module Export_file: ------------------------------------------------------------------
    import sys
        # Gestion dossier module
    sys.path.append(PF.Export_file_dir)
        # Import module
    from utile_save_data import *
    # ------------------------------------------------------------------------------------------------
    # Liste de table:
    """['element', 'element_keywords', 'element_topics', 'keywords','playlist',
    'propretie_channel', 'propretie_video', 'search', 'topics']"""
    
    # --------------------------------------
    # Test de insertion de fichier excel:
    # insert_excel_to_table()
    
    # --------------------------------------
        # Table supprimé
    # Test de insertion de fichier excel pour la table country:
    # df_country = insert_country_code()
    # print(df_country)
    # country_code_insert: list = wrapper("insert", "country", dataframe=df_country)
    
    # --------------------------------------
    # Test de insertion de fichier excel pour les channels FR:
    """
    element             ['id_element', 'type', 'title', 'publish_date', 'description']
    search              ['id_element', 'element_search_id', 'kind']
    """
    
    # --------------------------------------
        # Récupération des datas Top FR excel 
    ['title', 'id_element', 'title_country', 'type', 'publish_date']
    df_channel_fr = insert_channel_FR()
    
        # Mise en forme pour la table country
    df_country = df_channel_fr[['id_element', 'title_country']]
    df_country = df_country.rename(columns = {'title_country': 'region_code'})
    
    df_element_country = df_country[['region_code']]
    df_element_country = df_element_country.rename(columns = {'region_code': 'id_element'})
    df_element_country["type"] = "pays"
    df_element_country["title"] = "France"
    print(df_element_country)
    
    # --------------------------------------
    #     # Mise en forme pour la table element
    df_element = df_channel_fr[['id_element', 'type', 'title', 'publish_date']]
    df_element = df_element.dropna(how='any', axis=0)
    
        # Insertion dans table: ['id_element', 'type', 'title', 'publish_date', 'description']
    element_insert: list = wrapper("insert", "element", dataframe=df_element)
    element_country: list = wrapper("insert", "element", dataframe=df_element_country)
    
    # --------------------------------------
    # Affichage des éléments inrérer
    df_element_insert = pd.DataFrame(element_insert, columns=['id_element', 'type', 'title', 'publish_date', 'description'])
    
    # --------------------------------------
    #     # Mise en forme pour la table search
    df1 = df_element[['id_element', 'type']]
    df2 = df_country
    df2 = df2.rename(columns = {'region_code': 'element_search_id'})
    df_search = pd.merge(df1, df2, on='id_element', how='left')
    
    df_search = df_search.rename(columns = {'id_element': 'element_id', 'region_code': 'element_search_id', 'type': 'kind'})
    df_search = df_search[['element_id', 'element_search_id', 'kind']]
    
    # --------------------------------------
        # Insertion dans table: ['id_element', 'element_search_id', 'kind']
    search_insert: list = wrapper("insert", "search", dataframe=df_search)
    
    print("-" * 25)
    print("Rapport:")
    print(df_country)
    print(df_element)
    print(df_search)
    print("-" * 25)
    print(df_element_insert)
    print(search_insert)