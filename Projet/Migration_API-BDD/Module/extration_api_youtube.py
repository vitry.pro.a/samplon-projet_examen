from datetime import datetime
from argparse import ArgumentParser

from connexion_api_youtube import *
from utile_api_youtube import *
from utile_save_data import *
from data_argument import *

debug_tool = True

def gestion_option(type_of_argument):
    debug_print(f"-> Lancement de la fonction option arguments - {type_of_argument} !", debug_tool)
    """["search_channel", "search_video", "search_playlist", "channel", "video", "playlist", "playlistItem"]"""
    if type_of_argument == "search_channel":
        arguments = arg_search_channel
    elif type_of_argument == "search_video":
        arguments = arg_search_video
    elif type_of_argument == "search_playlist":
        arguments = arg_search_playlist
    elif type_of_argument == "channel":
        arguments = arg_channel
    elif type_of_argument == "video":
        arguments = arg_video
    elif type_of_argument == "playlist":
        arguments = arg_playlist
    elif type_of_argument == "playlistItem":
        arguments = arg_playlistItem
    # Création du parseur pour les arguments:
    parser = ArgumentParser()
    for key in arguments.keys():
        if arguments[key] != None:
            parser.add_argument(f"--{key}", help=f"{key}", default=arguments[key])
        else:
            parser.add_argument(f"--{key}", help=f"{key}")
    args = parser.parse_args()
    return parser, args


def option_set_if(parser, argument, id):
    debug_print(f"-> Lancement de la fonction set id option!", debug_tool)
    for key in vars(argument):
        if key == "channelId":
            # print("channelId: ", getattr(argument, str(key)))
            parser.set_defaults(channelId=id)
            argument = parser.parse_args()
        if key == "id":
            # print("id: ", getattr(argument, str(key)))
            parser.set_defaults(id=id)
            argument = parser.parse_args()
        if key == "playlistId":
            # print("playlistId: ", getattr(argument, str(key)))
            parser.set_defaults(playlistId=id)
            argument = parser.parse_args()
    return parser, argument


def youtube_search(parser, options, type_request, nbr_résultat, dico_list_of_result):
    debug_print("-> Lanchement de la fonction youtube_search !", debug_tool)
    # Requète de recherche:
    search_response = request_API_Youtube(options, type_request)
    # Traitement du résultat JSON:
    if search_response != None:
        debug_print(f"Lecture des datas de la requète http:", debug_tool)
        # Boucle sur les dimensions du JSON:
        for head in search_response.keys():
            debug_print(f"Dimension du Json: {head}", debug_tool)
            # Vérification de la pagination:
            if "nextPageToken" in search_response.keys():
                token_tool = True
            else:
                token_tool = False
            # Incrémentation du nombre de recherche à chaque appel de la fonction récusive:
            if head == "pageInfo":
                nbr_résultat["nbr_current"] += int(search_response[head]["resultsPerPage"])
                nbr_résultat["nbr_total"] = int(search_response[head]["totalResults"])
            debug_print('Nombre de résultat en cours: %s / %s' % (nbr_résultat["nbr_current"], nbr_résultat["nbr_total"]), debug_tool)
            # Vérification du header de la requète:
            if head != "items":
                debug_print(f"Key token: {search_response[head]}", debug_tool)
                if token_tool == True and head == "nextPageToken":
                    key_token_page = search_response[head]
                    debug_print(f"Key token pour pagination: {key_token_page}", debug_tool)
                elif token_tool == False:
                    key_token_page = None
                    debug_print(f"Key token pour pagination: {key_token_page}", debug_tool)
                debug_print(None, debug_tool)
            # Vérification du corps de la requète:
            else:
                # Boucle sur les dimensions du corps (items) du JSON:
                for search_result in search_response[head]:
                    debug_print(f"-> Insertion {search_result['id']} dans une liste !", debug_tool)
                    # Recherche des vidéos:
                    if search_result['id']['kind'] == 'youtube#video':
                        type_search: str = (search_result['id']['kind']).split('#')[1]
                        dico_list_of_result["video"].append([search_result['id']['videoId'],
                                                                type_search,
                                                                search_result['snippet']['title'],
                                                                search_result['snippet']['publishedAt'],
                                                                search_result['snippet']['description'],
                                                                search_result['snippet']['channelId']])
                    # Recherche des chaines:
                    elif search_result['id']['kind'] == 'youtube#channel':
                        type_search: str = (search_result['id']['kind']).split('#')[1]
                        dico_list_of_result["channel"].append([search_result['id']['channelId'],
                                                                type_search,
                                                                search_result['snippet']['title'],
                                                                search_result['snippet']['publishedAt'],
                                                                search_result['snippet']['description'],
                                                                search_result['snippet']['channelId']])
                    # Recherche des playlists:
                    elif search_result['id']['kind'] == 'youtube#playlist':
                        type_search: str = (search_result['id']['kind']).split('#')[1]
                        dico_list_of_result["playlist"].append([search_result['id']['playlistId'],
                                                                type_search,
                                                                search_result['snippet']['title'],
                                                                search_result['snippet']['publishedAt'],
                                                                search_result['snippet']['description'],
                                                                search_result['snippet']['channelId']])
                # Vérification de la fin de la pagination:
                if key_token_page != None:
                    # print(options)
                    parser.set_defaults(pageToken=key_token_page)
                    options = parser.parse_args()
                    # print(options)
                    debug_print(None, debug_tool)
                    dico_list_of_result = youtube_search(parser, options, type_request, nbr_résultat, dico_list_of_result)
    # Affichage du nombre de résultat obtenus dans la requète:
    debug_print('Nombre de résultat traité: %s / %s' % (nbr_résultat["nbr_current"], nbr_résultat["nbr_total"]), debug_tool)
    return dico_list_of_result


def youtube_channel(parser, options, type_request, nbr_résultat, dico_list_of_result):
    debug_print("-> Lanchement de la fonction youtube_channel !", debug_tool)
    # Requète de recherche:
    print(options)
    channel_response = request_API_Youtube(options, type_request)
    print(channel_response)
    # Traitement du résultat JSON:
    if channel_response != None:
        debug_print(f"Lecture des datas de la requète http:", debug_tool)
        id_channel: str = ''
        type_channel: str = ''
        title_channel: str | None = None
        date_channel: str | None = None
        description_channel: str | None = None
        view_channel: str | None = None
        video_channel: str | None = None
        Subscriber_channel: str | None = None
        status_channel: str | None = None
        topic_channel: str | None = None
        like_channel: str | None = None
        keywords_channel: str | None  = None
        upload_channel: str | None = None
        # Boucle sur les dimensions du JSON:
        for head in channel_response.keys():
            debug_print(f"Dimension du Json: {head}", debug_tool)
            # Vérification de la pagination:
            if "nextPageToken" in channel_response.keys():
                token_tool = True
            else:
                token_tool = False
            # Incrémentation du nombre de recherche à chaque appel de la fonction récusive:
            if head == "pageInfo":
                nbr_résultat["nbr_current"] += int(channel_response[head]["resultsPerPage"])
                nbr_résultat["nbr_total"] = int(channel_response[head]["totalResults"])
            debug_print('Nombre de résultat en cours: %s / %s' % (nbr_résultat["nbr_current"], nbr_résultat["nbr_total"]), debug_tool)
            # Vérification du header de la requète:
            if head != "items":
                debug_print(f"Key token: {channel_response[head]}", debug_tool)
                if token_tool == True and head == "nextPageToken":
                    key_token_page = channel_response[head]
                    debug_print(f"Key token pour pagination: {key_token_page}", debug_tool)
                elif token_tool == False:
                    key_token_page = None
                    debug_print(f"Key token pour pagination: {key_token_page}", debug_tool)
                debug_print(None, debug_tool)
            # Vérification du corps de la requète:
            else:
                # for list_items in channel_response[head]: # items
                list_fist_lvl = ["kind", "id", "snippet", "statistics", "status", "contentDetails", "topicDetails", "brandingSettings"]
                list_second_lvl = ["title", "publishedAt", "description", "viewCount", "relatedPlaylists", "videoCount", "hiddenSubscriberCount", "subscriberCount", "privacyStatus", "topicIds", "channel"]
                list_third_lvl = ["likes", "uploads", "keywords"]
                # Boucle sur les dimensions du corps (items) du JSON:
                for channel_result in channel_response[head]: # items
                    debug_print(f"-> Insertion {channel_result['id']} dans une liste !", debug_tool)
                    # -----------------------------------------------------------------------------
                    for first_key in channel_result.keys():
                        # print("<>", first_key)
                        if first_key in list_fist_lvl:
                            # -----------------------
                            id_channel = channel_result['id']
                            type_channel = (channel_result['kind']).split('#')[1]
                            # -----------------------
                            if type(channel_result[first_key]) == dict:
                                for second_key in channel_result[first_key]:
                                    # print("<><>", second_key)
                                    if second_key in list_second_lvl:
                                        # -----------------------
                                        if "title" in channel_result[first_key].keys():
                                            title_channel = channel_result['snippet']['title']
                                        if "publishedAt" in channel_result[first_key].keys():
                                            date_channel = channel_result['snippet']['publishedAt']
                                        if "description" in channel_result[first_key].keys():
                                            description_channel = channel_result['snippet']['description']
                                        if "viewCount" in channel_result[first_key].keys():
                                            view_channel = channel_result['statistics']["viewCount"]
                                        if "videoCount" in channel_result[first_key].keys():
                                            video_channel = channel_result['statistics']["videoCount"]
                                        if "subscriberCount" in channel_result[first_key].keys():
                                            Subscriber_channel = channel_result['statistics']["subscriberCount"]
                                        if "privacyStatus" in channel_result[first_key].keys():
                                            status_channel = channel_result['status']["privacyStatus"]
                                        if "topicIds" in channel_result[first_key].keys():
                                            topic_channel = channel_result['topicDetails']["topicIds"]
                                        # -----------------------
                                        if type(channel_result[first_key][second_key]) == dict:
                                            for third_key in channel_result[first_key][second_key]:
                                                # print("<><><>", third_key)
                                                if third_key in list_third_lvl:
                                                    # -----------------------
                                                    if "likes" in channel_result[first_key][second_key].keys():
                                                        like_channel = channel_result['contentDetails']["relatedPlaylists"]["likes"]
                                                    if "uploads" in channel_result[first_key][second_key].keys():
                                                        upload_channel = channel_result['contentDetails']["relatedPlaylists"]["uploads"]  # Id playlist
                                                    if "keywords" in channel_result[first_key][second_key].keys():
                                                        keywords_channel = (channel_result['brandingSettings']["channel"]["keywords"]).replace('"', '').split(' ')
                    # -----------------------------------------------------------------------------
                    # Insertion des résultats:
                    dico_list_of_result["result"].append([id_channel,
                                                            type_channel,
                                                            title_channel,
                                                            date_channel,
                                                            description_channel,
                                                            view_channel,
                                                            like_channel,
                                                            video_channel,
                                                            Subscriber_channel,
                                                            status_channel,
                                                            upload_channel,
                                                            keywords_channel, 
                                                            topic_channel
                                                            ])
                # Vérification de la fin de la pagination:
                if key_token_page != None:
                    print(options)
                    parser.set_defaults(pageToken=key_token_page)
                    options = parser.parse_args()
                    print(options)
                    debug_print(None, debug_tool)
                    dico_list_of_result = youtube_channel(parser, options, type_request, nbr_résultat, dico_list_of_result)
        # ------------------------------------------
        if 'items' not in channel_response.keys():
            print(">>>>> Corps de la requète vide !!!!")
            default_data: list = [getattr(options, "id"), ((channel_response['kind']).split('#')[1]).split('List')[0], 
                            title_channel, date_channel, description_channel, view_channel, like_channel, video_channel, 
                            Subscriber_channel, status_channel, upload_channel, keywords_channel, 
                            topic_channel]
            dico_list_of_result["result"].append(default_data)
        # ------------------------------------------
    # Affichage du nombre de résultat obtenus dans la requète:
    debug_print('Nombre de résultat traité: %s / %s' % (nbr_résultat["nbr_current"], nbr_résultat["nbr_total"]), debug_tool)
    return dico_list_of_result

def youtube_video(parser, options, type_request, nbr_résultat, dico_list_of_result):
    pass

def youtube_playlist(parser, options, type_request, nbr_résultat, dico_list_of_result):
    pass

def youtube_playlistItem(parser, options, type_request, nbr_résultat, dico_list_of_result):
    pass


def quotas_of_request(type_of_request, nbr_of_request, parser, arguments, nbr_résultat, list_of_result):
    """["search_channel", "search_video", "search_playlist", "channel", "video", "playlist", "playlistItem"]"""
    for quotas in range(0, nbr_of_request):
        debug_print(f"Numéro de la requète en cours: {quotas}", debug_tool)
        if type_of_request in ["search_channel", "search_video", "search_playlist"]:
            list_of_result = youtube_search(parser, arguments, type_of_request, nbr_résultat, list_of_result)
        elif type_of_request == "channel":
            list_of_result = youtube_channel(parser, arguments, type_of_request, nbr_résultat, list_of_result)
        elif type_of_request == "video":
            list_of_result = youtube_video(parser, arguments, type_of_request, nbr_résultat, list_of_result)
        elif type_of_request == "playlist":
            list_of_result = youtube_playlist(parser, arguments, type_of_request, nbr_résultat, list_of_result)
        elif type_of_request == "playlistItem":
            list_of_result = youtube_playlistItem(parser, arguments, type_of_request, nbr_résultat, list_of_result)
    return list_of_result


def export(path: str, list_result: dict | list, type_of_export):
    debug_print(f"-> Lancement de la fonction export {type_of_export} !", debug_tool)
    """["search_channel", "search_video", "search_playlist", "channel", "video", "playlist", "playlistItem"]"""
    for dico in list_result.keys():
        if list_result[dico] != []:
            print(f"{dico}:")
            insert_data_excel(path, f"API_extract_{dico}_{datetime.now().date()}.xlsx", list_result[dico])
        else:
            # insert_data_excel(path, f"API_extract_{dico}_{datetime.now().date()}.xlsx", list_result[dico])
            print(f"{dico}: []")


def request_list(path, type_of_request, list_id=None):
    debug_print(f"-> Lancement de la fonction request_{type_of_request} !", debug_tool)
    """["search_channel", "search_video", "search_playlist", "channel", "video", "playlist", "playlistItem"]"""
    # Création du conteneur du nombre de résultats pour la requète de recherche:
    nbr_resultat = {"nbr_current": 0, "nbr_total": 0}
    dico_of_result: dict = {}
    # Création des arguments pour la requète de recherche:
    parser, arguments = gestion_option(type_of_request)
    # print("Vérification, argument", arguments)
    if type_of_request in ["search_video", "search_playlist", "channel", "video", "playlist", "playlistItem"] and list_id != None:
        # Création du conteneur des résultats pour la requète "search_video", "search_playlist", "channel", "video", "playlist", "playlistItem":
        dico_of_result.update({"video": [], "playlist": [], "result": []})
        for id in list_id:
            parser, arguments = option_set_if(parser, arguments, id)
            print(f"Modification {id} argument", arguments)
            # Requète de channel pour les chaines youtube:
            dico_of_result = quotas_of_request(type_of_request, 10, parser, arguments, nbr_resultat, dico_of_result)
            # dico_of_result["video"].append(f"test {id}") # valeur test
        # Insertion et affichage des résultats:
        export(path, dico_of_result, type_of_request)
    elif type_of_request in ["search_channel", "search_video", "search_playlist"] and list_id == None:
        # Création du conteneur des résultats pour la requète de recherche:
        dico_of_result.update({"channel": [], "video": [], "playlist": []})
        # Requète de recherche pour les chaines youtube vu en France:
        dico_of_result = quotas_of_request(type_of_request, 1, parser, arguments, nbr_resultat, dico_of_result)
        # Insertion et affichage des résultats:
        export(path, dico_of_result, type_of_request)
    return dico_of_result


if __name__ == '__main__':
    # parser, args_search = gestion_option("search_channel")
    # print(args_search)
    # parser, args_channel = gestion_option("channel")
    # print(args_channel)
    
    list_of_id: list = ['UCu8imFFd1IQQMk6znGL481w', "aleksytb"]
    path_export = '/home/antony/Projet_Examen/Projet/Ressources/DATA/Export_via_BDD/'
        # Type de requète possible:
    """["search_channel", "search_video", "search_playlist", "channel", "video", "playlist", "playlistItem"]"""
    dico_search = request_list(path_export, "channel", list_of_id)
    print("Dico recherche: ", dico_search)