import pandas as pd
import numpy as np
import sys, os

sys.path.append("Projet/Migration_API-BDD/Module")
from Module.connexion_api_youtube import *
from Module.extration_api_youtube import *
from Module.connexion_database_mysql import *
from Module.insertion_database_mysql import *
from Module.utile_api_youtube import *
from Module.utile_save_data import *

debug_tool = True

def transfert_ExceltopFR_to_BDD():
    debug_print("### Start migration ExceltopFR ###", debug_tool)
    # Récupération des datas Top FR excel 
    """['title', 'id_element', 'title_country', 'type', 'publish_date']"""
    df_channel_fr = insert_channel_FR()
    # --------------------------------------
    # Mise en forme pour la table country
    df_country = df_channel_fr[['id_element', 'title_country']]
    df_country = df_country.rename(columns = {'title_country': 'region_code'})
    # --------------------------------------
    # Mise en forme pour la table element
    df_element_country = df_country[['region_code']]
    df_element_country = df_element_country.rename(columns = {'region_code': 'id_element'})
    df_element_country["type"] = "pays"
    df_element_country["title"] = "France"
    # --------------------------------------
    # Mise en forme pour la table element
    df_element = df_channel_fr[['id_element', 'type', 'title', 'publish_date']]
    df_element = df_element.dropna(how='any', axis=0)
    # --------------------------------------
    # Insertion dans table: 
    """['id_element', 'type', 'title', 'publish_date', 'description', 'status']"""
    element_insert: list = wrapper("insert", "element", dataframe=df_element)
    element_country: list = wrapper("insert", "element", dataframe=df_element_country)
    # --------------------------------------
    # Création dataframe search
    df1 = df_element[['id_element', 'type']]
    df2 = df_country
    df2 = df2.rename(columns = {'region_code': 'element_search_id'})
    df_search = pd.merge(df1, df2, on='id_element', how='left')
    # --------------------------------------
    # Mise en forme pour la table search
    df_search = df_search.rename(columns = {'id_element': 'element_id', 'region_code': 'element_search_id', 'type': 'kind'})
    df_search = df_search[['element_id', 'element_search_id', 'kind']]
    # --------------------------------------
    # Insertion dans table: ['id_element', 'element_search_id', 'kind']
    search_insert: list = wrapper("insert", "search", dataframe=df_search)
    # --------------------------------------
    debug_print("### Fin migration ExceltopFR ###", debug_tool)


def transfert_Excel_to_BDD(type_of_file: str):
    debug_print("### Start migration Excel ###", debug_tool)
    path_test = '/home/antony/Projet_Examen/Projet/Ressources/DATA/Import_via_API'
    list_dossier = os.listdir(path_test + "/" + type_of_file + "/")
    for file in list_dossier:
        print(path_test + "/" + type_of_file + "/" + file)
        if type_of_file.startswith("search"):
            if type_of_file.split("_")[1] == "channel":
                columns = ["title", "id_element", "publish_date", "description"]
                dataframe = extract_data_excel(path_test + "/" + type_of_file + "/" + file, sheet=0, header=None, names=columns)
                dataframe["kind"] = type_of_file.split("_")[1]
                dataframe["element_search_id"] = dataframe["id_element"]
                columns_search = ["id_element", "kind", "title", "publish_date", "description", "element_search_id"]
                dataframe = dataframe[columns_search]
                dataframe["description"] = dataframe["description"].fillna("")
                print(dataframe)
                transfert_Search_to_BDD(dataframe, shunt=True)
            elif type_of_file.split("_")[1] == "video":
                columns = ["title", "id_element", "publish_date", "description"]
                print("Fonction video à faire !!!")
        elif type_of_file == "result":
            columns = ["id_element", "type", "title", "publish_date", "description", 
                            "viewCount", "likesCount", "videoCount", "subscriberCount", 
                            "privacyStatus", "playlist_id", "keyword", "topic"]
            dataframe = extract_data_excel(path_test + "/" + type_of_file + "/" + file, sheet=0, header=None, names=columns)
            dataframe["likesCount"] = dataframe["likesCount"].fillna("")
            dataframe["subscriberCount"] = dataframe["subscriberCount"].fillna(np.nan).replace([np.nan], [None])
            dataframe["videoCount"] = dataframe["videoCount"].fillna(np.nan).replace([np.nan], [None])
            dataframe["viewCount"] = dataframe["viewCount"].fillna(np.nan).replace([np.nan], [None])
            dataframe['publish_date'] = pd.to_datetime(dataframe['publish_date'])
            print(dataframe)
            # for row in dataframe.itertuples():
            #     print(row)
            #     print()
            transfert_Channel_to_BDD(dataframe, shunt=True)
        # break
    # --------------------------------------
    debug_print("### Fin migration Excel ###", debug_tool)


def transfert_Search_to_BDD(dataframe: pd.DataFrame = None, shunt: bool = False):
    debug_print("### Start migration Search ###", debug_tool)
    # Appel fonction sans requète http: ---------------------------------------
    if shunt == False:
        # {"video": [], "playlist": [], "result": []} or {"channel": [], "video": [], "playlist": []}
        list_channel = []
        list_video = []
        list_playlist = []
        list_result = []
        # Récupération des listes dans un dictionnaire
        path_export = '/home/antony/Projet_Examen/Projet/Ressources/DATA/Export_via_BDD/'
            # Type de requète possible:
        """["search_channel", "search_video", "search_playlist", "channel", "video", "playlist", "playlistItem"]"""
        dico_search = request_list(path_export, "search_channel")
        # --------------------------------------
        columns_search = ["id_element", "kind", "title", "publish_date", "description", "element_search_id"]
        # --------------------------------------
        # Keys: video, channel, playlist
        for list_type in dico_search.keys():
            if dico_search[list_type] != []:
                if list_type == "channel":
                    list_channel = dico_search[list_type]
                    # Création DataFrame:
                    df_request = pd.DataFrame(list_channel, columns=columns_search)
                if list_type == "video":
                    list_video = dico_search[list_type]
                    # Création DataFrame:
                    df_request = pd.DataFrame(list_video, columns=columns_search)
                if list_type == "playlist":
                    list_playlist = dico_search[list_type]
                    # Création DataFrame:
                    df_request = pd.DataFrame(list_playlist, columns=columns_search)
                if list_type == "result":
                    list_result = dico_search[list_type]
                    # Création DataFrame:
                    df_request = pd.DataFrame(list_result, columns=columns_search)
            else:
                # --------------------------------------
                print(f"Le type {list_type} contient une liste vide: ", dico_search[list_type])
    else:
        df_request = dataframe
    # -------------------------------------------------------------------------
    df_request['publish_date'] = pd.to_datetime(df_request['publish_date'])
    # --------------------------------------
    # Modication des dataframes:
    df_element = df_request[['id_element', 'kind', 'title', 'publish_date', 'description', 'status']]
    df_element = df_element.rename(columns = {'kind': 'type'})
    df_element["status"] = 0
    df_search = df_request[['id_element', 'element_search_id', 'kind']]
    df_search = df_search.rename(columns = {'id_element': 'element_id'})
    # --------------------------------------
    # Insertion dans la table:
    """element             ['id_element', 'type', 'title', 'publish_date', 'description', 'status']"""
    list_element: list = wrapper("insert", "element", dataframe=df_element)
    # --------------------------------------
    # Insertion dans la table:
    """search              ['element_id', 'element_search_id', 'kind']"""
    list_search: list = wrapper("insert", "search", dataframe=df_search)
    # --------------------------------------
    print()
    # --------------------------------------
    debug_print("### Fin migration Search ###", debug_tool)


def transfert_Channel_to_BDD(dataframe: pd.DataFrame | None = None, shunt: bool = False):
    debug_print("### Start migration Channel ###", debug_tool)
    # Appel fonction sans requète http: ---------------------------------------
    if shunt == False:
        path_export = '/home/antony/Projet_Examen/Projet/Ressources/DATA/Export_via_BDD/'
        list_result = []
        # Récupération de l'ID channel pour insertion requète
            # 1 - Récupération liste id de la BDD
        list_of_id: list = []
        list_id: list = wrapper("id", "element", cond_column=["type", "status"], value=["channel", 0], cond_limit=500)
        # Formatage tuple en liste:
        for item in list_id:
            list_of_id.append(item[0])
            # 2 - Récupération data channel via request_list()
        """["search_channel", "search_video", "search_playlist", "channel", "video", "playlist", "playlistItem"]"""
        dico_channel = request_list(path_export, "channel", list_of_id)
        #     # 3 - Traitement du status en fonction de l'id
        df_id_status = create_dataframe_id_status(list_of_id)
        list_status: list = wrapper("update", "element", dataframe=df_id_status, cond_column="id_element")
        """id: str, type: str, title: str, date: date, description: str, view: int, like: str ???, 
        video: int, Subscriber: int, status: str, upload: str, keywords: list, topic: list"""
        # Keys: video, playlist, result
        for list_type in dico_channel.keys():
            if dico_channel[list_type] != []:
                if list_type == "result":
                    list_result = dico_channel[list_type]
            else:
                print(f"Le type channel contient une liste vide: ", dico_channel)
        column_result: list = ["id_element", "type", "title", "publish_date", "description", 
                            "viewCount", "likesCount", "videoCount", "subscriberCount", 
                            "privacyStatus", "playlist_id", "keyword", "topic"]
        # Create DataFrame:
        df_result = pd.DataFrame(list_result, columns=column_result)
    else:
        df_result = dataframe
    # -------------------------------------------------------------------------
    df_result['publish_date'] = pd.to_datetime(df_result['publish_date'])
    df_result = df_result.replace({pd.NaT: None})
    df_result['description'] = df_result['description'].str.replace("'", " ", regex=True)
    df_result['description'] = df_result['description'].str.replace('"', " ", regex=True)
    df_result['title'] = df_result['title'].str.replace("'", " ", regex=True)
    df_result['title'] = df_result['title'].str.replace('"', " ", regex=True)
    # --------------------------
    # Modication des dataframes:
    # >>> Pas de gestion du status car réinit à 0 sinon
    df_element = df_result[['id_element', 'type', 'title', 'publish_date', 'description']]
    df_element['title'] = df_element['title'].str.replace("\\", " ", regex=True)
    # --------------------------
    df_proprety = df_result[['id_element', 'viewCount', 'likesCount', 'videoCount', 'subscriberCount', 'privacyStatus']]
    df_proprety = df_proprety.rename(columns = {'id_element': 'element_id'})
    # Gestion champs privacyStatus vide en "private"
    df_proprety['privacyStatus'] = df_proprety['privacyStatus'].fillna("private")
    df_proprety_private = df_proprety[(df_result['viewCount'].isnull()) & 
                                    (df_result['likesCount'].isnull()) & 
                                    (df_result['videoCount'].isnull()) & 
                                    (df_result['subscriberCount'].isnull())]
    df_proprety = df_proprety.dropna(subset=["viewCount", "likesCount", "videoCount", "subscriberCount"], how='all')
    df_proprety_private = df_proprety_private.drop(['viewCount', 'likesCount', 'videoCount', 'subscriberCount'], axis=1)
    # --------------------------
    df_playlist_element = df_result[['playlist_id']]
    df_playlist_element["status"] = 0
    df_playlist_element.insert(loc=1, column='type', value="playlist")
    df_playlist_element = df_playlist_element.rename(columns = {'playlist_id': 'id_element'})
    # --------------------------
    df_playlist_search = df_result[['id_element', 'playlist_id']]
    df_playlist_search = df_playlist_search.rename(columns = {'playlist_id': 'element_id'})
    df_playlist_search = df_playlist_search.rename(columns = {'id_element': 'element_search_id'})
    # Gestion champs kind vide en "playlist"
    df_playlist_search.insert(loc=1, column='kind', value="playlist")
    # --------------------------
    df_keyword = df_result[['id_element', 'keyword']]
    df_keyword = df_keyword.rename(columns = {'id_element': 'element_id'})
    df_keyword = df_keyword.explode('keyword')
    df_keyword['keyword'] = df_keyword['keyword'].str.replace("'", " ", regex=True)
    # --------------------------
    df_topic = df_result[['id_element', 'topic']]
    df_topic = df_topic.rename(columns = {'id_element': 'element_id'})
    df_topic = df_topic.explode('topic')
    df_topic['topic'] = df_topic['topic'].str.replace("'", " ", regex=True)
    # --------------------------
    # Update dans la table:
    """element              ['id_element', 'type', 'title', 'publish_date', 'description', 'status']"""
    list_update_element: list = wrapper("update", "element", dataframe=df_element, cond_column="id_element")
    # Insertion dans la table:
    """proprety             ["element_id", "viewCount", "likesCount", "videoCount", "commentCount", 
                            "duration", "subscriberCount", "privacyStatus"]"""
    list_proprety_public: list = wrapper("insert", "proprety", dataframe=df_proprety)
    list_proprety_private: list = wrapper("insert", "proprety", dataframe=df_proprety_private)
    # Insertion dans la table:
    """keywords             ["element_id", "keyword"]"""
    list_keywords: list = wrapper("insert", "keywords", dataframe=df_keyword)
    # Insertion dans la table:
    """topics               ["element_id", "topic"]"""
    list_topics: list = wrapper("insert", "topics", dataframe=df_topic)
    # Insertion dans la table:
    """element              ['id_element', 'type', 'title', 'publish_date', 'description', 'status']"""
    list_search: list = wrapper("insert", "element", dataframe=df_playlist_element)
    # Insertion dans la table:
    """search               ["element_id", "element_search_id", "kind"]"""
    list_search: list = wrapper("insert", "search", dataframe=df_playlist_search)
    # print(df_id_status)
    print()
    debug_print("### Fin migration channel ###", debug_tool)


if __name__ == "__main__":
    # Insertion MySQL Database
    all: bool = False
    topFR: bool = False
    excel: bool = False
    search_in_bdd: bool = False
    channel_in_bdd: bool = True
    warpper_id: bool = False
    
    if topFR == True or all == True:
        transfert_ExceltopFR_to_BDD()
    
    if excel == True or all == True:
        """["result", "search_channel", "search_video", "search_playlist", "channel", "video", "playlist", "playlistItem"]"""
        sous_dossier = "result"
        transfert_Excel_to_BDD(sous_dossier)
    
    if search_in_bdd == True or all == True:
        transfert_Search_to_BDD()
    
    if channel_in_bdd == True or all == True:
        for nb in range(0, 1):
            transfert_Channel_to_BDD()
            print(f"#### Requète N°{nb} terminé ! ####")
    
    if warpper_id == True or all == True:
        list_of_id: list = []
        list_id: list = wrapper("id", "element", cond_column="type", value="channel", cond_limit=20)
        for item in list_id:
            list_of_id.append(item[0])
        print(list_of_id)
