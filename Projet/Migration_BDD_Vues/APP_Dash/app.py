import dash
import dash_bootstrap_components as dbc
from dash import Dash, html, dcc

BS = "https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css"

app = Dash(__name__, use_pages=True, external_stylesheets=[BS, dbc.icons.FONT_AWESOME])


app.layout = html.Div(children=[
    dbc.Row(children=[
        
        dbc.Col(children=[
            
            html.H2('Navigation:', style={"border": "1px black solid", 'textAlign': 'center', 'backgroundColor':'#ec7063'}),
            html.Div([
                
                dcc.Link(f"-[{page['name']}]", href=page["relative_path"], style={'columnCount': 1}) 
                for page in dash.page_registry.values()
                
                ])
            
            ]),
        
        dbc.Col(children=[
            
            html.H1('Projet: Youtube Statistique', style={"border": "1px black solid", 'height':"10vh", 'textAlign': 'center', 'backgroundColor':'#ec7063'})
            
            ], align='bottom', width={'size': 9}, style={"border": "1px black solid", 'backgroundColor':' #f1948a'})
        
        ], justify = "center", style={'backgroundColor':'#ff3f3f', 'height':"12vh"}),
    
    dbc.Row(children=[
        
        dash.page_container
        
        ], justify = "center", style={"border": "1px black solid"})
    
], style={'backgroundColor': '#e8f8f5'})




if __name__ == '__main__':
    app.run(debug=True)