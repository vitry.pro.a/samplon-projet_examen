import sys
import dash
import plotly.express as px
import dash_bootstrap_components as dbc
from dash.exceptions import PreventUpdate
from dash import html, dcc, callback, Input, Output, Patch

sys.path.append("Projet/Migration_BDD_Vues/APP_Dash/Module")
from Module.connexion_database_mysql import * 
from Module.utile_mysql import * 
import Module.path_of_file as data_config


# Link de la page du menu
dash.register_page(__name__, path='/')


# Fonctions
def show_dataframe(table: str, properties_config, db, cursor, column_list):
    # column_list: list = column_name_in_properties(properties_config, "Projet_Examen", table)
    # Récupération des data dans la BDD
    column_str: str = ", ".join(column_list)
    db["Projet_Examen"].reconnect()
    list_table = select_bdd(cursor["Projet_Examen"], table, column_str)
    print("Mise à jour Data:")
    db["Projet_Examen"].close()
    # Mise en forme des datas
    df = pd.DataFrame(list_table, columns=column_list)
    df = df.sort_values(by=["subscriberCount"], axis=0, ascending = False)
    # print(df)
    return df


def graphique_dataframe(table: str, column: str, properties_config, db, cursor, column_list):
    # column_list: list = column_name_in_properties(properties_config, "Projet_Examen", table)
    # Récupération des data dans la BDD
    db["Projet_Examen"].reconnect()
    list_table = select_bdd_order_by_cond(cursor["Projet_Examen"], table, column_list[0], column)
    print("Mise à jour Data:")
    db["Projet_Examen"].close()
    # Mise en forme des datas
    df = pd.DataFrame(list_table, columns=[column_list[0], column])
    df = df.sort_values(by=[column], axis=0, ascending = False)
    df = df.iloc[:15]
    # print(df)
    return df

columns_list = ["element_id",
                "viewCount",
                "likesCount",
                "videoCount",
                "commentCount",
                "subscriberCount",
                "duration",
                "privacyStatus"]

# Variables
database = "Projet_Examen"
table = "proprety"

# Connexion à la database
# Erreur à corriger, fichier lecture json
mydb, mycursor = init_SQL(data_config)
bdd_properties_config: dict = create_properties(data_config, mydb, mycursor)
database_list: list = database_name_in_properties(bdd_properties_config)
list_radio = table_name_in_properties(bdd_properties_config, database_list[0])
col_list = column_name_in_properties(bdd_properties_config, database, table)

# Initialisation du dataframe
dataframe = show_dataframe(table, bdd_properties_config, mydb, mycursor, columns_list)
df_1 = graphique_dataframe(table, columns_list[5], bdd_properties_config, mydb, mycursor, columns_list)
df_2 = graphique_dataframe(table, columns_list[1], bdd_properties_config, mydb, mycursor, columns_list)
df_3 = graphique_dataframe(table, columns_list[3], bdd_properties_config, mydb, mycursor, columns_list)
df_4 = graphique_dataframe(table, columns_list[2], bdd_properties_config, mydb, mycursor, columns_list)
df_5 = graphique_dataframe(table, columns_list[4], bdd_properties_config, mydb, mycursor, columns_list)
df_6 = graphique_dataframe(table, columns_list[6], bdd_properties_config, mydb, mycursor, columns_list)

# Initialisation des figures
fig_1 = px.bar(df_1, x=df_1[columns_list[0]], y=df_1[columns_list[5]], color='subscriberCount', text_auto='.2s')
fig_1.update_traces(textfont_size=12, textangle=0, textposition="outside", cliponaxis=False)
fig_1.add_shape(type="line", line_color="salmon", line_width=3, 
                opacity=1, line_dash="dot", xref="paper", 
                x0=0, x1=1, y0='shape-1-output', y1='shape-1-output', yref="y")

fig_2 = px.bar(df_2, x=df_2[columns_list[0]], y=df_2[columns_list[1]], color='viewCount', text_auto='.2s')
fig_2.update_traces(textfont_size=12, textangle=0, textposition="outside", cliponaxis=False)
fig_2.add_shape(type="line", line_color="salmon", line_width=3, 
                opacity=1, line_dash="dot", xref="paper", 
                x0=0, x1=1, y0='shape-2-output', y1='shape-2-output', yref="y")

fig_3 = px.bar(df_3, x=df_3[columns_list[0]], y=df_3[columns_list[3]], color='videoCount', text_auto='.2s')
fig_3.update_traces(textfont_size=12, textangle=0, textposition="outside", cliponaxis=False)
fig_3.add_shape(type="line", line_color="salmon", line_width=3, 
                opacity=1, line_dash="dot", xref="paper", 
                x0=0, x1=1, y0='shape-3-output', y1='shape-3-output', yref="y")

fig_4 = px.bar(df_4, x=df_4[columns_list[0]], y=df_4[columns_list[2]], color='likesCount', text_auto='.2s')
fig_4.update_traces(textfont_size=12, textangle=0, textposition="outside", cliponaxis=False)
fig_4.add_shape(type="line", line_color="salmon", line_width=3, 
                opacity=1, line_dash="dot", xref="paper", 
                x0=0, x1=1, y0='shape-4-output', y1='shape-4-output', yref="y")

fig_5 = px.bar(df_5, x=df_5[columns_list[0]], y=df_5[columns_list[4]], color='commentCount', text_auto='.2s')
fig_5.update_traces(textfont_size=12, textangle=0, textposition="outside", cliponaxis=False)
fig_5.add_shape(type="line", line_color="salmon", line_width=3, 
                opacity=1, line_dash="dot", xref="paper", 
                x0=0, x1=1, y0='shape-5-output', y1='shape-5-output', yref="y")

fig_6 = px.bar(df_6, x=df_6[columns_list[0]], y=df_6[columns_list[6]], color='duration', text_auto='.2s')
fig_6.update_traces(textfont_size=12, textangle=0, textposition="outside", cliponaxis=False)
fig_6.add_shape(type="line", line_color="salmon", line_width=3, 
                opacity=1, line_dash="dot", xref="paper", 
                x0=0, x1=1, y0='shape-6-output', y1='shape-6-output', yref="y")

# Mise en forme de la page
layout = html.Div(children=[
    
    html.Div([
        
        html.H1('Comparaison du Top 15 avec 1 chaine au choix', style={'textAlign': 'center'}),
        # df.columns
        dcc.Dropdown(dataframe["element_id"], id='pandas-dropdown', placeholder="Select une chaine Youtube", searchable=False, clearable=False),
        
        html.Div(id='dd-output-container')
        ]),
    
    dbc.Row(children=[
        
        dbc.Col(children=[
            
            dcc.Graph(figure=fig_1, id='shape-1-output')
            ]),
        
        dbc.Col(children=[
            
            dcc.Graph(figure=fig_2, id='shape-2-output')
            ])
        ]),
    
    dbc.Row(children=[
        
        dbc.Col(children=[
            
            dcc.Graph(figure=fig_3, id='shape-3-output')
            ]),
        
        dbc.Col(children=[
            
            dcc.Graph(figure=fig_4, id='shape-4-output')
            ])
        ]),
    
    dbc.Row(children=[
        
        dbc.Col(children=[
            
            dcc.Graph(figure=fig_5, id='shape-5-output')
            ]),
        
        dbc.Col(children=[
            
            dcc.Graph(figure=fig_6, id='shape-6-output')
            ])
        ])
    ])

# Callback
@callback(
    Output('dd-output-container', 'children'),
    Input('pandas-dropdown', 'value'))
def update_output(value):
    if value is None:
        raise PreventUpdate

    for row in dataframe.itertuples():
        if value == getattr(row, "element_id"):
            text_val = row
    
    text = f'Votre sélection : \n {text_val}'
    
    return text

@callback(
    Output('shape-1-output', 'figure'),
    Input('pandas-dropdown', 'value'))
def update_fig_1(value):
    if value is None:
        raise PreventUpdate

    # Sélection de la chaine courante
    for row in dataframe.itertuples():
        if value == getattr(row, "element_id"):
            y_fig_1 = getattr(row, "subscriberCount")
    
    # Update Figure
    patched_figure = Patch()
    patched_figure['layout']['shapes'][0].update({
        "y0": y_fig_1, "y1": y_fig_1})
    
    return patched_figure

@callback(
    Output('shape-2-output', 'figure'),
    Input('pandas-dropdown', 'value'))
def update_fig_2(value):
    if value is None:
        raise PreventUpdate

    # Sélection de la chaine courante
    for row in dataframe.itertuples():
        if value == getattr(row, "element_id"):
            y_fig_2 = getattr(row, "viewCount")
    
    # Update Figure
    patched_figure = Patch()
    patched_figure['layout']['shapes'][0].update({
        "y0": y_fig_2, "y1": y_fig_2})
    
    return patched_figure

@callback(
    Output('shape-3-output', 'figure'),
    Input('pandas-dropdown', 'value'))
def update_fig_3(value):
    if value is None:
        raise PreventUpdate

    # Sélection de la chaine courante
    for row in dataframe.itertuples():
        if value == getattr(row, "element_id"):
            y_fig_3 = getattr(row, "videoCount")
    
    # Update Figure
    patched_figure = Patch()
    patched_figure['layout']['shapes'][0].update({
        "y0": y_fig_3, "y1": y_fig_3})
    
    return patched_figure

@callback(
    Output('shape-4-output', 'figure'),
    Input('pandas-dropdown', 'value'))
def update_fig_4(value):
    if value is None:
        raise PreventUpdate

    # Sélection de la chaine courante
    for row in dataframe.itertuples():
        if value == getattr(row, "element_id"):
            y_fig_4 = getattr(row, "likesCount")
    
    # Update Figure
    patched_figure = Patch()
    patched_figure['layout']['shapes'][0].update({
        "y0": y_fig_4, "y1": y_fig_4})
    
    return patched_figure


@callback(
    Output('shape-5-output', 'figure'),
    Input('pandas-dropdown', 'value'))
def update_fig_5(value):
    if value is None:
        raise PreventUpdate

    # Sélection de la chaine courante
    for row in dataframe.itertuples():
        if value == getattr(row, "element_id"):
            y_fig_5 = getattr(row, "commentCount")
    
    # Update Figure
    patched_figure = Patch()
    patched_figure['layout']['shapes'][0].update({
        "y0": y_fig_5, "y1": y_fig_5})
    
    return patched_figure


@callback(
    Output('shape-6-output', 'figure'),
    Input('pandas-dropdown', 'value'))
def update_fig_6(value):
    if value is None:
        raise PreventUpdate

    # Sélection de la chaine courante
    for row in dataframe.itertuples():
        if value == getattr(row, "element_id"):
            y_fig_6 = getattr(row, "duration")
    
    # Update Figure
    patched_figure = Patch()
    patched_figure['layout']['shapes'][0].update({
        "y0": y_fig_6, "y1": y_fig_6})
    
    return patched_figure

if __name__=="__main__":
    # print(dataframe)
    pass