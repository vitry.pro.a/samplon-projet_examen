import sys
import dash
import pandas as pd
import dash_bootstrap_components as dbc
from dash.exceptions import PreventUpdate
from dash import html, dcc, callback, Input, Output, dash_table

sys.path.append("Projet/Migration_BDD_Vues/APP_Dash/Module")
from Module.connexion_database_mysql import * 
from Module.utile_mysql import * 

dash.register_page(__name__)

def show_dataframe(table: str, properties_config, db, cursor):
    column_list: list = column_name_in_properties(properties_config, "Projet_Examen", table)
    # Récupération des data dans la BDD
    column_str: str = ", ".join(column_list)
    db["Projet_Examen"].reconnect()
    list_table = select_bdd(cursor["Projet_Examen"], table, column_str)
    print("Mise à jour Data:")
    db["Projet_Examen"].close()
    # Mise en forme des datas
    df = pd.DataFrame(list_table, columns=column_list)
    # print(df)
    return df
        

# Connexion à la database
data_config: dict = create_config()
mydb, mycursor = init_SQL(data_config)
bdd_properties_config: dict = create_properties(data_config, mydb, mycursor)
database_list: list = database_name_in_properties(bdd_properties_config)
list_radio = table_name_in_properties(bdd_properties_config, database_list[0])
# Initialisation du dataframe
dataframe = show_dataframe(list_radio[0], bdd_properties_config, mydb, mycursor)

layout = html.Div([
    
    html.Div([
        
        html.H1('Contenu de la base de donnée', style={'textAlign': 'center'}),
        "Selectionnez une table dans la base de donnée: ",
        dcc.RadioItems(options=list_radio, value='element', id='analytics-input', inline=True, labelStyle={"display": "inline-block", "align-items": "center", 'margin-left':'20px'}),
        html.Div(id='analytics-output')
        ]),
    
    html.Br(),
    
    dash_table.DataTable(
            id='data-table',
            data=dataframe.to_dict('records'), 
            columns=[{"name": i, "id": i} for i in dataframe.columns],
            page_size=50,
            style_data={
            'whiteSpace': 'normal'
            },
            css=[{
                'selector': '.dash-spreadsheet td div',
                'rule': '''
                    line-height: 15px;
                    max-height: 30px; min-height: 10px; height: 15px;
                    display: block;
                    overflow-y: hidden;
                '''}],
            tooltip_data=[
                {
                    column: {'value': str(value), 'type': 'markdown'}
                    for column, value in row.items()
                } for row in dataframe.to_dict('records')
            ],
            tooltip_duration=None,
            style_table={'overflowX': 'auto'},
            style_cell={'textAlign': 'left'},
            style_cell_conditional=[
                    {'if': {'column_id': 'title'},
                    'width': '15%'},
                    {'if': {'column_id': 'id_element'},
                    'width': '15%'},
                    {'if': {'column_id': 'date'},
                    'width': '15%'}]
        )

], style={"border": "1px black solid", 'width': '92%', 'display': 'inline-block', 'horizontal-align': 'center', 'margin-right': '3vw', 'margin-left': '3vw', 'margin-top': '3vw', 'margin-bottom': '3vw'})


@callback(
    [Output('analytics-output', 'children'), 
    Output('data-table', 'data'),
    Output('data-table', 'columns')],
    Input('analytics-input', 'value'))
def update_city_selected(input_value):
    if input_value is None:
        raise PreventUpdate
    
    dataframe = show_dataframe(input_value, bdd_properties_config, mydb, mycursor)

    if dataframe.empty:
        columns_list = column_name_in_properties(bdd_properties_config, "Projet_Examen", input_value)
        value_list = ['Pas de valeurs'] * len(columns_list)
        data = [list_to_dict(columns_list, value_list)]
    else:
        data = dataframe.to_dict('records')
    
    print(data)
    columns = [
        {'name': k.capitalize(), 'id': k}
        for k in data[0].keys()]
    print(columns)
    
    text = f'Votre sélection : {input_value}'
    
    return text, data, columns
