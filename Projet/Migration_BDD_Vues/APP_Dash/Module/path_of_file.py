from datetime import datetime
import os

# /home/antony/Projet_Examen/Projet/Migration_BDD_Vues/Module/utile_save_data.py
path_excel: str = '/home/antony/Projet_Examen/Projet/Ressources/DATA/Export_BDD/get_channel_id-search_2024-05-11.xlsx'

# /home/antony/Projet_Examen/Projet/Migration_BDD_Vues/Module/save_data_as_file.py
path: str = "/home/antony/Projet_Examen/Projet/Ressources/DATA/"

# /home/antony/Projet_Examen/Projet/Migration_BDD_Vues/Module/insertion_database_mysql.py
    # Init Module
path_module: str = "/home/antony/Projet_Examen/Projet/Migration_BDD_Vues/APP_Dash/Module/"
    # Config
path_config: str = "/home/antony/Projet_Examen/Projet/Migration_BDD_Vues/APP_Dash/Module/Properties/"
file_name_config: str = "bdd_config.json"
file_name_properties: str = "bdd_properties.json"
    # __main__
Export_file_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/Export_file/')
# à remplacer par /home/antony/Projet_Examen/Ressources/DATA

# /home/antony/Projet_Examen/Projet/Migration_BDD_Vues/Module/connexion_database_mysql.py
data_config: dict = {
    "path_config": path_config, 
    "list_database": ['Projet_Examen'], 
    "name_config": file_name_config, 
    "name_properties": file_name_properties,
    "return_dictionnary": False # dictionary = True, return dict / dictionary = False, return liste
    }