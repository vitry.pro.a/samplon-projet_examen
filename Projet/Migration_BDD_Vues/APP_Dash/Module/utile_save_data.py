import json
import os
import xlsxwriter
from datetime import datetime
import pandas as pd

import path_of_file as PF


# Fonction JSON:
def read_JSON(path: str, file_name: str):
    path_file: str = path + file_name
    with open(path_file, "r") as f:
        data = json.load(f)
    return data


def write_JSON(path: str, file_name: str, data, option_indent=2, default=None):
    path_file: str = path + file_name
    with open(path_file, "w") as file:
        json.dump(data, file, indent=option_indent, default=default)


# Fonction Excel:
def extract_data_excel(path, index_col=None, header=0, sheet=0, names=None):
    df = pd.read_excel(path, index_col=index_col, header=header, sheet_name=sheet, names=names)
    return df


def insert_data_excel(path, file_name, list_result: list[list]):
    print(path + file_name, list_result)
    workbook = xlsxwriter.Workbook(path + file_name)
    sheet = workbook.add_worksheet()
    
    for index, row_in_list in enumerate(list_result): # Write les lignes
        list_row: list = []
        for item in row_in_list:
            if type(item) == list:
                element_str: str = ', '.join(item)
            elif type(item) == bool:
                element_str: str = str(item)
                # if item == True:
                #     element_str: str = "1"
                # elif item == False:
                #     element_str: str = "0"
            elif type(item) == int:
                element_str: str = str(item)
            else:
                element_str = item
            list_row.append(element_str)
        sheet.write_row(index, 0, list_row)
    
    workbook.close()


if __name__=="__main__":
    df_index_default = extract_data_excel(PF.path_excel, sheet=0)
    print(df_index_default)
    
    
    list_of_result = [
        ['UCVauig-EswAJtieNhzdjhfh', 'channel', 'POLARIZACION', '2024-06-11T19:10:14Z', 'IMPLEMENTACION DEL RETO.', ''],
        ['UCVauig-EswAJtieNhzGgVyg', 'channel', 'POLARIZACION POLITICA', '2024-05-11T19:10:14Z', 'IMPLEMENTACION DEL RETO.', 'UCVauig-EswAJtieNhzGgVyg'],
        ['UCVauig-EswAJtieNhzGgVyg', 'channel', 'POLARIZACION POLITICA', '2024-05-11T19:10:14Z', 'IMPLEMENTACION DEL RETO.', '']]
    search_type = "channel"
    
    path_extract = "/home/antony/Projet_Examen/Ressources/DATA/Import_API/"
    file_name_extract = f"Test_extract_search_{search_type}_{datetime.now().date()}.xlsx"
    insert_data_excel(path_extract, file_name_extract, list_of_result)

