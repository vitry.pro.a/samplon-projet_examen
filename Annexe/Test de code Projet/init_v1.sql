DROP DATABASE IF EXISTS Projet_Examen;
CREATE DATABASE IF NOT EXISTS Projet_Examen;

USE Projet_Examen;


CREATE TABLE `element` (
  `id_element` varchar(255) PRIMARY KEY NOT NULL,
  `title` varchar(255) NOT NULL,
  `publish_date` date NOT NULL,
  `description` text
);

CREATE TABLE `search` (
  `element_id` varchar(255) NOT NULL,
  `element_search_id` varchar(255) NOT NULL,
  `kind` varchar(255) NOT NULL
);

CREATE TABLE `playlist` (
  `playlist_id` varchar(255) NOT NULL,
  `channel_id` varchar(255) NOT NULL,
  `video_id` varchar(255) NOT NULL
);

CREATE TABLE `keywords` (
  `id_keyword` varchar(255) PRIMARY KEY NOT NULL,
  `keyword` varchar(255) NOT NULL
);

CREATE TABLE `element_keywords` (
  `element_id` varchar(255) NOT NULL,
  `keyword_id` varchar(255) NOT NULL
);

CREATE TABLE `topics` (
  `id_topic` varchar(255) PRIMARY KEY NOT NULL,
  `topic` varchar(255) NOT NULL
);

CREATE TABLE `element_topics` (
  `element_id` varchar(255) NOT NULL,
  `topic_id` varchar(255) NOT NULL
);

CREATE TABLE `propretie_channel` (
  `channel_id` varchar(255) PRIMARY KEY NOT NULL,
  `viewCount` int,
  `likesCount` int,
  `videoCount` int,
  `hiddenSubscridenCount` int,
  `subscriberCount` int
);

CREATE TABLE `propretie_video` (
  `video_id` varchar(255) PRIMARY KEY NOT NULL,
  `viewCount` int,
  `likesCount` int,
  `videoCount` int,
  `commentCount` int,
  `duration` int,
  `uploadstatus` varchar(255)
);

ALTER TABLE `search` ADD FOREIGN KEY (`element_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `search` ADD FOREIGN KEY (`element_search_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `playlist` ADD FOREIGN KEY (`channel_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `playlist` ADD FOREIGN KEY (`playlist_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `playlist` ADD FOREIGN KEY (`video_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `element_topics` ADD FOREIGN KEY (`element_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `element_topics` ADD FOREIGN KEY (`topic_id`) REFERENCES `topics` (`id_topic`);

ALTER TABLE `element_keywords` ADD FOREIGN KEY (`element_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `element_keywords` ADD FOREIGN KEY (`keyword_id`) REFERENCES `keywords` (`id_keyword`);

ALTER TABLE `propretie_channel` ADD FOREIGN KEY (`channel_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `propretie_video` ADD FOREIGN KEY (`video_id`) REFERENCES `element` (`id_element`);
