from utile_save_data import *
import path_of_file as PF


if __name__=="__main__":
    list_data = [1, 2, 3, 4, 5]
    dict_data = {
        "Cahiers": 3,
        "Stylos": 2,
        "Agrafes": None
    }
    
    list_of_name = ["list", "dict"]
    list_of_data = [list_data, dict_data]
    
    for file, data in zip(list_of_name, list_of_data):
        print("list_name", file)
        name_file = f"{file}_data_test.json"
        
        write_JSON(PF.path, name_file, data)
        print(f"write_{file}:", data)
        
        read_data = read_JSON(PF.path, name_file)
        print(f"read_{file}:", read_data)
