# -*- coding: utf-8 -*-

# Permissions:
# ls -l get_channel_id-search.py
# chmod +x get_channel_id-search.py 

from utile import * 
import argparse
import datetime
import xlsxwriter
# from insert_BDD import *
import os


scopes = ["https://www.googleapis.com/auth/youtube.force-ssl"]


def youtube_search(options, nbr_résultat, dico_list_of_result):
    # Booléen pour affichage dans le terminal:
    debug_tool = True
    # Requète de recherche:
    search_response = connexion(options)
    # Traitement du résultat JSON:
    if search_response != None:
        debug_print(f"Lecture des data de la requète http:", debug_tool)
        # Boucle sur les dimensions du JSON:
        for head in search_response.keys():
            debug_print(f"Dimension du Json: {head}", debug_tool)
            # Vérification de la pagination:
            if "nextPageToken" in search_response.keys():
                token_tool = True
            else:
                token_tool = False
            # Incrémentation du nombre de recherche à chaque appel de la fonction récusive:
            if head == "pageInfo":
                nbr_résultat["nbr_current"] += int(search_response[head]["resultsPerPage"])
                nbr_résultat["nbr_total"] = int(search_response[head]["totalResults"])
            debug_print('Nombre de résultat en cours: %s / %s' % (nbr_résultat["nbr_current"], nbr_résultat["nbr_total"]), debug_tool)
            # Vérification du header de la requète:
            if head != "items":
                debug_print(f"Key token: {search_response[head]}", debug_tool)
                if token_tool == True and head == "nextPageToken":
                    key_token_page = search_response[head]
                    debug_print(f"Key token pour pagination: {key_token_page}", debug_tool)
                elif token_tool == False:
                    key_token_page = None
                    debug_print(f"Key token pour pagination: {key_token_page}", debug_tool)
                debug_print(None, debug_tool)
            # Vérification du corps de la requète:
            else:
                # Boucle sur les dimensions du corps (items) du JSON:
                for search_result in search_response[head]:
                    # Recherche des vidéos:
                    if search_result['id']['kind'] == 'youtube#video':
                        dico_list_of_result["videos"].append([search_result['id']['videoId'],
                                                                search_result['snippet']['title'],
                                                                search_result['snippet']['publishedAt'],
                                                                search_result['snippet']['description'],
                                                                search_result['snippet']['channelId']])
                    # Recherche des chaines:
                    elif search_result['id']['kind'] == 'youtube#channel':
                        dico_list_of_result["channels"].append([search_result['id']['channelId'],
                                                                search_result['snippet']['title'],
                                                                search_result['snippet']['publishedAt'],
                                                                search_result['snippet']['description'],
                                                                search_result['snippet']['channelId']])
                    # Recherche des playlists:
                    elif search_result['id']['kind'] == 'youtube#playlist':
                        dico_list_of_result["playlists"].append([search_result['id']['playlistId'],
                                                                search_result['snippet']['title'],
                                                                search_result['snippet']['publishedAt'],
                                                                search_result['snippet']['description'],
                                                                search_result['snippet']['channelId']])
                # Vérification de la fin de la pagination:
                if key_token_page != None:
                    parser.set_defaults(page_Token=key_token_page)
                    options = parser.parse_args()
                    debug_print(None, debug_tool)
                    dico_list_of_result = youtube_search(options, nbr_résultat, dico_list_of_result)
    # Affichage du nombre de résultat obtenus dans la requète:
    debug_print('Nombre de résultat traité: %s / %s' % (nbr_résultat["nbr_current"], nbr_résultat["nbr_total"]), debug_tool)
    return dico_list_of_result


if __name__ == '__main__':
    # Choix des arguments en fonction des options prédéfini de l'API pour search:
    """
    '--part' --> snippet
    '--channel_Type' --> any,show
    '--order' --> date,rating,relevance,title,videoCount,viewCount
    '--type' --> channel,playlist,video
    """
    # Création des arguments pour la requète de recherche:
    arguments = {"q": '', "part": "id,snippet", "channelId": '', "channel_Type": "any", "max_results": 50, "order": "date", "region_code": "FR", "type": "channel"}
    # Création du conteneur des résultats pour la requète de recherche:
    list_of_result = {"videos": [], "channels": [], "playlists": []}
    # Création du conteneur du nombre de résultats pour la requète de recherche:
    nbr_résultat = {"nbr_current": 0, "nbr_total": 0}
    # Création du conteneur pour les éléments qui n'ont été inséré dans la BDD:
    list_not_insert_in_bdd = []
    # Chemin par défaut des fichiers créer:
    path = 'Préparation/Python/data'
    
    
    # Création du parseur pour les arguments:
    parser = argparse.ArgumentParser()
    parser.add_argument('--q',              help='Search term',     default=arguments['q'])
    parser.add_argument('--part',           help='Search part',     default=arguments['part'])
    parser.add_argument('--channel_Type',   help='channel Type',    default=arguments['channel_Type'])
    parser.add_argument('--channelId',      help='channel id',      default=arguments['channelId'])
    parser.add_argument('--max_results',    help='Max results',     default=arguments['max_results'])
    parser.add_argument('--page_Token',     help='Page Token')
    parser.add_argument('--order',          help='Order',           default=arguments['order'])
    parser.add_argument('--region_code',    help='Region code',     default=arguments['region_code'])
    parser.add_argument('--type',           help='Type',            default=arguments['type'])
    args = parser.parse_args()

    # Requète de recherche pour les chaines youtube vu en France:
    for quotas in range(0, 10):
        print(f"Numéro de la requète en cours: {quotas}")
        list_of_result = youtube_search(args, nbr_résultat, list_of_result)
    
    # Connexion BDD
    # list_table = list_of_table_bdd(mycursor)
    
    workbook = xlsxwriter.Workbook(f"Préparation/Python/get_channel_id-search_{datetime.datetime.now().date()}.xlsx")
    sheet = workbook.add_worksheet()
    
    # fichier = open(f"Préparation/Python/get_channel_id-search_{datetime.datetime.now().date()}.txt", "a")
    # fichier.writelines(f"Historique requète: {datetime.datetime.now()}\n")
    # Affichage des résultats:
    for dico in list_of_result.keys():
        print(f"{dico}:")
        if list_of_result[dico] != []:
            for index, liste_in_dico in enumerate(list_of_result[dico]):
                print(index, liste_in_dico)
                # list_not_insert_in_bdd = transfert_in_database(liste_in_dico, index)
                sheet.write(f"A{index}", liste_in_dico[0]) # id > videoId / channelId / playlistId
                sheet.write(f"B{index}", liste_in_dico[1]) # title 
                sheet.write(f"C{index}", liste_in_dico[2]) # publishedAt
                sheet.write(f"D{index}", liste_in_dico[3]) # description
                sheet.write(f"E{index}", liste_in_dico[4]) # channelId
                sheet.write(f"F{index}", f"{datetime.datetime.now().date()}") # date d'import
                sheet.write(f"G{index}", arguments["type"]) # type > video / channel / playlist
                # fichier.write(f"{liste_in_dico[0]},{liste_in_dico[1]},{liste_in_dico[2]},{liste_in_dico[3]}\n")
        else:
            print("[]")
        print()
    
    # fichier.close()
    workbook.close()
    
    # -------------------------------------------------------------
    files = []
    files_dir = os.listdir(path)
    for name in files_dir:
        if name.startswith("doublon_api_"):
            print(name)
            files.append(name)
    
    # -------------------------------------------------------------
    # nb_file_doublon = len(files) + 1
    # workbook = xlsxwriter.Workbook(f"Préparation/Python/data/doublon_api_{datetime.datetime.now().date()}_{nb_file_doublon}.xlsx")
    # sheet = workbook.add_worksheet()
    
    # if list_not_insert_in_bdd != []:
    #     for ind, liste_in_return in enumerate(list_not_insert_in_bdd):
    #         print(ind, liste_in_return)
    #         sheet.write(f"A{ind}", liste_in_return[0]) # title
    #         sheet.write(f"B{ind}", liste_in_return[1]) # id > videoId / channelId / playlistId
    #         sheet.write(f"C{ind}", liste_in_return[2]) # publishedAt
    #         sheet.write(f"D{ind}", liste_in_return[3]) # description
    #         sheet.write(f"E{ind}", liste_in_return[4]) # channelId
    #         sheet.write(f"F{ind}", f"{datetime.datetime.now().date()}") # date d'import
    # else:
    #     print("[]")
    
    
    # workbook.close()
    
    print("Fin du téléchargement: ", nbr_résultat)
    
    
    
    
    ###################################
    # Code exemple:
    ###################################

    # dico_list_of_result["channels"].append('%s (%s) at [%s]' % (search_result['snippet']['title'],
    #                         # search_result['snippet']['description'],
    #                         search_result['id']['channelId'],
    #                         search_result['snippet']['publishedAt']))


    # videos = []
    # channels = []
    # playlists = []

    # if options.type == "video":
    #     print('Videos:\n', '\n'.join(videos), '\n')
    # elif options.type == "channel":
    #     print('Channels:\n', '\n'.join(channels), '\n')
    # elif options.type == "playlist":
    #     print('Playlists:\n', '\n'.join(playlists), '\n')


    # print(search_response)
    # for ind, val in zip(search_response.keys(), search_response.values()):
    #     if ind != "items":
    #         print("clef dico:", ind, "value dico:", val)
    #     if ind == "items":
    #         print("clef dico:", ind, "value dico:")
    #         for i in val:
    #             print(i)
    #     print()


    # Affiche data des chaines trouvés sur youtube:
    # for i in range(len(search_response["items"])):
    #     print("Type d'élément --->", search_response["items"][i]["id"]["kind"])
    #     print("channelId --->", search_response["items"][i]["id"]["channelId"])
    #     for key in search_response["items"][i]["snippet"].keys():
    #         if key != "thumbnails" and key != "channelId":
    #             print(key, "----->", search_response["items"][i]["snippet"][key])
    #     print()