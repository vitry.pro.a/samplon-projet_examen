import os
import argparse

from googleapiclient.errors import HttpError
import googleapiclient.discovery
import googleapiclient.errors

from YOUR_CLIENT_SECRET_FILE import *
from variable import * 


def create_API():
    # Get credentials and create an API client
    return googleapiclient.discovery.build(API_SERVICE_NAME, API_VERSION, 
                                            developerKey=api_key)


def connexion(options_argparse):
    # Disable OAuthlib's HTTPS verification when running locally.
    # *DO NOT* leave this option enabled in production.
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

    youtube = create_API()

    # Call the search.list method to retrieve results matching the specified
    # query term.
    try:
        response_JSON = youtube.search().list(q=options_argparse.q,
                                                part=           options_argparse.part,
                                                channelType=    options_argparse.channel_Type,
                                                maxResults=     options_argparse.max_results,
                                                pageToken=      options_argparse.page_Token,
                                                order=          options_argparse.order,
                                                regionCode=     options_argparse.region_code,
                                                type=           options_argparse.type
                                                ).execute()
    except HttpError as e:
        print('An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))
        response_JSON = None

    return response_JSON


def debug_print(chaine: str | None , debug: bool = False):
    if debug == True:
        if chaine != None:
            print(chaine)
        else:
            print()


if __name__=="__main__":
    debug_tool = True
    nbr_résultat = {"nbr_current": 50, "nbr_total": 1000}
    data_debug = 'nbr de résultat: %s / %s' % (nbr_résultat["nbr_current"], nbr_résultat["nbr_total"])
    
    # Fonction:
    debug_print(data_debug, debug_tool)
    # Mise en forme print:    
    print('nbr de résultat: %s / %s' % (nbr_résultat["nbr_current"], nbr_résultat["nbr_total"]))