"""
# Recherche des videos:
    [search_result['id']['videoId'],
    search_result['id']['kind'],
    search_result['snippet']['title'],
    search_result['snippet']['publishedAt'],
    search_result['snippet']['description'],
    search_result['snippet']['channelId']]
# Recherche des chaines:
    [search_result['id']['channelId'],
    search_result['id']['kind'],
    search_result['snippet']['title'],
    search_result['snippet']['publishedAt'],
    search_result['snippet']['description'],
    search_result['snippet']['channelId']]
# Recherche des playlists:
    [search_result['id']['playlistId'],
    search_result['id']['kind'],
    search_result['snippet']['title'],
    search_result['snippet']['publishedAt'],
    search_result['snippet']['description'],
    search_result['snippet']['channelId']])
"""



# Connexion API Youtube

# Extraction API Youtube
    # Extraction Search
"""
{
    "kind": "youtube#searchResult",
    "id": {
        "kind": string,
        "videoId": string,
        "channelId": string,
        "playlistId": string
    },
    "snippet": {
        "publishedAt": datetime,
        "channelId": string,
        "title": string,
        "description": string,
        "channelTitle": string
    }
}

[
    "channelId": string,
    "kind": string,
    "title": string,
    "publishedAt": datetime,
    "description": string,
    "videoId": string OR "channelId": string OR "playlistId": string,
]

"""
    # Extraction Channel
"""
{
    "kind": "youtube#channel",
    "id": string,
    "snippet": {
        "title": string,
        "description": string,
        "publishedAt": datetime,
        "country": string
    },
    "contentDetails": {
        "relatedPlaylists": {
            "likes": string,
            "favorites": string,
            "uploads": string
        }
    },
    "statistics": {
        "viewCount": unsigned long,
        "subscriberCount": unsigned long, // this value is rounded to three significant figures
        "hiddenSubscriberCount": boolean,
        "videoCount": unsigned long
    },
    "topicDetails": {
        "topicIds": [
        string
        ],
        "topicCategories": [
        string
        ]
    },
    "brandingSettings": {
        "channel": {
            "title": string,
            "description": string,
            "unsubscribedTrailer": string,
            "country": string
        }
    },
    "localizations": {
        (key): {
            "title": string,
            "description": string
        }
    }
}

[
    "id": string,
    "title": string,
    "publishedAt": datetime,
    "description": string,
    "country": string,
    "likes": string,
    "keywords": string,
    "topicIds": [string],
    "viewCount": unsigned long,
    "hiddenSubscriberCount": boolean,
    "subscriberCount": unsigned long,
    "uploads": string,
    "videoCount": unsigned long
]


"""
    # Extraction Video
"""
{
    "kind": "youtube#video",
    "id": string,
    "snippet": {
        "publishedAt": datetime,
        "channelId": string,
        "title": string,
        "description": string,
        "tags": [
        string
        ],
        "categoryId": string
    },
    "contentDetails": {
        "duration": string,
        "dimension": string,
        "definition": string,
        "caption": string
    },
    "status": {
        "privacyStatus": string,
        "publishAt": datetime,
        "publicStatsViewable": boolean
    },
    "statistics": {
        "viewCount": string,
        "likeCount": string,
        "dislikeCount": string,
        "favoriteCount": string,
        "commentCount": string
    },
    "topicDetails": {
        "topicIds": [
        string
        ],
        "relevantTopicIds": [
        string
        ],
        "topicCategories": [
        string
        ]
    },
    "recordingDetails": {
        "recordingDate": datetime
    },
    "fileDetails": {
        "fileName": string,
        "fileSize": unsigned long,
        "fileType": string,
        "container": string,
        "durationMs": unsigned long,
        "bitrateBps": unsigned long,
        "creationTime": string
    },
    "localizations": {
        (key): {
            "title": string,
            "description": string
        }
    }
}

[
    "id": string,
    "channelId": string,
    "title": string,
    "publishedAt": datetime,
    "description": string,
    ---> Country non disponible
    "likeCount": string,
    "tags": [string],
    "relevantTopicIds": [string],
    "viewCount": string,
    "duration": string,
    "privacyStatus": string,
    "dislikeCount": string,
    "commentCount": string
]
"""
    # Extraction Paylist

# Connexion MySQL Database
"""
Fonction wrapper connecte à la BDD et appel la fonction pour l'insertion dans une table donné
"""
"""
country             ['id_country', 'title', 'region_code']
element             ['id_element', 'type', 'title', 'publish_date', 'description']
element_keywords    ['element_id', 'keyword_id']
element_topics      ['element_id', 'topic_id']
keywords            ['id_keyword', 'keyword']
playlist_by_channel ['playlist_id', 'channel_id', 'video_id']
propretie_channel   ['channel_id', 'viewCount', 'likesCount', 'videoCount', 'hiddenSubscridenCount', 'subscriberCount']
propretie_video     ['video_id', 'viewCount', 'likesCount', 'videoCount', 'commentCount', 'duration', 'privacyStatus']
search              ['element_id', 'element_search_id', 'kind']
topics              ['id_topic', 'topic']
"""

# Insertion MySQL Database
    # Insertion Search



    # Insertion Channel
"""
[
    "id": string,
    "title": string,
    "publishedAt": datetime,
    "description": string,
    "country": string,
    "likes": string,
    "keywords": string,
    "topicIds": [string],
    "viewCount": unsigned long,
    "hiddenSubscriberCount": boolean,
    "subscriberCount": unsigned long,
    "uploads": string,
    "videoCount": unsigned long
]

country             ['id_country', 'title', 'region_code']
element             ['id_element', 'type', 'title', 'publish_date', 'description']
element_keywords    ['element_id', 'keyword_id']
element_topics      ['element_id', 'topic_id']
keywords            ['id_keyword', 'keyword']
playlist_by_channel ['playlist_id', 'channel_id', 'video_id']
propretie_channel   ['channel_id', 'viewCount', 'likesCount', 'videoCount', 'hiddenSubscridenCount', 'subscriberCount']
search              ['element_id', 'element_search_id', 'kind']
topics              ['id_topic', 'topic']
"""
    
    # Insertion Video
"""
[
    "id": string,
    "channelId": string,
    "title": string,
    "publishedAt": datetime,
    "description": string,
    ---> Country non disponible
    "likeCount": string,
    "tags": [string],
    "relevantTopicIds": [string],
    "viewCount": string,
    "duration": string,
    "privacyStatus": string,
    "dislikeCount": string,
    "commentCount": string
]

country             ['id_country', 'title', 'region_code']
element             ['id_element', 'type', 'title', 'publish_date', 'description']
element_keywords    ['element_id', 'keyword_id']
element_topics      ['element_id', 'topic_id']
keywords            ['id_keyword', 'keyword']
playlist_by_channel ['playlist_id', 'channel_id', 'video_id']
propretie_video     ['video_id', 'viewCount', 'likesCount', 'videoCount', 'commentCount', 'duration', 'privacyStatus']
search              ['element_id', 'element_search_id', 'kind']
topics              ['id_topic', 'topic']
"""
    # Insertion Paylist
"""

"""

