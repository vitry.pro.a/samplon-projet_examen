# Test connexion_database_mysql.py

1. Création fichier config
```python
data_config: dict = create_config()
```

2. Initialisation de la connexion BDD
```python
mydb, mycursor = init_SQL(data_config)
```

3. Création fichier properties
```python
bdd_properties_config: dict = create_properties(data_config, mydb, mycursor)
```

4. Récupération des informations de la BDD (ici nom datadase)
```python
database_list: list = database_name_in_properties(bdd_properties_config)
```

5. Récupération des informations de la BDD (ici nom datadase)
```python
table_list: list = table_name_in_properties(bdd_properties_config, database_list[0])
```


```python
    column_list: list = column_name_in_properties(bdd_properties_config, database_list[0], table_list[0])
```