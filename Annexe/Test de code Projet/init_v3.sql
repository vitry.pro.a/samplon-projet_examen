DROP DATABASE IF EXISTS Projet_Examen;

CREATE DATABASE IF NOT EXISTS Projet_Examen;

USE Projet_Examen;

CREATE TABLE `element` (
  `id_element` varchar(255) PRIMARY KEY NOT NULL,
  `type` ENUM ('channel', 'video', 'playlist', 'pays') NOT NULL,
  `title` varchar(255),
  `publish_date` date,
  `description` text
);

CREATE TABLE `country` (
  `id_country` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `region_code` varchar(255) UNIQUE NOT NULL,
  `title` varchar(255) NOT NULL
);

CREATE TABLE `search` (
  `element_id` varchar(255) NOT NULL,
  `element_search_id` varchar(255) NOT NULL,
  `kind` varchar(255) NOT NULL
);

CREATE TABLE `playlist` (
  `playlist_id` varchar(255) NOT NULL,
  `channel_id` varchar(255) NOT NULL,
  `video_id` varchar(255) NOT NULL
);

CREATE TABLE `keywords` (
  `id_keyword` varchar(255) PRIMARY KEY NOT NULL,
  `keyword` varchar(255) NOT NULL
);

CREATE TABLE `element_keywords` (
  `element_id` varchar(255) NOT NULL,
  `keyword_id` varchar(255) NOT NULL
);

CREATE TABLE `topics` (
  `id_topic` varchar(255) PRIMARY KEY NOT NULL,
  `topic` varchar(255) NOT NULL
);

CREATE TABLE `element_topics` (
  `element_id` varchar(255) NOT NULL,
  `topic_id` varchar(255) NOT NULL
);

CREATE TABLE `proprety` (
  `element_id` varchar(255) NOT NULL,
  `viewCount` int,
  `likesCount` int,
  `videoCount` int,
  `commentCount` int,
  `duration` int,
  `hiddenSubscridenCount` int,
  `subscriberCount` int,
  `privacyStatus` ENUM ('private', 'public', 'unlisted') NOT NULL
);

CREATE UNIQUE INDEX `country_index_0` ON `country` (`id_country`, `region_code`);

CREATE UNIQUE INDEX `search_index_1` ON `search` (`element_id`, `element_search_id`);

CREATE UNIQUE INDEX `playlist_index_2` ON `playlist` (`playlist_id`, `channel_id`, `video_id`);

ALTER TABLE `search` ADD FOREIGN KEY (`element_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `search` ADD FOREIGN KEY (`element_search_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `playlist` ADD FOREIGN KEY (`channel_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `playlist` ADD FOREIGN KEY (`playlist_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `playlist` ADD FOREIGN KEY (`video_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `element_topics` ADD FOREIGN KEY (`element_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `element_topics` ADD FOREIGN KEY (`topic_id`) REFERENCES `topics` (`id_topic`);

ALTER TABLE `element_keywords` ADD FOREIGN KEY (`element_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `element_keywords` ADD FOREIGN KEY (`keyword_id`) REFERENCES `keywords` (`id_keyword`);

ALTER TABLE `proprety` ADD FOREIGN KEY (`element_id`) REFERENCES `element` (`id_element`);

ALTER TABLE `element` ADD FOREIGN KEY (`id_element`) REFERENCES `country` (`id_country`);
