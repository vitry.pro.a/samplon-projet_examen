import xlsxwriter    
import datetime
import re


# ['GANG GANG', 'UC_yedN8zuurtNLp9DJXxyQg', '2024-04-26T09:32:13Z', '']
def parsing_file_line(chaine_of_list: str):
    list_of_element: list = []
    list_of_chaine: list = chaine_of_list.split(', ')
    for element in list_of_chaine:
        if element.startswith('['):
            element = element.replace("[", '')
        elif element.endswith('\n'):
            element = element.replace("]\n", '')
        list_of_element.append(element)
    return list_of_element

# ["'GANG GANG'", "'UC_yedN8zuurtNLp9DJXxyQg'", "'2024-04-26T09:32:13Z'", '""']
def parsing_element_list(list_of_chaine: list):
    list_of_element: list = []
    for element in list_of_chaine:
        if element.startswith("'") and element.endswith("'"):
            element = element.replace("'", '', 2)
        if element.startswith('"') and element.endswith('"'):
            element = element.replace('"', '', 2)
            element = element.replace('\\', '')
        list_of_element.append(element)
    return list_of_element


def main(path_input, path_output):
    workbook = xlsxwriter.Workbook(path_output)
    sheet = workbook.add_worksheet()
    index = 1
    sheet.write(f"A{index}", "Titre de la chaine")
    sheet.write(f"B{index}", "Id de la chaine")
    sheet.write(f"C{index}", "Date d'importation")
    sheet.write(f"D{index}", "Description de la chaine")
    sheet.write(f"E{index}", "Date de modification du fichier")
    with open(path_input, 'r') as fichier:
        for line in fichier:
            print(index, line, end='')
            if line.startswith("Historique requète:"):
                print("first line")
            else:
                liste_for_line = parsing_file_line(line)
                liste_for_line = parsing_element_list(liste_for_line)
                sheet.write(f"A{index}", liste_for_line[0])
                sheet.write(f"B{index}", liste_for_line[1])
                sheet.write(f"C{index}", liste_for_line[2])
                sheet.write(f"D{index}", liste_for_line[3])
                sheet.write(f"E{index}", f"{datetime.datetime.now().date()}")
            index += 1
    fichier.close()
    workbook.close()


if __name__=="__main__":
    path_input_file = f"Préparation/Python/get_channel_id-search_2024-05-11.txt"
    path_output_file = f"Préparation/Python/get_channel_id-search_2024-05-11.xlsx"
    main(path_input_file, path_output_file)