import requests
from geopy.geocoders import Nominatim
from dash import Dash, dcc, html
import folium


def get_lat_lon_by_country(country_name):
    geolocator = Nominatim(user_agent="your_app_name")
    location = geolocator.geocode(country_name)

    if location:
        latitude = location.latitude
        longitude = location.longitude
        return latitude, longitude
    else:
        return None

def extract_required_info(person_data_list):
    res = {}
    for person_data in person_data_list:
        residence_country_name = person_data.get("pays_residence")
        
        if residence_country_name:
            residence_lat_lon = get_lat_lon_by_country(residence_country_name)
            
            if not residence_lat_lon:
                print(f"Could not find location for {residence_country_name} (Residence)")
            else:
                print(f"{person_data['nom']}'s Residence Latitude and Longitude: {residence_lat_lon}")

            for voyage in person_data.get("voyages", []):
                country_name = voyage.get("pays")
                country_lat_lon = get_lat_lon_by_country(country_name)

                if country_lat_lon:
                    #print(f"{person_data['nom']}'s Latitude and Longitude for {country_name}: {country_lat_lon}")
                    res[country_name]= country_lat_lon
                #else:
                    #print(f"Could not find location for {country_name}")
        else:
            print("Error: 'pays_residence' key not found in person data.")
        return res
  
# Example usage:
api_url = "http://127.0.0.1:8000/person/travel_to"

url_with_params = f"{api_url}?nom=John&prenom=Doe&pays_residence=United States"

# Assuming you're sending a GET request to the API
response = requests.get(url_with_params)

if response.status_code == 200:
    person_data_list = response.json()
    print(f"API Response: {person_data_list}")
    res = extract_required_info(person_data_list)
    print(res)
else:
    print(f"Error {response.status_code}: {response.text}")





X= 54.5260
Y= 15.2551
m = folium.Map(location=(X, Y), zoom_start=3)


high_flux = folium.FeatureGroup("high_flux").add_to(m)

for i, key in enumerate(res):
    nom_pays = key
    lat = res[key][0]
    long = res[key][1]
    for j in range(i + 1, len(res)):
        print(j)

""" 

    points = [[dep_lat, dep_long],[ar_lat, ar_long]]


points = [{country_lat_lon},{country_lat_lon}]

folium.PolyLine(points, color='red', weight=10, opacity=1).add_to(high_flux)


folium.LayerControl().add_to(m)

m.save('map_test.html') """ 