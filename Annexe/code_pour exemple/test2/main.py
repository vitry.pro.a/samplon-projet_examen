from fastapi import FastAPI

# Démarrage de fast API par commande :
# uvicorn main:app --reload
app = FastAPI()

liste = [{"test": 1}, {"coucou": 1}, {"hello world": 1}]
# liste[0] → Affiche le produit
# liste[0]["test"] → Affiche la quantité du produit


def verif_quantite(product_name, data):
    for i, j in enumerate(data):
        x = data[i].keys()
        print(x, i)
        if data[i] == product_name:
            print(data[i], product_name)


@app.get("/")
def read_root():
    return {"Home": "Page d'accueil sur le test d'une API"}
    # {"Home": "Page d'accueil sur le test d'une API"}


# GET /courses | retourne la liste de course
@app.get("/courses")
def read_item():
    return liste


# GET /courses/{id} | retourne le produit à l'indice 'id' de ma liste de course
@app.get("/courses/{produit_id}")
def read_item(produit_id: int):
    try:
        produit = liste[produit_id]
    except:
        produit = {"message": "Mauvais ID"}
    return {"Produit_id": produit_id, "Produit": produit}


# POST /courses/{produit} | ajoute un produit à ma liste
@app.post("/courses/{produit_name}")
def update_item(produit_name: str):
    verif_quantite(produit_name, liste)
    return liste.append({produit_name: 1})


# DELETE /courses/{id} | surprime le produit de ma liste
@app.delete("/courses/{produit_id}")
def update_item(produit_id: int):
    try:
        produit = liste[produit_id]
    except:
        produit = {"message": "Mauvais ID"}
    return liste.pop(produit_id)


consigne = """
# GET /courses | retourne la liste de course
# GET /courses/{id} | retourne le produit à l'indice 'id' de ma liste de course
# POST /courses/{produit} | ajoute un produit à ma liste
# DELETE /courses/{id} | surprime le produit de ma liste

si je fait plusieurs fois le post pour ajouter des produits, il augmente la quantité
Du coup la liste ressemble à ça:
[
 {"banane": 2},
 {"orange": 1}
... etc.
]
Chaque produit est en fait un dict avec comme clef le nom du produit et en valeur la quantité

faire en sorte que si je précise la quantité avec un query string sur le POST, ca ajoute la quantité voulue
Par exemple :
/courses/poulet?quantite=100
m'ajoute 100 poulet dans la liste

Ajouter une route
GET /courses/save
Qui écrit ma liste de course en csv dans un fichier
Quand le serveur se lance (au moment ou je créé le tableau) 
lire le fichier csv pour créer le tableau avec les données persistées

utiliser cette API pour récupérer la quantité de calorie dans ma liste de course
https://openfoodfacts.github.io/openfoodfacts-server/api/
"""

# Example: de requête en pull
# http://127.0.0.1:8000/items/4?item=red
