from dash import Dash, dcc, html
import plotly.express as px
import requests 
from dash.dependencies import Input, Output

# Créer une application Dash
app = Dash(__name__)

def get_data():
    api_url = "http://127.0.0.1:8000/person/travel_to"
    try:
        response = requests.get(api_url)
        response.raise_for_status() 
        data = response.json()
        return data
    except requests.RequestException as e:
        print(f"Error fetching data from API: {e}")
        return None  

# Initialize fig outside the callback
fig = px.choropleth(
    locations=[],
    locationmode='country names',
    color=[],
    color_continuous_scale='Viridis',
    center={"lat": 51.1657, "lon": 10.4515},
    title='Nombre total de jours passés par pays européen par les voyageurs'
)

# @app.callback(
#     Output('europe-map', 'figure'),
#     [Input('interval-component', 'n_intervals')]
# )
# def update_map(n_intervals):
#     data = get_data()

#     if data:
#         flat_data = {
#             "pays_residence": data["pays_residence"],
#             "pays": [entry["pays"] for entry in data["voyages"]],
#             "nb_jour_visite": [entry["nb_jour_visite"] for entry in data["voyages"]]
#         }

#         # Update fig inside the callback
#         fig.update_traces(
#             locations=flat_data['pays_residence'],
#             z=flat_data['nb_jour_visite']
#         )

#         return fig

# Mise en page de l'application Dash
app.layout = html.Div([
    html.H4('Carte du Continent Européen avec les jours passés par pays par les voyageurs'),
    dcc.Graph(id='europe-map', figure=fig, style={'width': '2000px', 'height': '1000px'})
])

# Exécuter l'application Dash
if __name__ == '__main__':
    app.run_server(debug=True)

