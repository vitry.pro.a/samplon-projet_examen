
def country(mydatabase, mycursor, name_database, bdd_properties, dataframe):
    """
    country             ['id_country', 'title', 'region_code']
    """
    # Initialisation de variable de la fontion: ------------------------------
    nb_table = 0
    nb_column_id = 0
    list_duplicate_bdd, list_insert_bdd = []
    # Initialisation de liste de la table: -----------------------------------
    table_list: list = table_name_in_properties(bdd_properties, name_database)
    column_list: list = column_name_in_properties(bdd_properties, name_database, table_list[nb_table])
    # Lancement de la boucle d'insertion: ------------------------------------
    for row in dataframe.itertuples():
        # Coordination liste de la table et liste des valeurs: ---------------
        value_country = [row.id, row.type, row.title, row.date, row.description]
        id_of_insert = check_for_insertion(mydatabase, mycursor, column_list, value_country, name_database, table_list[nb_table], nb_column_id)
        if type(id_of_insert) == list and id_of_insert != []:
            list_duplicate_bdd.append(id_of_insert)
        elif type(id_of_insert) == str:
            list_insert_bdd.append(id_of_insert)
    # Création du rapport d'insertion: ---------------------------------------
    report_insertion("insert", table_list[nb_table], list_insert_bdd)
    report_insertion("duplicate", table_list[nb_table], list_duplicate_bdd)


def element_keyword(mydatabase, mycursor, name_database, bdd_properties, dataframe):
    """
    element_keywords    ['element_id', 'keyword_id']
    """
    # Initialisation de variable de la fontion: ------------------------------
    nb_table = 2
    nb_column_id = 0
    list_duplicate_bdd, list_insert_bdd = []
    # Initialisation de liste de la table: -----------------------------------
    table_list: list = table_name_in_properties(bdd_properties, name_database)
    column_list: list = column_name_in_properties(bdd_properties, name_database, table_list[nb_table])
    # Lancement de la boucle d'insertion: ------------------------------------
    for row in dataframe.itertuples():
        # Coordination liste de la table et liste des valeurs: ---------------
        value_element_keyword = [row.id, row.type, row.title, row.date, row.description]
        id_of_insert = check_for_insertion(mydatabase, mycursor, column_list, value_element_keyword, name_database, table_list[nb_table], nb_column_id)
        if type(id_of_insert) == list and id_of_insert != []:
            list_duplicate_bdd.append(id_of_insert)
        elif type(id_of_insert) == str:
            list_insert_bdd.append(id_of_insert)
    # Création du rapport d'insertion: ---------------------------------------
    report_insertion("insert", table_list[nb_table], list_insert_bdd)
    report_insertion("duplicate", table_list[nb_table], list_duplicate_bdd)


def element_topic(mydatabase, mycursor, name_database, bdd_properties, dataframe):
    """
    element_topics      ['element_id', 'topic_id']
    """
    # Initialisation de variable de la fontion: ------------------------------
    nb_table = 3
    nb_column_id = 0
    list_duplicate_bdd, list_insert_bdd = []
    # Initialisation de liste de la table: -----------------------------------
    table_list: list = table_name_in_properties(bdd_properties, name_database)
    column_list: list = column_name_in_properties(bdd_properties, name_database, table_list[nb_table])
    # Lancement de la boucle d'insertion: ------------------------------------
    for row in dataframe.itertuples():
        # Coordination liste de la table et liste des valeurs: ---------------
        value_element_topic = [row.id, row.type, row.title, row.date, row.description]
        id_of_insert = check_for_insertion(mydatabase, mycursor, column_list, value_element_topic, name_database, table_list[nb_table], nb_column_id)
        if type(id_of_insert) == list and id_of_insert != []:
            list_duplicate_bdd.append(id_of_insert)
        elif type(id_of_insert) == str:
            list_insert_bdd.append(id_of_insert)
    # Création du rapport d'insertion: ---------------------------------------
    report_insertion("insert", table_list[nb_table], list_insert_bdd)
    report_insertion("duplicate", table_list[nb_table], list_duplicate_bdd)


def element(mydatabase, mycursor, name_database, bdd_properties, dataframe):
    """
    element             ['id_element', 'type', 'title', 'publish_date', 'description']
    """
    # Initialisation de variable de la fontion: ------------------------------
    nb_table = 1
    nb_column_id = 0
    list_duplicate_bdd, list_insert_bdd = []
    # Initialisation de liste de la table: -----------------------------------
    table_list: list = table_name_in_properties(bdd_properties, name_database)
    column_list: list = column_name_in_properties(bdd_properties, name_database, table_list[nb_table])
    # Lancement de la boucle d'insertion: ------------------------------------
    for row in dataframe.itertuples():
        # Coordination liste de la table et liste des valeurs: ---------------
        value_element = [row.id, row.type, row.title, row.date, row.description]
        id_of_insert = check_for_insertion(mydatabase, mycursor, column_list, value_element, name_database, table_list[nb_table], nb_column_id)
        if type(id_of_insert) == list and id_of_insert != []:
            list_duplicate_bdd.append(id_of_insert)
        elif type(id_of_insert) == str:
            list_insert_bdd.append(id_of_insert)
    # Création du rapport d'insertion: ---------------------------------------
    report_insertion("insert", table_list[nb_table], list_insert_bdd)
    report_insertion("duplicate", table_list[nb_table], list_duplicate_bdd)


def topic(mydatabase, mycursor, name_database, bdd_properties, dataframe):
    """
    topics              ['id_topic', 'topic']
    """
    # Initialisation de variable de la fontion: ------------------------------
    nb_table = 9
    nb_column_id = 0
    list_duplicate_bdd, list_insert_bdd = []
    # Initialisation de liste de la table: -----------------------------------
    table_list: list = table_name_in_properties(bdd_properties, name_database)
    column_list: list = column_name_in_properties(bdd_properties, name_database, table_list[nb_table])
    # Lancement de la boucle d'insertion: ------------------------------------
    for row in dataframe.itertuples():
        # Coordination liste de la table et liste des valeurs: ---------------
        value_topic = [row.id, row.type, row.title, row.date, row.description]
        id_of_insert = check_for_insertion(mydatabase, mycursor, column_list, value_topic, name_database, table_list[nb_table], nb_column_id)
        if type(id_of_insert) == list and id_of_insert != []:
            list_duplicate_bdd.append(id_of_insert)
        elif type(id_of_insert) == str:
            list_insert_bdd.append(id_of_insert)
    # Création du rapport d'insertion: ---------------------------------------
    report_insertion("insert", table_list[nb_table], list_insert_bdd)
    report_insertion("duplicate", table_list[nb_table], list_duplicate_bdd)


def keyword(mydatabase, mycursor, name_database, bdd_properties, dataframe):
    """
    keywords            ['id_keyword', 'keyword']
    """
    # Initialisation de variable de la fontion: ------------------------------
    nb_table = 4
    nb_column_id = 0
    list_duplicate_bdd, list_insert_bdd = []
    # Initialisation de liste de la table: -----------------------------------
    table_list: list = table_name_in_properties(bdd_properties, name_database)
    column_list: list = column_name_in_properties(bdd_properties, name_database, table_list[nb_table])
    # Lancement de la boucle d'insertion: ------------------------------------
    for row in dataframe.itertuples():
        # Coordination liste de la table et liste des valeurs: ---------------
        value_keyword = [row.id, row.type, row.title, row.date, row.description]
        id_of_insert = check_for_insertion(mydatabase, mycursor, column_list, value_keyword, name_database, table_list[nb_table], nb_column_id)
        if type(id_of_insert) == list and id_of_insert != []:
            list_duplicate_bdd.append(id_of_insert)
        elif type(id_of_insert) == str:
            list_insert_bdd.append(id_of_insert)
    # Création du rapport d'insertion: ---------------------------------------
    report_insertion("insert", table_list[nb_table], list_insert_bdd)
    report_insertion("duplicate", table_list[nb_table], list_duplicate_bdd)


def playlist_by_element(mydatabase, mycursor, name_database, bdd_properties, dataframe):
    """
    playlist_by_channel ['playlist_id', 'channel_id', 'video_id']
    """
    # Initialisation de variable de la fontion: ------------------------------
    nb_table = 5
    nb_column_id = 0
    list_duplicate_bdd, list_insert_bdd = []
    # Initialisation de liste de la table: -----------------------------------
    table_list: list = table_name_in_properties(bdd_properties, name_database)
    column_list: list = column_name_in_properties(bdd_properties, name_database, table_list[nb_table])
    # Lancement de la boucle d'insertion: ------------------------------------
    for row in dataframe.itertuples():
        # Coordination liste de la table et liste des valeurs: ---------------
        value_playlist_by_element = [row.id, row.type, row.title, row.date, row.description]
        id_of_insert = check_for_insertion(mydatabase, mycursor, column_list, value_playlist_by_element, name_database, table_list[nb_table], nb_column_id)
        if type(id_of_insert) == list and id_of_insert != []:
            list_duplicate_bdd.append(id_of_insert)
        elif type(id_of_insert) == str:
            list_insert_bdd.append(id_of_insert)
    # Création du rapport d'insertion: ---------------------------------------
    report_insertion("insert", table_list[nb_table], list_insert_bdd)
    report_insertion("duplicate", table_list[nb_table], list_duplicate_bdd)


def propriete_channel(mydatabase, mycursor, name_database, bdd_properties, dataframe):
    """
    propretie_channel   ['channel_id', 'viewCount', 'likesCount', 'videoCount', 'hiddenSubscridenCount', 'subscriberCount']
    """
    # Initialisation de variable de la fontion: ------------------------------
    nb_table = 6
    nb_column_id = 0
    list_duplicate_bdd, list_insert_bdd = []
    # Initialisation de liste de la table: -----------------------------------
    table_list: list = table_name_in_properties(bdd_properties, name_database)
    column_list: list = column_name_in_properties(bdd_properties, name_database, table_list[nb_table])
    # Lancement de la boucle d'insertion: ------------------------------------
    for row in dataframe.itertuples():
        # Coordination liste de la table et liste des valeurs: ---------------
        value_propriete_channel = [row.id, row.type, row.title, row.date, row.description]
        id_of_insert = check_for_insertion(mydatabase, mycursor, column_list, value_propriete_channel, name_database, table_list[nb_table], nb_column_id)
        if type(id_of_insert) == list and id_of_insert != []:
            list_duplicate_bdd.append(id_of_insert)
        elif type(id_of_insert) == str:
            list_insert_bdd.append(id_of_insert)
    # Création du rapport d'insertion: ---------------------------------------
    report_insertion("insert", table_list[nb_table], list_insert_bdd)
    report_insertion("duplicate", table_list[nb_table], list_duplicate_bdd)


def propriete_video(mydatabase, mycursor, name_database, bdd_properties, dataframe):
    """
    propretie_video     ['video_id', 'viewCount', 'likesCount', 'videoCount', 'commentCount', 'duration', 'privacyStatus']
    """
    # Initialisation de variable de la fontion: ------------------------------
    nb_table = 7
    nb_column_id = 0
    list_duplicate_bdd, list_insert_bdd = []
    # Initialisation de liste de la table: -----------------------------------
    table_list: list = table_name_in_properties(bdd_properties, name_database)
    column_list: list = column_name_in_properties(bdd_properties, name_database, table_list[nb_table])
    # Lancement de la boucle d'insertion: ------------------------------------
    for row in dataframe.itertuples():
        # Coordination liste de la table et liste des valeurs: ---------------
        value_propriete_video = [row.id, row.type, row.title, row.date, row.description]
        id_of_insert = check_for_insertion(mydatabase, mycursor, column_list, value_propriete_video, name_database, table_list[nb_table], nb_column_id)
        if type(id_of_insert) == list and id_of_insert != []:
            list_duplicate_bdd.append(id_of_insert)
        elif type(id_of_insert) == str:
            list_insert_bdd.append(id_of_insert)
    # Création du rapport d'insertion: ---------------------------------------
    report_insertion("insert", table_list[nb_table], list_insert_bdd)
    report_insertion("duplicate", table_list[nb_table], list_duplicate_bdd)


def search(mydatabase, mycursor, name_database, bdd_properties, dataframe):
    """
    search              ['element_id', 'element_search_id', 'kind']
    """
    # Initialisation de variable de la fontion: ------------------------------
    nb_table = 8
    nb_column_id = 0
    list_duplicate_bdd, list_insert_bdd = []
    # Initialisation de liste de la table: -----------------------------------
    table_list: list = table_name_in_properties(bdd_properties, name_database)
    column_list: list = column_name_in_properties(bdd_properties, name_database, table_list[nb_table])
    # Lancement de la boucle d'insertion: ------------------------------------
    for row in dataframe.itertuples():
        # Coordination liste de la table et liste des valeurs: ---------------
        value_search = [row.id, row.type, row.title, row.date, row.description]
        id_of_insert = check_for_insertion(mydatabase, mycursor, column_list, value_search, name_database, table_list[nb_table], nb_column_id)
        if type(id_of_insert) == list and id_of_insert != []:
            list_duplicate_bdd.append(id_of_insert)
        elif type(id_of_insert) == str:
            list_insert_bdd.append(id_of_insert)
    # Création du rapport d'insertion: ---------------------------------------
    report_insertion("insert", table_list[nb_table], list_insert_bdd)
    report_insertion("duplicate", table_list[nb_table], list_duplicate_bdd)

