import pandas as pd
from utile_mysql import connection_mysql, cursor

def sql_to_df(mydb):
    sql = f"""
    SELECT product_country.`id_order`, product.`name` AS `product`, country.`name` AS `country`, 
    product_country.`category`, product_country.`amount`, product_country.`date`
    FROM product_country
    JOIN product ON product_country.`id_product` = product.`id_product`
    JOIN country ON product_country.`id_country` = country.`id_country`
    WHERE product_country.`id_product` in (SELECT product.`id_product` FROM product)
    AND product_country.`id_country` in (SELECT country.`id_country` FROM country);
    """
    df = pd.read_sql(sql, mydb)
    return df


def sql_to_df_TCD_sum(pays: str, mydb):
    sql_sum = f"""
    SELECT product.`name` AS `product`, country.`name` AS `country`, 
    product_country.`category`, 
    SUM(product_country.`amount`) AS `Sum of Amount`
    FROM product_country
    JOIN product ON product_country.`id_product` = product.`id_product`
    JOIN country ON product_country.`id_country` = country.`id_country`
    WHERE product_country.`id_product` in (
        SELECT product.`id_product` FROM product)
    AND product_country.`id_country` in (
        SELECT country.`id_country` FROM country WHERE `name` = "{pays}")
    GROUP BY product.`name`, country.`name`, product_country.`category`;
    """
    df = pd.read_sql(sql_sum, mydb)
    df["Grand Total"] = df["Sum of Amount"].sum()
    df_pivot = df.pivot(index=["country", "Grand Total"], columns="product", values="Sum of Amount")
    return df_pivot


def sql_to_df_TCD_count(pays: str, mydb):
    sql_count = f"""
        SELECT product.`name` AS `product`, country.`name` AS `country`, 
        product_country.`category`, 
        COUNT(product_country.`amount`) AS `Count of Amount`
        FROM product_country
        JOIN product ON product_country.`id_product` = product.`id_product`
        JOIN country ON product_country.`id_country` = country.`id_country`
        WHERE product_country.`id_product` in (
            SELECT product.`id_product` FROM product)
        AND product_country.`id_country` in (
            SELECT country.`id_country` FROM country WHERE `name` = "{pays}")
        GROUP BY product.`name`, country.`name`, product_country.`category`;
    """
    df = pd.read_sql(sql_count, mydb)
    df["Grand Total"] = df["Count of Amount"].count()
    df_pivot = df.pivot(index="product", columns=["country", "Grand Total"], values="Count of Amount")
    return df_pivot

# Avec pandas faire des tables équivalentes aux TCD du fichier.
if __name__ == "__main__":
    mydb = connection_mysql()
    mycursor = cursor(mydb)

    df_sum = sql_to_df_TCD_sum("united states", mydb)
    print(df_sum)
    df_count = sql_to_df_TCD_count("france", mydb)
    print(df_count)
    df = sql_to_df(mydb)
    print(df)
