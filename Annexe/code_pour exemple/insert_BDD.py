import mysql.connector
from Préparation.Python.code_export_via_project.mysql_utile import *


# Initialisation SQL: ------------------------------------------------------------------
list_of_bdd = ["Projet_Examen"]

cond_dictionary=False
# dictionary = False, récup en liste pour refaire en dict via fonction
# dictionary = True, ne retourne pas dans l'ordre

# Connection SQL: ------------------------------------------------------------------
db_dict = connexion_bdd(mysql.connector, list_of_bdd)
mydb = db_dict['Projet_Examen']
mycursor = mydb.cursor(dictionary=cond_dictionary)

### mise en forme à faire
list_table_bdd = list_of_table_bdd(mycursor)

mydb.reconnect() # si deconnnection

# Récupération data SQL: ------------------------------------------------------------------
sql = """SELECT country.`Code`, country.`Code2`, country.`Name`, countrylanguage.`Language` 
        FROM country
        JOIN countrylanguage ON country.`Code` = countrylanguage.`CountryCode`
        WHERE countrylanguage.`IsOfficial` = "T";"""
mycursor.execute(sql)
# Liste des langues officielles importés
dico_lang_off_by_country = mycursor.fetchall()

# OR

index = 0
list_dico = select_bdd(mycursor, list_table_bdd[index], "*")

# Insertion SQL: ------------------------------------------------------------------
columns_many_table = many_column_str(list_dico)
columns_many_value = format_many_column_str(list_dico)
mycursor.executemany(f"""INSERT INTO {list_table_bdd[index]} ({columns_many_table}) 
                            VALUES ({columns_many_value})""", list_dico)

"""
nbr_résultat = {"nbr_current": 0, "nbr_total": 0}
liste_in_dico = {"liste_in_dico[0]": "title",
                    "liste_in_dico[1]": "id > videoId / channelId / playlistId",
                    "liste_in_dico[2]": "publishedAt",
                    "liste_in_dico[3]": "description",
                    "liste_in_dico[4]": "channelId"}
"""

def transfert_in_database(liste_in_dico, index):
    # list_not_insert_in_bdd = [] # list[[], [], [], []]
    if liste_in_dico == None:
        list_not_insert_in_bdd = []
        return list_not_insert_in_bdd
    list_dico = list_to_dict(list_table_bdd, liste_in_dico)
    columns_many_table = many_column_str(list_dico)
    columns_many_value = format_many_column_str(list_dico)
    mycursor.executemany(f"""INSERT INTO {list_table_bdd[index]} ({columns_many_table}) 
                                VALUES ({columns_many_value})""", list_dico)
    
    return list_not_insert_in_bdd



# Déconnexion SQL: ------------------------------------------------------------------
print("Table en cours:", list_table_bdd[index])
print("Nombre de lignes insérées :", mycursor.rowcount)
mydb.commit()
mydb.close()