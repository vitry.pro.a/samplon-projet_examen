import aspose.cells 
from aspose.cells import Workbook

file_1 = "scearch_data"
file_2 = "channel_data"
file_3 = "video_data"
file_4 = "playlist_data"

path_intput = f"Annexe/input/{file_4}.xlsx"
path_output = f"Annexe/output/{file_4}.md"

workbook = Workbook(path_intput)
workbook.save(path_output)

"""Vous pouvez également convertir EXCEL vers de nombreux autres formats de fichiers, dont quelques-uns répertoriés ci-dessous.
EXCEL AU BMP
(Image bitmap)
EXCEL AU EMF
(Format de métafichier amélioré)
EXCEL AU GIF
(Format d\'échange graphique)
EXCEL AU HTML
(Langage Signalétique Hyper Text)
EXCEL VERS MD
(Langage de démarque)
EXCEL AU MHTML
(Format d\'archive de pages Web)
EXCEL AU ODS
(Fichier de feuille de calcul OpenDocument)
EXCEL AU PDF
(Portable Document Format)
EXCEL AU PNG
(Portable Network Graphics)
EXCEL AU SVG
(Image Vectorielle)
EXCEL AU TIFF
(Format d\'image balisé)
EXCEL AU TSV
(Valeurs séparées par des tabulations)
EXCEL AU TXT
(Document texte)
EXCEL AU XLS
(Format binaire Excel)
EXCEL AU XLSB
(Fichier de classeur Excel binaire)
EXCEL AU XLSM
(Fichier de feuille de calcul)
EXCEL AU XLSX
(Fichier Excel OOXML)
EXCEL AU XLT
(Modèle Excel Microsoft)
EXCEL AU XLTM
(Modèle Excel compatible avec les macros)
EXCEL AU XLTX
(Modèle Excel Office OpenXML)
EXCEL VERS XML
(Langage de balisage extensible)
EXCEL AU XPS
(Spécifications du papier XML)
EXCEL AU JSON
(Notation d\'objet JavaScript)"""