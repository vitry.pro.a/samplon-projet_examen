# Structure du projet

## Introduction : idée retenu

### Faisabilité :  
API youtube complexe: clef d'api utile trop complexe à déterminer (dimension / clef)

### Thématique :  
sur les youtubeurs qui ont plus 1M d'abonné, quels sont les éléments qui reviennent le plus? 
(vue, vidéo, thème de vidéo, nbr video publié etc...)

---

## Chapitre 1 : Analyse

### Analyse de l'API Youtube Data :
Sources de datas - Extraction et communication avec API
Echantillon par filtre:
n > 1/7 
soit 15% de la taille du nbr du total

### Analyses des Vues :
1er vue - liste des chaines de youtubeurs avec leurs nbr abonnées et leurs vues et nbr vidéos (Totaux de stats)
2ème vue - pour chaque chaine de youtubeur, affichage en fonction de la date de création et d'aujourd'hui avec les dates de publication des vidéos -> (dates + count + thèmes)

---

## Chapitre 2 : Modèles des données MLD

### MCD: Niveau conceptuel (MCD)

### MLD: Niveau logique (MLD)

### MPD: Niveau physique (MPD)