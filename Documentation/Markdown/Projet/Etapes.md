# Chronologie des étapes:

## 18 / 04 / 2024: Documentation / Etapes / ...
- Etablissement des objectifs de la semaine:
    1. résumé du projet certif en 2 phrases
    2. les vues (quoi, comment, pourquoi)
    3. les sources de données
    4. une liste des users stories (au moins deux)
    5. Faire de la documentation

## 12 / 04 / 2024: Préparation / Etapes / Analyse
- Recherches de la viabilité de(s) idée(s): 1 seule idée viable
- Recherches des "set data" pour l'insertion de données dans une BDD
- Recherches des vues possible en parallèle des "set data" pour l'extration d'une BDD
- Recherches en parallèle du fonctionnement de l'API Youtube Data
- Etablissement de l'idée du projet, après avoir statuer de la viabilité de l'idée: 
        > Sur les youtubeurs qui ont plus 1M d'abonné:
        > Quels sont les éléments qui reviennent le plus?
- Etablissement des objectifs de la semaine:
    1. Liste des sources
    2. Liste des vues
    3. modélisation du logiciel: MCD / MLD / modele de la vue / modeles-diagrame etc....

        ### Bilan de la semaine:
            - idée de projet trouvée
            - Sources des données trouvées
            - liste de 2 vues trouvées
            - Pas de modélisation, seulement JSON data youtube

## 11 / 04 / 2024: Start project - Contexte
- Etablissement du contexte: Project Examen de fin de formation
- Recherches d'idées pour le projet
- Recherches de la viabilité de(s) idée(s): 4 idées trouvées