# Niveau logique (MLD)

## Définition:  
Les entités mises en relation deviennent des tables.

EMPLOYE: id_emp, nom_emp, preno_emp, daten_emp, adres_emp
COMMUNE: id_com, nom_com, codep_com
SERVICE: id_ser, nom_ser


## Méthodologie:

![MCD_to_MLD](../Ressources/MCD_to_MLD.png)


## MLD simplifier:

![MLD](../Ressources/MLD_simplifier.png)

### Dimensions Search:
"""  
"id":  
    "kind": string  
    "videoId": string  
    "channelId": string  
    "playlistId": string  
"""  

- Search:  
"kind": "youtube#searchResult"  
"channelId": string  
"channelTitle": string  

- Playlist:  
"kind": "youtube#playlist"  
"id": string  **ou** "channelId": string  
"title": string  


### Dimensions Channel:
"title": string                     ==> Catégorie: Type de chaine  
"description": string               ==> Catégorie: Type de chaine  
"publishedAt": datetime             ==> Catégorie: Type de chaine  
"country": string                   ==> Catégorie: Type de chaine  
"hiddenSubscriberCount": boolean    ==> Catégorie: Type de chaine  
"topicIds": [string]                ==> Catégorie: Type de chaine    

"likes": string                     ==> Catégorie: Statistiques chaine  
"uploads": string                   ==> Catégorie: Statistiques chaine  (Id playlist)  
"viewCount": unsigned long          ==> Catégorie: Statistiques chaine  
"videoCount": unsigned long         ==> Catégorie: Statistiques chaine  
"subscriberCount": unsigned long    ==> Catégorie: Statistiques chaine  


### Dimensions Vidéo:
"id": string                        ==> Catégorie: Type de video  
"publishedAt": datetime             ==> Catégorie: Type de video  
"channelId": string                 ==> Catégorie: Type de video  
"title": string                     ==> Catégorie: Type de video  
"description": string               ==> Catégorie: Type de video    
"country": string                   ==> Catégorie: Type de video  
"tags": [string]                    ==> Catégorie: Type de video  
"duration": string                  ==> Catégorie: Type de video  
"privacyStatus": string             ==> Catégorie: Type de video  
"relevantTopicIds": [string]        ==> Catégorie: Type de video  

"viewCount": string                 ==> Catégorie: Statistiques video  
"likeCount": string                 ==> Catégorie: Statistiques video  
"dislikeCount": string              ==> Catégorie: Statistiques video  
"commentCount": string              ==> Catégorie: Statistiques video  



> Champs vérifié et validé le 15/05: reste mettre à jour documentation entière

Erratum: duration en ms retiré


## MLD détailler:

![MLD](../Ressources/MCD_BDD.png)

![MLD](../Ressources/MDL_BDD.png)

![MLD](../Ressources/MLD_Final.png)


COMMENT OBTENIR LES STATS EN FONCTION DE LA DATE????  



Interroger l'API YouTube Analytics pour les reportages vidéo peut renvoyer les métriques d'une vidéo pendant une période spécifique.  


