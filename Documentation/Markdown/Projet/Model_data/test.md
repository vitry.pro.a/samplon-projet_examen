```json
{
    "kind": "youtube#channel",
    "id": string,
    },
    "contentDetails": {
        "relatedPlaylists": {
            "likes": string,
            "favorites": string,
            "uploads": string
        }
    },
    "statistics": {
        "viewCount": unsigned long,
        "subscriberCount": unsigned long, // this value is rounded to three significant figures
        "hiddenSubscriberCount": boolean,
        "videoCount": unsigned long
    },
    "topicDetails": {
        "topicIds": [
        string
        ],
        "topicCategories": [
        string
        ]
    },
    "brandingSettings": {
        "channel": {
            "title": string,
            "description": string,
            "unsubscribedTrailer": string,
            "country": string
        }
    },
    "localizations": {
        (key): {
            "title": string,
            "description": string
        }
    }
}
{
    "kind": "youtube#video",
    "id": string,
    "snippet": {
        "channelId": string,
        "tags": [string],
        "categoryId": string
    },
    "contentDetails": {
        "duration": string,
        "dimension": string,
        "definition": string,
        "caption": string
    },
    "status": {
        "uploadStatus": string,
        "publishAt": datetime,
        "publicStatsViewable": boolean
    },
    "statistics": {
        "viewCount": string,
        "likeCount": string,
        "dislikeCount": string,
        "favoriteCount": string,
        "commentCount": string
    },
    "topicDetails": {
        "topicIds": [
        string
        ],
        "relevantTopicIds": [
        string
        ],
        "topicCategories": [
        string
        ]
    },
    "recordingDetails": {
        "recordingDate": datetime
    },
    "fileDetails": {
        "fileName": string,
        "fileSize": unsigned long,
        "fileType": string,
        "container": string,
        "durationMs": unsigned long,
        "bitrateBps": unsigned long,
        "creationTime": string
    },
    "localizations": {
        (key): {
            "title": string,
            "description": string
        }
    }
}
```