# Idées
Idee 1: Rejeté
Faisabilité : Impossible en python pour l'extension, uniquement le scraping, pas de front donc pas d'interaction pour donnée le choix à l'utilisateur
thème : sur le navigateur, récup la page puis retourner sa structure simplifié
Objectif: via extension navigateur, choisir une URL et retourner l'arbre dom de la page avec quel data à étudier dans la BDD
choisir les datas à retourner pour choisir les datas à save (BDD)
------------------------------------------------
idee 2: Rejeté
Faisabilité : Dataset à creer via Scraping de multiple page (wikipédia) + doc à récup pour la base d'arbre de recherche
Arbre de recherche: méthode scientifique > discipline scientifique > métiers de recherche ou ingéniérie > outils (mesures, expérimentation, recherches, etc)
thème : récupérer la technologie associé à une compétence scientifique
ex: spectroscopie donne quels outils?
------------------------------------------------
idee 3 : Validé -> faisable
Faisabilité : API youtube complexe: clef d'api utile trop complexe à déterminer (dimension / clef)
thème : sur les youtubeurs qui ont plus 1M d'abonné, quels sont les éléments qui reviennent le plus? (vue, vidéo, thème de vidéo, nbr video publié etc...)
------------------------------------------------
idee 4 : Rejeté
Faisabilité : Data analyse de dataset randoms
thème : superposition de data sur un élément commun... location, time, etc..
------------------------------------------------
