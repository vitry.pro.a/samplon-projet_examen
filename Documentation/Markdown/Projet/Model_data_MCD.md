# Niveau conceptuel (MCD)

## Définition:  
Le MCD permet de représenter le système d'information indépendamment de son aspect informatique, il doit être compréhensible par tous: informaticiens, employés, secrétaires, personnel de direction, etc.  

Pour ce faire la méthode Merise utilise 2 représentations:  

- **l'entité:** elle regroupe l'information statique et durable.
    Par exemple, l’entité employe rassemble toutes les informations communes aux employés d'une entreprise
    Une entité est représentée par un nom commun écrit en majuscules et au sungulier.
    Exemples: EMPLOYE, DIPLOME, COMMUNE, FOURNISSEUR, etc.  
- **l'association:** elle matérialise la dynamique du système et donc les relations entre les entités.
    Par exemple, l'entité employe est en relation avec l'entité service de l'entreprise.
    L'association (ou relation) est représentée par un verbe d'action ou d'éta à l'infinitif.
    Exemples: HABITER, LOUER, JOUER, CARACTERISER, etc.


## Data Youtube:  

![MCD](../Ressources/Schema_by_scearch.png)

Search: permet d'avoir un liste de channel selon un critère ici pays (FR)  
Channel: permet d'avoir les caractéristiques d'un channel (id_channel)  
Playlist: permet d'avoir un liste de vidéo selon un critère ici channel (id_channel)  
Video: permet d'avoir les caractéristiques d'une vidéo (id_video)  

![MCD](../Ressources/MCD.png)


### Dimensions Search:
"kind": "youtube#searchResult"      ==> OK pour Recherche de chaine  
"videoId": string                   ==> Non utile  
"channelId": string                 ==> OK pour Recherche de chaine  
"playlistId": string                ==> Non utile  
"publishedAt": datetime             ==> Doublon  
"channelId": string                 ==> Doublon  
"title": string                     ==> Doublon  
"description": string               ==> Doublon  
"channelTitle": string              ==> OK pour Recherche de chaine  

### Dimensions Channel:
"kind": "youtube#channel"           ==> Catégorie: Type de recherche  
"id": string                        ==> Doublon  
"title": string                     ==> Catégorie: Type de chaine  
"description": string               ==> Catégorie: Type de chaine  
"publishedAt": datetime             ==> Catégorie: Type de chaine  
"country": string                   ==> Catégorie: Type de chaine  
"likes": string                     ==> Catégorie: Statistiques chaine  
"favorites": string                 ==> Catégorie: Statistiques chaine  
"uploads": string                   ==> Catégorie: Statistiques chaine  
"viewCount": unsigned long          ==> Catégorie: Statistiques chaine  
"subscriberCount": unsigned long    ==> Catégorie: Statistiques chaine  
"hiddenSubscriberCount": boolean    ==> Catégorie: Statistiques chaine  
"videoCount": unsigned long         ==> Catégorie: Statistiques chaine  
"topicIds": [string]                ==> Catégorie: Statistiques chaine  
"topicCategories": [string]         ==> Catégorie: Statistiques chaine  
"title": string                     ==> Catégorie: Type de chaine  
"description": string               ==> Catégorie: Type de chaine  
"unsubscribedTrailer": string       ==> inutile
"country": string                   ==> Catégorie: Type de chaine  
"title": string                     ==> Catégorie: Type de chaine  
"description": string               ==> Catégorie: Type de chaine  

### Dimensions Playlist:
"kind": "youtube#playlist"          ==> OK pour Recherche de video  
"id": string                        ==> OK pour Recherche de video  -> id_video   
"publishedAt": datetime             ==> Doublon  
"channelId": string                 ==> OK pour Recherche de video  -> id_channel  
"title": string                     ==> OK pour Recherche de video    
"description": string               ==> Doublon    
"title": string                     ==> Doublon  
"description": string               ==> Doublon  

### Dimensions Vidéo:
"kind": "youtube#video"             ==> Catégorie: Type de recherche  
"id": string                        ==> Catégorie: Type de video  
"publishedAt": datetime             ==> Catégorie: Type de video  
"channelId": string                 ==> Catégorie: Type de video  
"title": string                     ==> Catégorie: Type de video  
"description": string               ==> Catégorie: Type de video    
"tags": [string]                    ==> Catégorie: Type de video  
"categoryId": string                ==> Catégorie: Statistiques video  
"duration": string                  ==> Catégorie: Type de video  
"dimension": string                 ==> Catégorie: Type de video  
"definition": string                ==> Catégorie: Type de video  
"caption": string                   ==> Catégorie: Type de video  
"uploadStatus": string              ==> Catégorie: Type de video  
"publishAt": datetime               ==> Catégorie: Type de video  
"publicStatsViewable": boolean      ==> Catégorie: Statistiques video  
"viewCount": string                 ==> Catégorie: Statistiques video  
"likeCount": string                 ==> Catégorie: Statistiques video  
"dislikeCount": string              ==> Catégorie: Statistiques video  
"favoriteCount": string             ==> Catégorie: Statistiques video  
"commentCount": string              ==> Catégorie: Statistiques video  
"topicIds": [string]                ==> Catégorie: Type de video  
"relevantTopicIds": [string]        ==> Catégorie: Type de video  
"topicCategories": [string]         ==> Catégorie: Type de video  
"recordingDate": datetime           ==> Catégorie: Type de video  
"fileName": string                  ==> Catégorie: Type de video  
"fileSize": unsigned long           ==> Catégorie: Type de video  
"fileType": string                  ==> Catégorie: Type de video  
"container": string                 ==> Catégorie: Type de video  
"durationMs": unsigned long         ==> Catégorie: Type de video  
"bitrateBps": unsigned long         ==> Catégorie: Type de video  
"creationTime": string              ==> Catégorie: Type de video  
"title": string                     ==> Catégorie: Type de video  
"description": string               ==> Catégorie: Type de video  