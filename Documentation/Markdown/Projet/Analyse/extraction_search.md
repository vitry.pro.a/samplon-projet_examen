# Format JSON

```json
{
    "kind": "youtube#searchResult",
    "etag": etag,
    "id": {
        "kind": string,
        "videoId": string,
        "channelId": string,
        "playlistId": string
    },
    "snippet": {
        "publishedAt": datetime,
        "channelId": string,
        "title": string,
        "description": string,
        "thumbnails": {
        (key): {
                "url": string,
                "width": unsigned integer,
                "height": unsigned integer
            }
        },
        "channelTitle": string,
        "liveBroadcastContent": string
    }
}
```

# Search et statue (à conserver ou jeter ou à vérifier)

    "kind": "youtube#searchResult"      ==>     à conserver
    "etag": etag                        ==>     à jeter
    "id": dict                          ==>     à conserver
    "snippet": dict                     ==>     à conserver

> - ## Kind
Information du type de dictionnaire: ici scearch of channel

> - ## id
Information de l'identifiant du résultat

    "kind": string                      ==>     à conserver
    "videoId": string                   ==>     à conserver
    "channelId": string                 ==>     à conserver
    "playlistId": string                ==>     à conserver

> - ## snippet
Information de base du résultat

    "publishedAt": datetime             ==>     à conserver
    "channelId": string                 ==>     à conserver
    "title": string                     ==>     à conserver
    "description": string               ==>     à conserver
    "thumbnails": dict                  ==>     à jeter
    "channelTitle": string              ==>     à conserver
    "liveBroadcastContent": string      ==>     à jeter


---


# Récapitulatif de Search:

```json
{
    "kind": "youtube#searchResult",
    "id": {
        "kind": string,
        "videoId": string,
        "channelId": string,
        "playlistId": string
    },
    "snippet": {
        "publishedAt": datetime,
        "channelId": string,
        "title": string,
        "description": string,
        "channelTitle": string
    }
}
```

---

|Propriétés | |
|-|-|
|tablescraper-selected-row|tablescraper-selected-row 2 <br> apitype <br> tablescraper-selected-row 3 <br> quiet <br> tablescraper-selected-row 4|
|kind|Identifie le type de ressource de l&apos;API. La valeur sera . <br> string <br> youtube#searchResult <br>  <br> |
|etag|ETag de cette ressource. <br> etag <br>  <br>  <br> |
|id|L&apos;objet  contient des informations permettant d&apos;identifier de manière unique la ressource correspondant à la requête de recherche. <br> object <br> id <br>  <br> |
|kind|Type de la ressource API. <br> string <br>  <br> id. <br> |
|videoId|Si la valeur de la propriété  est , cette propriété sera présente et sa valeur contiendra l&apos;ID utilisé par YouTube pour identifier de façon unique une vidéo correspondant à la requête de recherche. <br> string <br> id.type <br> id. <br> youtube#video|
|channelId|Si la valeur de la propriété  est , cette propriété sera présente et sa valeur contiendra l&apos;ID utilisé par YouTube pour identifier de façon unique une chaîne correspondant à la requête de recherche. <br> string <br> id.type <br> id. <br> youtube#channel|
|playlistId|Si la valeur de la propriété  est , cette propriété sera présente et sa valeur contiendra l&apos;ID utilisé par YouTube pour identifier de façon unique une playlist correspondant à la requête de recherche. <br> string <br> id.type <br> id. <br> youtube#playlist|
|snippet|L&apos;objet  contient des détails de base sur un résultat de recherche, tels que son titre ou sa description. Par exemple, si le résultat de la recherche est une vidéo, le titre sera le titre de la vidéo et la description sera la description de la vidéo. <br> object <br> snippet <br>  <br> |
|publishedAt|Date et heure de création de la ressource identifiée par le résultat de recherche. La valeur est spécifiée au format . <br> datetime <br> ISO 8601 <br> snippet. <br> |
|channelId|Valeur utilisée par YouTube pour identifier de façon unique la chaîne sur laquelle la ressource identifiée par les résultats de recherche a été publiée <br> string <br>  <br> snippet. <br> |
|title|Titre du résultat de recherche. <br> string <br>  <br> snippet. <br> |
|description|Description du résultat de recherche. <br> string <br>  <br> snippet. <br> |
|thumbnails|Vignette associée au résultat de recherche. Pour chaque objet de la carte, la clé est le nom de la vignette et la valeur est un objet contenant d&apos;autres informations sur la vignette. <br> object <br>  <br> snippet. <br> |
|(key)|Les clés-valeurs valides sont les suivantes : <br> object <br> : vignette par défaut. La miniature par défaut d&apos;une vidéo, ou une ressource faisant référence à une vidéo, telle qu&apos;un élément de playlist ou un résultat de recherche, fait 120 x 90 pixels de haut. La miniature par défaut d&apos;une chaîne fait 88 x 88 pixels. <br> snippet.thumbnails. <br> default|
|url|URL de l&apos;image. <br> string <br>  <br> snippet.thumbnails.(key). <br> |
|width|Largeur de l&apos;image. <br> unsigned integer <br>  <br> snippet.thumbnails.(key). <br> |
|height|Hauteur de l&apos;image. <br> unsigned integer <br>  <br> snippet.thumbnails.(key). <br> |
|channelTitle|Titre de la chaîne ayant publié la ressource identifiée par le résultat de recherche. <br> string <br>  <br> snippet. <br> |
|liveBroadcastContent|Indique si une ressource  ou  comporte du contenu diffusé en direct. Les valeurs valides pour cette propriété sont ,  et . Pour une ressource , la valeur  indique que la vidéo est une diffusion en direct qui n&apos;a pas encore commencé, tandis que la valeur  indique que la vidéo est une diffusion en direct active. Pour une ressource , la valeur  indique que la diffusion d&apos;une chaîne est en cours, alors qu&apos;une valeur  indique qu&apos;une diffusion en direct est active sur la chaîne. <br> string <br> video <br> snippet. <br> channel|