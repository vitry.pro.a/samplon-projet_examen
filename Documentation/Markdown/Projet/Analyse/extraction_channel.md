# Format JSON

```json
{
    "kind": "youtube#channel",
    "etag": etag,
    "id": string,
    "snippet": {
        "title": string,
        "description": string,
        "customUrl": string,
        "publishedAt": datetime,
        "thumbnails": {
            (key): {
                "url": string,
                "width": unsigned integer,
                "height": unsigned integer
            }
        },
        "defaultLanguage": string,
        "localized": {
            "title": string,
            "description": string
        },
        "country": string
    },
    "contentDetails": {
        "relatedPlaylists": {
            "likes": string,
            "favorites": string,
            "uploads": string
        }
    },
    "statistics": {
        "viewCount": unsigned long,
        "subscriberCount": unsigned long, // this value is rounded to three significant figures
        "hiddenSubscriberCount": boolean,
        "videoCount": unsigned long
    },
    "topicDetails": {
        "topicIds": [
        string
        ],
        "topicCategories": [
        string
        ]
    },
    "status": {
        "privacyStatus": string,
        "isLinked": boolean,
        "longUploadsStatus": string,
        "madeForKids": boolean,
        "selfDeclaredMadeForKids": boolean
    },
    "brandingSettings": {
        "channel": {
            "title": string,
            "description": string,
            "keywords": string,
            "trackingAnalyticsAccountId": string,
            "unsubscribedTrailer": string,
            "defaultLanguage": string,
            "country": string
        },
        "watch": {
            "textColor": string,
            "backgroundColor": string,
            "featuredPlaylistId": string
        }
    },
    "auditDetails": {
        "overallGoodStanding": boolean,
        "communityGuidelinesGoodStanding": boolean,
        "copyrightStrikesGoodStanding": boolean,
        "contentIdClaimsGoodStanding": boolean
    },
    "contentOwnerDetails": {
        "contentOwner": string,
        "timeLinked": datetime
    },
    "localizations": {
        (key): {
            "title": string,
            "description": string
        }
    }
}
```


# Channel et statue (à conserver ou jeter ou à vérifier)

    -"kind": "youtube#channel"         ==>     à conserver
    -"etag": etag                      ==>     à jeter
    -"id": string                      ==>     à conserver
    -"snippet": dict                   ==>     à conserver
    -"contentDetails": dict            ==>     à conserver
    -"statistics": dict                ==>     à conserver
    -"topicDetails": dict              ==>     à conserver
    -"status": dict                    ==>     à conserver
    -"brandingSettings": dict          ==>     à conserver
    -"auditDetails": dict              ==>     à jeter
    -"contentOwnerDetails": dict       ==>     à jeter
    -"localizations": dict             ==>     à jeter

Final:
    -"kind": "youtube#channel"         ==>     à conserver
    -"id": string                      ==>     à conserver
    -"snippet": dict                   ==>     à conserver
    -"contentDetails": dict            ==>     à conserver
    -"statistics": dict                ==>     à conserver
    -"topicDetails": dict              ==>     à conserver
    -"status": dict                    ==>     à conserver
    -"brandingSettings": dict          ==>     à conserver
---


> - ## Kind
Information du type de dictionnaire: ici channel

> - ## id
Information de l'identifiant de la chaine

> - ## snippet
Information de base de la chaine

    -"title": string                   ==>     à conserver
    -"description": string             ==>     à conserver
    -"customUrl": string               ==>     à jeter
    -"publishedAt": datetime           ==>     à conserver
    -"thumbnails": dict                ==>     à jeter
    -"defaultLanguage": string         ==>     à jeter
    -"localized": dict                 ==>     à jeter
    -"country": string                 ==>     à conserver

> - ## contentDetails
Information contenant les informations lié à la liste de lecture associées

    -"relatedPlaylists": dict          ==>     à conserver

> - ## statistics
Information de statistique de la chaine

    -"viewCount": unsigned long        ==>     à conserver
    -"subscriberCount": unsigned long  ==>     à conserver
    -"hiddenSubscriberCount": boolean  ==>     à conserver
    -"videoCount": unsigned long       ==>     à conserver

> - ## topicDetails
Information de la catégorie de la chaine

    -"topicIds": [string]              ==>     à conserver
    -"topicCategories": [string]       ==>     à conserver

> - ## brandingSettings
Information de

    -"channel": dict                   ==>     à conserver
    -"watch": dict                     ==>     à jeter

> - ## localizations
Information de la localisation: ici FR clef de la requête 

    -"(key)": dict                     ==>     à jeter


---


>> - ### relatedPlaylists
Information des j'aimes, playlist, et les importations

    "likes": string                     ==>     à conserver
    "favorites": string                 ==>     à conserver
    "uploads": string                   ==>     à conserver

>> - ### channel
Information

    "title": string                         ==>     à conserver
    "description": string                   ==>     à conserver
    "keywords": string                      ==>     à jeter
    "trackingAnalyticsAccountId": string    ==>     à jeter
    "unsubscribedTrailer": string           ==>     à conserver
    "defaultLanguage": string               ==>     à jeter
    "country": string                       ==>     à conserver


>> - ### KEY: localisation FR
Information de l'emplacement de la chaine

    - "title": string                   ==>     à conserver
    - "description": string             ==>     à conserver


---


# Récapitulatif de Channel:

First Header | Second Header | Third Header
------------ | ------------- | ------------
Content Cell | Content Cell  | Content Cell
Content Cell | Content Cell  | Content Cell




```json
{
    "kind": "youtube#channel",
    "id": string,
    "snippet": {
        "title": string,
        "description": string,
        "publishedAt": datetime,
        "country": string
    },
    "contentDetails": {
        "relatedPlaylists": {
            "likes": string,
            "favorites": string,
            "uploads": string
        }
    },
    "statistics": {
        "viewCount": unsigned long,
        "subscriberCount": unsigned long, // this value is rounded to three significant figures
        "hiddenSubscriberCount": boolean,
        "videoCount": unsigned long
    },
    "topicDetails": {
        "topicIds": [
        string
        ],
        "topicCategories": [
        string
        ]
    },
    "brandingSettings": {
        "channel": {
            "title": string,
            "description": string,
            "unsubscribedTrailer": string,
            "country": string
        }
    },
    "localizations": {
        (key): {
            "title": string,
            "description": string
        }
    }
}
```


---

|Propriétés | |
|-|-|
|tablescraper-selected-row|tablescraper-selected-row 2 <br> apitype <br> tablescraper-selected-row 3 <br> quiet <br> tablescraper-selected-row 5|
|kind|Identifie le type de ressource d'API. La valeur est . <br> string <br> youtube#channel <br>  <br> |
|etag|Etag de cette ressource. <br> etag <br>  <br>  <br> |
|id|ID utilisé par YouTube pour identifier la chaîne de façon unique. <br> string <br>  <br>  <br> |
|snippet|L'objet  contient des informations de base sur la chaîne, comme son titre, sa description et ses miniatures. <br> object <br> snippet <br>  <br> |
|title|Titre de la chaîne. <br> string <br>  <br> snippet. <br> |
|description|Description de la chaîne. La valeur de la propriété ne doit pas comporter plus de 1 000 caractères. <br> string <br>  <br> snippet. <br> |
|customUrl|URL personnalisée de la chaîne. Le  décrit les critères d'éligibilité à remplir pour obtenir une URL personnalisée et explique comment la configurer. <br> string <br>  <br> snippet. <br> |
|publishedAt|La date et l'heure de création de la chaîne. La valeur est spécifiée au format . <br> datetime <br>  <br> snippet. <br> |
|thumbnails|Carte des miniatures associées à la chaîne. Pour chaque objet de la carte, la clé est le nom de la vignette et la valeur est un objet contenant d'autres informations sur la vignette.Lorsque vous affichez des vignettes dans votre application, assurez-vous que votre code utilise les URL d'image exactement telles qu'elles sont renvoyées dans les réponses de l'API. Par exemple, votre application ne doit pas utiliser le domaine  au lieu du domaine  dans une URL renvoyée dans une réponse d'API.Les URL des vignettes de chaîne ne sont disponibles que dans le domaine , comme c'est le cas dans les réponses de l'API. Il est possible que des images corrompues s'affichent dans votre application si elle tente de charger des images YouTube à partir du domaine . Les miniatures peuvent être vides pour les chaînes nouvellement créées. Un délai d'un jour peut être nécessaire avant qu'elles ne s'affichent. <br> object <br> http <br> snippet. <br> https|
|(key)|Les clés-valeurs valides sont les suivantes: <br> object <br> default <br> snippet.thumbnails. <br> : vignette par défaut La miniature par défaut d'une vidéo (ou d'une ressource faisant référence à une vidéo, comme un élément de playlist ou un résultat de recherche) mesure 120 pixels de large et 90 pixels de haut. La miniature par défaut d'une chaîne fait 88 pixels de large et 88 pixels de haut.|
|url|URL de l'image. Consultez la définition de la propriété  pour obtenir des consignes supplémentaires sur l'utilisation des URL de vignettes dans votre application. <br> string <br>  <br> snippet.thumbnails.(key). <br> |
|width|Largeur de l'image. <br> unsigned integer <br>  <br> snippet.thumbnails.(key). <br> |
|height|Hauteur de l'image. <br> unsigned integer <br>  <br> snippet.thumbnails.(key). <br> |
|defaultLanguage|Langue du texte dans les propriétés  et  de la ressource . <br> string <br> snippet.title <br> snippet. <br> snippet.description|
|localized|L'objet  contient un titre et une description localisés pour la chaîne, ou bien le titre et la description de la chaîne dans la  des métadonnées de la chaîne.  La propriété contient une valeur en lecture seule. Utilisez l'objet  pour ajouter, mettre à jour ou supprimer des métadonnées localisées. <br> object <br> snippet.localized <br> snippet. <br> Le texte localisé est renvoyé dans l'extrait de ressource si la requête  a utilisé le paramètre  pour spécifier une langue pour laquelle le texte localisé doit être renvoyé, si la valeur du paramètre  identifie une  et si le texte localisé est disponible dans cette langue.|
|title|Titre localisé de la chaîne. <br> string <br>  <br> snippet.localized. <br> |
|description|Description localisée de la chaîne. <br> string <br>  <br> snippet.localized. <br> |
|country|Pays auquel la chaîne est associée. Pour définir la valeur de cette propriété, mettez à jour la valeur de la propriété . <br> string <br>  <br> snippet. <br> |
|contentDetails|L'objet  encapsule les informations sur le contenu de la chaîne. <br> object <br> contentDetails <br>  <br> |
|relatedPlaylists|L'objet  est une carte qui identifie les playlists associées à la chaîne, telles que les vidéos mises en ligne sur la chaîne ou les vidéos "J'aime". Vous pouvez récupérer n'importe laquelle de ces playlists à l'aide de la méthode . <br> object <br> relatedPlaylists <br> contentDetails. <br> |
|likes|ID de la playlist contenant les vidéos "J'aime" de la chaîne. Utilisez les méthodes  et  pour ajouter des éléments à cette liste ou en supprimer. <br> string <br> playlistItems.delete <br> contentDetails.relatedPlaylists. <br> |
|favorites|ID de la playlist qui contient les vidéos préférées de la chaîne. Utilisez les méthodes  et  pour ajouter des éléments à cette liste ou en supprimer.Notez que YouTube a abandonné la fonctionnalité de vidéo préférée. Par exemple, la propriété  de la ressource  a été abandonnée le 28 août 2015. Par conséquent, pour des raisons historiques, cette valeur de propriété peut contenir un ID de playlist qui fait référence à une playlist vide et ne peut donc pas être récupérée. <br> string <br> playlistItems.delete <br> contentDetails.relatedPlaylists. <br> Cette propriété a été abandonnée.|
|uploads|ID de la playlist contenant les vidéos mises en ligne par la chaîne. Utilisez la méthode  pour importer de nouvelles vidéos et la méthode  pour supprimer des vidéos déjà mises en ligne. <br> string <br> videos.delete <br> contentDetails.relatedPlaylists. <br> |
|statistics|L'objet  encapsule les statistiques du canal. <br> object <br> statistics <br>  <br> |
|viewCount|Nombre de fois où la chaîne a été regardée. <br> unsigned long <br>  <br> statistics. <br> |
|commentCount|Nombre de commentaires de la chaîne. <br> unsigned long <br>  <br> statistics. <br> Cette propriété est obsolète.|
|subscriberCount|Nombre d'abonnés de la chaîne. Cette valeur est arrondie à trois chiffres significatifs. Consultez l' ou le  pour en savoir plus sur l'arrondi du nombre d'abonnés. <br> unsigned long <br> Centre d'aide YouTube <br> statistics. <br> |
|hiddenSubscriberCount|Indique si le nombre d'abonnés de la chaîne est visible publiquement. <br> boolean <br>  <br> statistics. <br> |
|videoCount|Nombre de vidéos publiques mises en ligne sur la chaîne. Notez que cette valeur reflète uniquement le nombre de vidéos publiques de la chaîne, y compris celles des propriétaires. Ce comportement est cohérent avec celui indiqué sur le site Web YouTube. <br> unsigned long <br>  <br> statistics. <br> |
|topicDetails|L'objet  encapsule les informations sur les thèmes associés à la chaîne. :Consultez la définition de la propriété  et l' pour en savoir plus sur les modifications liées aux ID de thèmes. <br> object <br> topicDetails <br>  <br> topicDetails.topicIds[]|
|topicIds[]|Liste des ID de thèmes associés à la chaîne.:En raison de l'abandon de Freebase et de l'API Freebase, les ID de thèmes ont commencé à fonctionner différemment depuis le 27 février 2017. À cette date, YouTube a commencé à renvoyer un petit ensemble d'ID de thèmes sélectionnés. <br> list <br>  <br> topicDetails. <br> |
|topicCategories[]|Liste d'URL Wikipédia décrivant le contenu de la chaîne. <br> list <br>  <br> topicDetails. <br> |
|status|L'objet  encapsule les informations sur l'état de confidentialité du canal. <br> object <br> status <br>  <br> |
|privacyStatus|État de confidentialité de la chaîne.Les valeurs acceptées pour cette propriété sont les suivantes : <br> string <br> private <br> status. <br> |
|isLinked|Indique si les données de la chaîne identifient un utilisateur déjà associé à un nom d'utilisateur YouTube ou à un compte Google+. Un utilisateur disposant de l'un de ces liens possède déjà une identité YouTube publique, ce qui est indispensable pour plusieurs actions, telles que la mise en ligne de vidéos. <br> boolean <br>  <br> status. <br> |
|longUploadsStatus|Indique si la chaîne peut mettre en ligne des vidéos de plus de 15 minutes. Cette propriété n'est renvoyée que si le propriétaire de la chaîne a autorisé la requête API. Pour en savoir plus sur cette fonctionnalité, consultez le .Les valeurs acceptées pour cette propriété sont les suivantes : <br> string <br> allowed <br> status. <br> : cette chaîne peut mettre en ligne des vidéos de plus de 15 minutes.|
|madeForKids|Cette valeur indique si la chaîne est désignée comme étant destinée aux enfants. Elle indique également qu'elle est actuellement définie comme "conçue pour les enfants". Par exemple, l'état peut être déterminé en fonction de la valeur de la propriété . Consultez le  pour découvrir comment définir l'audience de votre chaîne, de vos vidéos ou de vos diffusions. <br> boolean <br> selfDeclaredMadeForKids <br> status. <br> |
|selfDeclaredMadeForKids|Dans une demande , cette propriété permet au propriétaire de la chaîne de la désigner comme étant destinée aux enfants. La valeur de propriété n'est renvoyée que si le propriétaire du canal a autorisé la requête API. <br> boolean <br>  <br> status. <br> |
|brandingSettings|L'objet  encapsule les informations sur le branding de la chaîne. <br> object <br> brandingSettings <br>  <br> |
|channel|L'objet  encapsule les propriétés de branding de la page de chaîne. <br> object <br> channel <br> brandingSettings. <br> |
|title|Titre de la chaîne. Le titre ne doit pas dépasser 30 caractères. <br> string <br>  <br> brandingSettings.channel. <br> |
|description|La description de la chaîne, qui s'affiche dans la zone "Informations sur la chaîne" sur la page de votre chaîne. La valeur de la propriété ne doit pas comporter plus de 1 000 caractères. <br> string <br>  <br> brandingSettings.channel. <br> |
|keywords|Mots clés associés à votre chaîne. La valeur est une liste de chaînes séparées par des espaces. Les mots clés associés à des critères peuvent être tronqués s'ils dépassent la longueur maximale autorisée de 500 caractères ou s'ils contiennent des guillemets non échappés (). Notez que la limite de 500 caractères ne correspond pas à une limite par mot clé, mais à la longueur totale de tous les mots clés. <br> string <br> " <br> brandingSettings.channel. <br> |
|trackingAnalyticsAccountId|ID du  que vous souhaitez utiliser pour suivre et mesurer le trafic vers votre chaîne. <br> string <br>  <br> brandingSettings.channel. <br> |
|unsubscribedTrailer|Vidéo à lire dans le module de sélection vidéo de l'affichage "Parcourir" de la page de chaîne pour les spectateurs non abonnés. Les spectateurs abonnés peuvent regarder une autre vidéo, qui présente une activité récente sur la chaîne. Si elle est spécifiée, la valeur de la propriété doit correspondre à l'ID vidéo YouTube d'une vidéo publique ou non répertoriée appartenant au propriétaire de la chaîne. <br> string <br>  <br> brandingSettings.channel. <br> |
|defaultLanguage|Langue du texte dans les propriétés  et  de la ressource . <br> string <br> snippet.title <br> brandingSettings.channel. <br> snippet.description|
|country|Pays auquel la chaîne est associée. Mettez à jour cette propriété pour définir la valeur de la propriété . <br> string <br>  <br> brandingSettings.channel. <br> |
|watch|:Cet objet et toutes ses propriétés enfants ont été abandonnés. L'objet  encapsule les propriétés de branding des pages de lecture des vidéos de la chaîne. <br> object <br> watch <br> brandingSettings. <br> |
|textColor|:Cette propriété est obsolète. Couleur du texte de la zone associée à la marque de la page de lecture de la vidéo. <br> string <br>  <br> brandingSettings.watch. <br> |
|backgroundColor|:Cette propriété est obsolète. Couleur d'arrière-plan de la zone de marque de la page de lecture de la vidéo. <br> string <br>  <br> brandingSettings.watch. <br> |
|featuredPlaylistId|:Cette propriété est obsolète. L'API renvoie une erreur si vous tentez de définir sa valeur. <br> string <br>  <br> brandingSettings.watch. <br> |
|image|L'objet  encapsule les informations sur les images qui s'affichent sur la page de chaîne de la chaîne ou sur les pages de lecture des vidéos. <br> object <br> image <br> brandingSettings. <br> Cette propriété et toutes ses propriétés enfants ont été abandonnées.|
|bannerImageUrl|URL de l'image de bannière affichée sur la page de la chaîne sur le site Web YouTube. Elle mesure 1 060 x 175 pixels. <br> string <br>  <br> brandingSettings.image. <br> Cette propriété est obsolète.|
|bannerMobileImageUrl|URL de la bannière affichée sur la page de la chaîne dans les applications mobiles. Elle mesure 640 x 175 pixels. <br> string <br>  <br> brandingSettings.image. <br> Cette propriété est obsolète.|
|watchIconImageUrl|URL de l'image qui s'affiche au-dessus du lecteur vidéo. Cette image fait 25 pixels de haut et sa largeur flexible ne peut pas dépasser 170 pixels. Si vous ne fournissez pas cette image, le nom de votre chaîne s'affichera à la place d'une image. <br> string <br>  <br> brandingSettings.image. <br> Cette propriété est obsolète.|
|trackingImageUrl|URL d'un pixel de suivi de 1 x 1 pixel permettant de collecter des statistiques sur les vues de la chaîne ou des pages de vidéos. <br> string <br>  <br> brandingSettings.image. <br> Cette propriété est obsolète.|
|bannerTabletLowImageUrl|URL d'une bannière à faible résolution qui s'affiche sur la page de chaîne des applications pour tablette. La taille maximale de l'image est de 1 138 x 188 pixels. <br> string <br>  <br> brandingSettings.image. <br> Cette propriété est obsolète.|
|bannerTabletImageUrl|URL d'une image de bannière qui s'affiche sur la page de chaîne dans les applications pour tablette. Elle mesure 1 707 x 283 pixels. <br> string <br>  <br> brandingSettings.image. <br> Cette propriété est obsolète.|
|bannerTabletHdImageUrl|URL d'une bannière haute résolution qui s'affiche sur la page de chaîne des applications pour tablette. La taille maximale de l'image est de 2 276 x 377 pixels. <br> string <br>  <br> brandingSettings.image. <br> Cette propriété est obsolète.|
|bannerTabletExtraHdImageUrl|URL d'une bannière très haute résolution qui s'affiche sur la page de chaîne dans les applications pour tablette. La taille maximale de l'image est de 2 560 x 424 pixels. <br> string <br>  <br> brandingSettings.image. <br> Cette propriété est obsolète.|
|bannerMobileLowImageUrl|URL d'une bannière à faible résolution qui s'affiche sur la page de votre chaîne dans les applications mobiles. La taille maximale de l'image est de 320 x 88 pixels. <br> string <br>  <br> brandingSettings.image. <br> Cette propriété est obsolète.|
|bannerMobileMediumHdImageUrl|URL d'une bannière de résolution moyenne qui s'affiche sur la page de votre chaîne dans les applications mobiles. La taille maximale de l'image est de 960 x 263 pixels. <br> string <br>  <br> brandingSettings.image. <br> Cette propriété est obsolète.|
|bannerMobileHdImageUrl|URL d'une bannière haute résolution qui s'affiche sur la page de votre chaîne dans les applications mobiles. La taille maximale de l'image est de 1 280 x 360 pixels. <br> string <br>  <br> brandingSettings.image. <br> Cette propriété est obsolète.|
|bannerMobileExtraHdImageUrl|URL d'une image de bannière très haute résolution qui s'affiche sur la page de chaîne dans les applications mobiles. La taille maximale de l'image est de 1 440 x 395 pixels. <br> string <br>  <br> brandingSettings.image. <br> Cette propriété est obsolète.|
|bannerTvImageUrl|URL d'une image de bannière haute résolution qui s'affiche sur la page de la chaîne dans les applications TV. La taille maximale de l'image est de 2 120 x 1 192 pixels. <br> string <br>  <br> brandingSettings.image. <br> Cette propriété est obsolète.|
|bannerTvLowImageUrl|URL d'une image de bannière basse résolution qui s'affiche sur la page de chaîne dans les applications TV. La taille maximale de l'image est de 854 x 480 pixels. <br> string <br>  <br> brandingSettings.image. <br> Cette propriété est obsolète.|
|bannerTvMediumImageUrl|URL d'une image de bannière de résolution moyenne qui s'affiche sur la page de chaîne dans les applications TV. La taille maximale de l'image est de 1 280 x 720 pixels. <br> string <br>  <br> brandingSettings.image. <br> Cette propriété est obsolète.|
|bannerTvHighImageUrl|URL d'une bannière haute résolution qui s'affiche sur la page de la chaîne dans les applications TV. La taille maximale de l'image est de 1 920 x 1 080 pixels. <br> string <br>  <br> brandingSettings.image. <br> Cette propriété est obsolète.|
|bannerExternalUrl|Cette propriété spécifie l'emplacement de l'image de bannière utilisée par YouTube pour générer les différentes tailles d'image de bannière d'une chaîne. <br> string <br>  <br> brandingSettings.image. <br> |
|hints[]|L'objet  encapsule des propriétés de branding supplémentaires. <br> list <br> hints <br> brandingSettings. <br> Cette propriété et toutes ses propriétés enfants ont été abandonnées.|
|property|Une propriété. <br> string <br>  <br> brandingSettings.hints[]. <br> Cette propriété est obsolète.|
|value|Valeur de la propriété. <br> string <br>  <br> brandingSettings.hints[]. <br> Cette propriété est obsolète.|
|auditDetails|L'objet  encapsule les données de canal qu'un réseau multichaîne évaluerait, tout en déterminant s'il faut accepter ou refuser un canal particulier. Notez que toute requête API qui récupère cette partie de ressource doit fournir un jeton d'autorisation contenant le champ d'application . De plus, tout jeton utilisant ce champ d'application doit être révoqué lorsque le réseau multichaîne accepte ou refuse la chaîne, ou dans les deux semaines suivant sa date d'émission. <br> object <br> auditDetails <br>  <br> https://www.googleapis.com/auth/youtubepartner-channel-audit|
|overallGoodStanding|Ce champ indique si la chaîne présente des problèmes. Actuellement, ce champ représente le résultat de l'opération logique  sur les propriétés ,  et , ce qui signifie que cette propriété a la valeur  si toutes ces autres propriétés ont également la valeur . Cependant, cette propriété aura la valeur  si l'une de ces propriétés a la valeur . Notez toutefois que la méthodologie utilisée pour définir la valeur de cette propriété est susceptible d'être modifiée. <br> boolean <br> AND <br> auditDetails. <br> communityGuidelinesGoodStanding|
|communityGuidelinesGoodStanding|Indique si la chaîne respecte le règlement de la communauté YouTube. <br> boolean <br>  <br> auditDetails. <br> |
|copyrightStrikesGoodStanding|Indique si la chaîne a reçu des avertissements pour atteinte aux droits d'auteur. <br> boolean <br>  <br> auditDetails. <br> |
|contentIdClaimsGoodStanding|Indique si la chaîne présente des revendications non résolues. <br> boolean <br>  <br> auditDetails. <br> |
|contentOwnerDetails|L'objet  encapsule les données de chaîne qui ne sont visibles que par le partenaire YouTube qui a associé la chaîne à son Gestionnaire de contenu. <br> object <br> contentOwnerDetails <br>  <br> |
|contentOwner|ID du propriétaire de contenu associé à la chaîne. <br> string <br>  <br> contentOwnerDetails. <br> |
|timeLinked|Date et heure de l'association de la chaîne au propriétaire de contenu. La valeur est spécifiée au format . <br> datetime <br>  <br> contentOwnerDetails. <br> |
|localizations|L'objet  encapsule la traduction des métadonnées de la chaîne. <br> object <br> localizations <br>  <br> |
|(key)|Langue des métadonnées localisées associées à la clé-valeur. La valeur est une chaîne contenant un code de langue . <br> object <br>  <br> localizations. <br> |
|title|Titre localisé de la chaîne. <br> string <br>  <br> localizations.(key). <br> |
|description|Description localisée de la chaîne. <br> string <br>  <br> localizations.(key). <br> |