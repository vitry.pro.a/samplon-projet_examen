# Bilan format data

## Search

```json
{
    "kind": "youtube#searchResult",
    "id": {
        "kind": string,
        "videoId": string,
        "channelId": string,
        "playlistId": string
    },
    "snippet": {
        "publishedAt": datetime,
        "channelId": string,
        "title": string,
        "description": string,
        "channelTitle": string
    }
}
```
### Catégories:
kind / id / snippet

### Dimensions:
"kind": "youtube#searchResult"  
"videoId": string  
"channelId": string  
"playlistId": string  
"publishedAt": datetime  
"channelId": string  
"title": string  
"description": string  
"channelTitle": string  


## Channel:

```json
{
    "kind": "youtube#channel",
    "id": string,
    "snippet": {
        "title": string,
        "description": string,
        "publishedAt": datetime,
        "country": string
    },
    "contentDetails": {
        "relatedPlaylists": {
            "likes": string,
            "favorites": string,
            "uploads": string
        }
    },
    "statistics": {
        "viewCount": unsigned long,
        "subscriberCount": unsigned long, // this value is rounded to three significant figures
        "hiddenSubscriberCount": boolean,
        "videoCount": unsigned long
    },
    "topicDetails": {
        "topicIds": [
        string
        ],
        "topicCategories": [
        string
        ]
    },
    "brandingSettings": {
        "channel": {
            "title": string,
            "description": string,
            "unsubscribedTrailer": string,
            "country": string
        }
    },
    "localizations": {
        (key): {
            "title": string,
            "description": string
        }
    }
}
```
### Catégories:
kind / id / snippet / contentDetails / statistics / topicDetails / brandingSettings / localizations

### Sous-catégories:
relatedPlaylists / channel

### Dimensions:
"kind": "youtube#channel"  
"id": string  
"title": string  
"description": string  
"publishedAt": datetime  
"country": string  
"likes": string  
"favorites": string  
"uploads": string  
"viewCount": unsigned long  
"subscriberCount": unsigned long  
"hiddenSubscriberCount": boolean  
"videoCount": unsigned long  
"topicIds": [string]  
"topicCategories": [string]  
"title": string  
"description": string  
"unsubscribedTrailer": string  
"country": string  
"title": string  
"description": string  


## Playlist:

```json
{
    "kind": "youtube#playlist",
    "id": string,
    "snippet": {
        "publishedAt": datetime,
        "channelId": string,
        "title": string,
        "description": string,
        },
    "localizations": {
        (key): {
        "title": string,
        "description": string
        }
    }
}
```

### Catégories:
kind / id / snippet / contentDetails / localizations

### Dimensions:
"kind": "youtube#playlist"  
"id": string  
"publishedAt": datetime  
"channelId": string  
"title": string  
"description": string  
"title": string  
"description": string  


## Vidéo:

```json
{
    "kind": "youtube#video",
    "id": string,
    "snippet": {
        "publishedAt": datetime,
        "channelId": string,
        "title": string,
        "description": string,
        "tags": [
        string
        ],
        "categoryId": string
    },
    "contentDetails": {
        "duration": string,
        "dimension": string,
        "definition": string,
        "caption": string
    },
    "status": {
        "uploadStatus": string,
        "publishAt": datetime,
        "publicStatsViewable": boolean
    },
    "statistics": {
        "viewCount": string,
        "likeCount": string,
        "dislikeCount": string,
        "favoriteCount": string,
        "commentCount": string
    },
    "topicDetails": {
        "topicIds": [
        string
        ],
        "relevantTopicIds": [
        string
        ],
        "topicCategories": [
        string
        ]
    },
    "recordingDetails": {
        "recordingDate": datetime
    },
    "fileDetails": {
        "fileName": string,
        "fileSize": unsigned long,
        "fileType": string,
        "container": string,
        "durationMs": unsigned long,
        "bitrateBps": unsigned long,
        "creationTime": string
    },
    "localizations": {
        (key): {
            "title": string,
            "description": string
        }
    }
}
```

### Catégories:
kind / id / snippet / contentDetails / status / statistics / topicDetails / recordingDetails / fileDetails / localizations

### Dimensions:
"kind": "youtube#video"  
"id": string  
"publishedAt": datetime  
"channelId": string  
"title": string  
"description": string  
"tags": [string]  
"categoryId": string  
"duration": string  
"dimension": string  
"definition": string  
"caption": string  
"uploadStatus": string  
"publishAt": datetime  
"publicStatsViewable": boolean  
"viewCount": string  
"likeCount": string  
"dislikeCount": string  
"favoriteCount": string  
"commentCount": string  
"topicIds": [string]  
"relevantTopicIds": [string]  
"topicCategories": [string]  
"recordingDate": datetime  
"fileName": string  
"fileSize": unsigned long  
"fileType": string  
"container": string  
"durationMs": unsigned long  
"bitrateBps": unsigned long  
"creationTime": string  
"title": string  
"description": string  