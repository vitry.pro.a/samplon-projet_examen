```json
{
    "kind": "youtube#playlistItem",
    "etag": etag,
    "id": string,
    "snippet": {
        "publishedAt": datetime,
        "channelId": string,
        "title": string,
        "description": string,
        "thumbnails": {
        (key): {
            "url": string,
            "width": unsigned integer,
            "height": unsigned integer
        }
        },
        "channelTitle": string,
        "videoOwnerChannelTitle": string,
        "videoOwnerChannelId": string,
        "playlistId": string,
        "position": unsigned integer,
        "resourceId": {
            "kind": string,
            "videoId": string,
        }
    },
    "contentDetails": {
        "videoId": string,
        "startAt": string,
        "endAt": string,
        "note": string,
        "videoPublishedAt": datetime
    },
    "status": {
        "privacyStatus": string
    }
}
```

# JSON Final
```json
{
    "kind": "youtube#playlistItem",
    "etag": etag,
    "id": string,
    "snippet": {
        "publishedAt": datetime,
        "channelId": string,
        "title": string,
        "description": string,
        "playlistId": string,
        "resourceId": {
            "kind": string,
            "videoId": string,
        }
    },
    "contentDetails": {
        "videoId": string,
    },
    "status": {
        "privacyStatus": string
  }
}
```