# Format JSON

```json
{
    "kind": "youtube#playlist",
    "etag": etag,
    "id": string,
    "snippet": {
        "publishedAt": datetime,
        "channelId": string,
        "title": string,
        "description": string,
        "thumbnails": {
            (key): {
                "url": string,
                "width": unsigned integer,
                "height": unsigned integer
                }
            },
        "channelTitle": string,
        "defaultLanguage": string,
        "localized": {
            "title": string,
            "description": string
            }
        },
    "status": {
        "privacyStatus": string
    },
    "contentDetails": {
        "itemCount": unsigned integer
    },
    "player": {
        "embedHtml": string
    },
    "localizations": {
        (key): {
        "title": string,
        "description": string
        }
    }
}
```

# Playlist et statut (à conserver ou jeter ou à vérifier)

    "kind": "youtube#playlist"          ==>     à conserver
    "etag": etag                        ==>     à jeter
    "id": string                        ==>     à conserver
    "snippet": dict                     ==>     à conserver
    "status": dict                      ==>     à jeter
    "contentDetails": dict              ==>     à jeter
    "player": dict                      ==>     à jeter
    "localizations": dict               ==>     à conserver

Final
"kind": "youtube#playlist"          ==>     à conserver
"id": string                        ==>     à conserver
"snippet": dict                     ==>     à conserver
"status": dict                      ==>     à conserver
"contentDetails": dict              ==>     à conserver

> - ## Kind
Information du type de dictionnaire: ici scearch of channel

> - ## id
Information de l'identifiant du résultat

> - ## snippet
Information de base du résultat

    "publishedAt": datetime             ==>     à conserver
    "channelId": string                 ==>     à conserver
    "title": string                     ==>     à conserver
    "description": string               ==>     à conserver
    "thumbnails": dict                  ==>     à jeter
    "channelTitle": string              ==>     à jeter
    "defaultLanguage": string           ==>     à jeter
    "localized": dict                   ==>     à jeter


---


# Récapitulatif de Playlist:

First Header | Second Header | Third Header
------------ | ------------- | ------------
Content Cell | Content Cell  | Content Cell
Content Cell | Content Cell  | Content Cell


```json
{
    "kind": "youtube#playlist",
    "id": string,
    "snippet": {
        "publishedAt": datetime,
        "channelId": string,
        "title": string,
        "description": string,
        },
    "localizations": {
        (key): {
        "title": string,
        "description": string
        }
    }
}
```

---

|Propriétés | |
|-|-|
|tablescraper-selected-row|apitype <br>  <br> tablescraper-selected-row 2 <br>  <br> tablescraper-selected-row 3 <br>  <br> quiet|
|kind|string <br>  <br>  <br>  <br>  <br>  <br> |
|etag|etag <br>  <br> Etag de cette ressource. <br>  <br>  <br>  <br> |
|id|string <br>  <br> ID utilisé par YouTube pour identifier la playlist de façon unique. <br>  <br>  <br>  <br> |
|snippet|object <br>  <br> L'objet  contient des informations de base sur la playlist, comme son titre et sa description. <br>  <br> snippet <br>  <br> |
|publishedAt|datetime <br>  <br> Date et heure de création de la playlist. La valeur est spécifiée au format . <br>  <br>  <br>  <br> snippet.|
|channelId|string <br>  <br> ID utilisé par YouTube pour identifier de façon unique la chaîne ayant publié la playlist. <br>  <br>  <br>  <br> snippet.|
|title|string <br>  <br> Titre de la playlist. <br>  <br>  <br>  <br> snippet.|
|description|string <br>  <br> Description de la playlist <br>  <br>  <br>  <br> snippet.|
|thumbnails|object <br>  <br> Carte des miniatures associées à la playlist. Pour chaque objet de la carte, la clé est le nom de la vignette et la valeur est un objet contenant d'autres informations sur la vignette. <br>  <br>  <br>  <br> snippet.|
|(key)|object <br>  <br> Les clés-valeurs valides sont les suivantes: <br>  <br> default <br>  <br> snippet.thumbnails.|
|url|string <br>  <br> URL de l'image. <br>  <br>  <br>  <br> snippet.thumbnails.(key).|
|width|unsigned integer <br>  <br> Largeur de l'image. <br>  <br>  <br>  <br> snippet.thumbnails.(key).|
|height|unsigned integer <br>  <br> Hauteur de l'image. <br>  <br>  <br>  <br> snippet.thumbnails.(key).|
|channelTitle|string <br>  <br> Titre de la chaîne à laquelle appartient la vidéo. <br>  <br>  <br>  <br> snippet.|
|tags[]|list <br>  <br> Tags de mots clés associés à la playlist. <br>  <br> Cette propriété est obsolète. <br>  <br> snippet.|
|defaultLanguage|string <br>  <br> Langue du texte dans les propriétés  et  de la ressource . <br>  <br> snippet.title <br>  <br> snippet.|
|localized|object <br>  <br> L'objet  contient soit un titre et une description localisés pour la playlist, soit le titre dans la  pour les métadonnées de la playlist.  La propriété contient une valeur en lecture seule. Utilisez l'objet  pour ajouter, mettre à jour ou supprimer des titres localisés. <br>  <br> snippet.localized <br>  <br> snippet.|
|title|string <br>  <br> Titre de la playlist localisée. <br>  <br>  <br>  <br> snippet.localized.|
|description|string <br>  <br> Description de la playlist localisée. <br>  <br>  <br>  <br> snippet.localized.|
|status|object <br>  <br> L'objet  contient des informations sur l'état de la playlist. <br>  <br> status <br>  <br> |
|privacyStatus|string <br>  <br> État de confidentialité de la playlist.Les valeurs valides pour cette propriété sont les suivantes : <br>  <br> private <br>  <br> status.|
|contentDetails|object <br>  <br> L'objet  contient des informations sur le contenu de la playlist, y compris le nombre de vidéos qu'elle contient. <br>  <br> contentDetails <br>  <br> |
|itemCount|unsigned integer <br>  <br> Nombre de vidéos dans la playlist. <br>  <br>  <br>  <br> contentDetails.|
|player|object <br>  <br> L'objet  contient les informations dont vous avez besoin pour lire la playlist dans un lecteur intégré. <br>  <br> player <br>  <br> |
|embedHtml|string <br>  <br> Une balise  intégrant un lecteur qui lit la playlist <br>  <br> <iframe> <br>  <br> player.|
|localizations|object <br>  <br> L'objet  encapsule la traduction des métadonnées de la playlist. <br>  <br> localizations <br>  <br> |
|(key)|object <br>  <br> Langue du texte localisé associé à la clé-valeur. La valeur est une chaîne contenant un code de langue . <br>  <br>  <br>  <br> localizations.|
|title|string <br>  <br> Titre de la playlist localisée. <br>  <br>  <br>  <br> localizations.(key).|
|description|string <br>  <br> Description de la playlist localisée. <br>  <br>  <br>  <br> localizations.(key).|
