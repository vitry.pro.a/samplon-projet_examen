# Format JSON

```json
{
    "kind": "youtube#video",
    "etag": etag,
    "id": string,
    "snippet": {
        "publishedAt": datetime,
        "channelId": string,
        "title": string,
        "description": string,
        "thumbnails": {
        (key): {
                "url": string,
                "width": unsigned integer,
                "height": unsigned integer
            }
        },
        "channelTitle": string,
        "tags": [
        string
        ],
        "categoryId": string,
        "liveBroadcastContent": string,
        "defaultLanguage": string,
        "localized": {
            "title": string,
            "description": string
        },
        "defaultAudioLanguage": string
    },
    "contentDetails": {
        "duration": string,
        "dimension": string,
        "definition": string,
        "caption": string,
        "licensedContent": boolean,
        "regionRestriction": {
            "allowed": [
            string
            ],
            "blocked": [
            string
            ]
        },
        "contentRating": {
            "acbRating": string,
            "agcomRating": string,
            "anatelRating": string,
            "bbfcRating": string,
            "bfvcRating": string,
            "bmukkRating": string,
            "catvRating": string,
            "catvfrRating": string,
            "cbfcRating": string,
            "cccRating": string,
            "cceRating": string,
            "chfilmRating": string,
            "chvrsRating": string,
            "cicfRating": string,
            "cnaRating": string,
            "cncRating": string,
            "csaRating": string,
            "cscfRating": string,
            "czfilmRating": string,
            "djctqRating": string,
            "djctqRatingReasons": [
                ,
            string
            ],
            "ecbmctRating": string,
            "eefilmRating": string,
            "egfilmRating": string,
            "eirinRating": string,
            "fcbmRating": string,
            "fcoRating": string,
            "fmocRating": string,
            "fpbRating": string,
            "fpbRatingReasons": [
                ,
            string
            ],
            "fskRating": string,
            "grfilmRating": string,
            "icaaRating": string,
            "ifcoRating": string,
            "ilfilmRating": string,
            "incaaRating": string,
            "kfcbRating": string,
            "kijkwijzerRating": string,
            "kmrbRating": string,
            "lsfRating": string,
            "mccaaRating": string,
            "mccypRating": string,
            "mcstRating": string,
            "mdaRating": string,
            "medietilsynetRating": string,
            "mekuRating": string,
            "mibacRating": string,
            "mocRating": string,
            "moctwRating": string,
            "mpaaRating": string,
            "mpaatRating": string,
            "mtrcbRating": string,
            "nbcRating": string,
            "nbcplRating": string,
            "nfrcRating": string,
            "nfvcbRating": string,
            "nkclvRating": string,
            "oflcRating": string,
            "pefilmRating": string,
            "rcnofRating": string,
            "resorteviolenciaRating": string,
            "rtcRating": string,
            "rteRating": string,
            "russiaRating": string,
            "skfilmRating": string,
            "smaisRating": string,
            "smsaRating": string,
            "tvpgRating": string,
            "ytRating": string
        },
        "projection": string,
        "hasCustomThumbnail": boolean
    },
    "status": {
        "uploadStatus": string,
        "failureReason": string,
        "rejectionReason": string,
        "privacyStatus": string,
        "publishAt": datetime,
        "license": string,
        "embeddable": boolean,
        "publicStatsViewable": boolean,
        "madeForKids": boolean,
        "selfDeclaredMadeForKids": boolean
    },
    "statistics": {
        "viewCount": string,
        "likeCount": string,
        "dislikeCount": string,
        "favoriteCount": string,
        "commentCount": string
    },
    "player": {
        "embedHtml": string,
        "embedHeight": long,
        "embedWidth": long
    },
    "topicDetails": {
        "topicIds": [
        string
        ],
        "relevantTopicIds": [
        string
        ],
        "topicCategories": [
        string
        ]
    },
    "recordingDetails": {
        "recordingDate": datetime
    },
    "fileDetails": {
        "fileName": string,
        "fileSize": unsigned long,
        "fileType": string,
        "container": string,
        "videoStreams": [
            {
                "widthPixels": unsigned integer,
                "heightPixels": unsigned integer,
                "frameRateFps": double,
                "aspectRatio": double,
                "codec": string,
                "bitrateBps": unsigned long,
                "rotation": string,
                "vendor": string
            }
        ],
        "audioStreams": [
            {
                "channelCount": unsigned integer,
                "codec": string,
                "bitrateBps": unsigned long,
                "vendor": string
            }
        ],
        "durationMs": unsigned long,
        "bitrateBps": unsigned long,
        "creationTime": string
    },
    "processingDetails": {
        "processingStatus": string,
        "processingProgress": {
            "partsTotal": unsigned long,
            "partsProcessed": unsigned long,
            "timeLeftMs": unsigned long
        },
        "processingFailureReason": string,
        "fileDetailsAvailability": string,
        "processingIssuesAvailability": string,
        "tagSuggestionsAvailability": string,
        "editorSuggestionsAvailability": string,
        "thumbnailsAvailability": string
    },
    "suggestions": {
        "processingErrors": [
        string
        ],
        "processingWarnings": [
        string
        ],
        "processingHints": [
        string
        ],
        "tagSuggestions": [
            {
                "tag": string,
                "categoryRestricts": [
            string
                ]
            }
        ],
        "editorSuggestions": [
        string
        ]
    },
    "liveStreamingDetails": {
        "actualStartTime": datetime,
        "actualEndTime": datetime,
        "scheduledStartTime": datetime,
        "scheduledEndTime": datetime,
        "concurrentViewers": unsigned long,
        "activeLiveChatId": string
    },
    "localizations": {
        (key): {
            "title": string,
            "description": string
        }
    }
}
```


# Vidéo et statue (conserver ou jeter ou à vérifier)

    kind": "youtube#video"              ==>     à conserver
    "etag": etag                        ==>     à jeter
    "id": string                        ==>     à conserver
    "snippet": dict                     ==>     à conserver
    "contentDetails": dict              ==>     à conserver
    "status": dict                      ==>     à conserver
    "statistics": dict                  ==>     à conserver
    "player": dict                      ==>     à jeter
    "topicDetails": dict                ==>     à conserver
    "recordingDetails": dict            ==>     à conserver
    "fileDetails": dict                 ==>     à conserver
    "processingDetails": dict           ==>     à jeter
    "suggestions": dict                 ==>     à jeter
    "liveStreamingDetails": dict        ==>     à jeter
    "localizations": dict               ==>     à conserver

Final
"kind": "youtube#video"             ==>     à conserver
"id": string                        ==>     à conserver
"snippet": dict                     ==>     à conserver
"contentDetails": dict              ==>     à conserver
"status": dict                      ==>     à conserver
"statistics": dict                  ==>     à conserver
"topicDetails": dict                ==>     à conserver
"suggestions": dict                 ==>     à conserver
---

> - ## Kind
Information du type de dictionnaire: ici video

> - ## id
Information de l'identifiant de la video

> - ## snippet
Information de base de la video

    -"publishedAt": datetime            ==>     à conserver
    -"channelId": string                ==>     à conserver
    -"title": string                    ==>     à conserver
    -"description": string              ==>     à conserver
    -"thumbnails": dict                 ==>     à jeter
    -"channelTitle": string             ==>     à jeter
    -"tags": [string]                   ==>     à conserver
    -"categoryId": string               ==>     à conserver
    -"liveBroadcastContent": string     ==>     à jeter
    -"defaultLanguage": string          ==>     à jeter
    -"localized": dict                  ==>     à jeter
    -"defaultAudioLanguage": string     ==>     à jeter

> - ## contentDetails
Information contenant les informations lié à la liste de lecture associées

    "duration": string                  ==>     à conserver
    "dimension": string                 ==>     à conserver
    "definition": string                ==>     à conserver
    "caption": string                   ==>     à conserver
    "licensedContent": boolean          ==>     à jeter
    "regionRestriction": dict           ==>     à jeter
    "contentRating": dict               ==>     à jeter
    "projection": string                ==>     à jeter
    "hasCustomThumbnail": boolean       ==>     à jeter

> - ## status
Information 

    "uploadStatus": string              ==>     à jeter
    "failureReason": string             ==>     à jeter
    "rejectionReason": string           ==>     à jeter
    "privacyStatus": string             ==>     à conserver
    "publishAt": datetime               ==>     à conserver
    "license": string                   ==>     à jeter
    "embeddable": boolean               ==>     à jeter
    "publicStatsViewable": boolean      ==>     à conserver
    "madeForKids": boolean              ==>     à jeter
    "selfDeclaredMadeForKids": boolean  ==>     à jeter

> - ## statistics
Information 

    "viewCount": string                 ==>     à conserver
    "likeCount": string                 ==>     à conserver
    "dislikeCount": string              ==>     à conserver
    "favoriteCount": string             ==>     à conserver
    "commentCount": string              ==>     à conserver

> - ## topicDetails
Information 

    "topicIds": [string]                ==>     à conserver
    "relevantTopicIds": [string]        ==>     à conserver
    "topicCategories": [string]         ==>     à conserver

> - ## recordingDetails
    "recordingDate": datetime           ==>     à conserver

> - ## fileDetails
Information 

    "fileName": string                  ==>     à conserver
    "fileSize": unsigned long           ==>     à conserver
    "fileType": string                  ==>     à conserver
    "container": string                 ==>     à conserver
    "videoStreams": list[dict]          ==>     à jeter
    "audioStreams": list[dict]          ==>     à jeter
    "durationMs": unsigned long         ==>     à conserver
    "bitrateBps": unsigned long         ==>     à conserver
    "creationTime": string              ==>     à conserver


> - ## localizations
Information de l'emplacement de la chaine

    - "title": string                   ==>     à conserver
    - "description": string             ==>     à conserver


---


# Récapitulatif de Video:

First Header | Second Header | Third Header
------------ | ------------- | ------------
Content Cell | Content Cell  | Content Cell
Content Cell | Content Cell  | Content Cell


```json
{
    "kind": "youtube#video",
    "id": string,
    "snippet": {
        "publishedAt": datetime,
        "channelId": string,
        "title": string,
        "description": string,
        "tags": [
        string
        ],
        "categoryId": string
    },
    "contentDetails": {
        "duration": string,
        "dimension": string,
        "definition": string,
        "caption": string
    },
    "status": {
        "privacyStatus": string,
        "publishAt": datetime,
        "publicStatsViewable": boolean
    },
    "statistics": {
        "viewCount": string,
        "likeCount": string,
        "dislikeCount": string,
        "favoriteCount": string,
        "commentCount": string
    },
    "topicDetails": {
        "topicIds": [
        string
        ],
        "relevantTopicIds": [
        string
        ],
        "topicCategories": [
        string
        ]
    },
    "recordingDetails": {
        "recordingDate": datetime
    },
    "fileDetails": {
        "fileName": string,
        "fileSize": unsigned long,
        "fileType": string,
        "container": string,
        "durationMs": unsigned long,
        "bitrateBps": unsigned long,
        "creationTime": string
    },
    "localizations": {
        (key): {
            "title": string,
            "description": string
        }
    }
}
```


---

|Propriétés | |
|-|-|
|tablescraper-selected-row|apitype <br>  tablescraper-selected-row 2 <br>  tablescraper-selected-row 3 <br>  quiet <br>  tablescraper-selected-row 4 <br>  tablescraper-selected-row 5tablescraper-selected-row 7 <br>  tablescraper-selected-row 8 <br>  tablescraper-selected-row 9 <br>  tablescraper-selected-row 10 <br>  tablescraper-selected-row 11 <br>  tablescraper-selected-row 12 <br>  tablescraper-selected-row 13 <br>  tablescraper-selected-row 14 <br>  tablescraper-selected-row 15 <br>  tablescraper-selected-row 16 <br>  tablescraper-selected-row 17 <br>   <br>|
|kind|string <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|etag|etag <br>  Etag de cette ressource. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|id|string <br>  ID utilisé par YouTube pour identifier la vidéo de façon unique. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|snippet|object <br>  L'objet  contient des informations de base sur la vidéo, comme son titre, sa description et sa catégorie. <br>  snippet <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|publishedAt|datetime <br>  Date et heure de publication de la vidéo. Notez que l'heure de la mise en ligne peut être différente de celle de la mise en ligne de la vidéo. Par exemple, si une vidéo est mise en ligne en tant que vidéo privée, puis rendue publique ultérieurement, cette propriété indique l'heure à laquelle elle a été rendue publique.Il existe quelques cas particuliers:  La valeur est spécifiée au format . <br>   <br>  snippet. <br>  Si une vidéo est mise en ligne en tant que vidéo privée et que ses métadonnées sont récupérées par le propriétaire de la chaîne, la valeur de la propriété indique la date et l'heure de mise en ligne de la vidéo. <br>  Si une vidéo est mise en ligne en tant que vidéo non répertoriée, la valeur de la propriété indique également la date et l'heure de la mise en ligne. Dans ce cas, toute personne connaissant l'ID vidéo unique de la vidéo peut récupérer ses métadonnées. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|channelId|string <br>  ID utilisé par YouTube pour identifier de façon unique la chaîne sur laquelle la vidéo a été mise en ligne. <br>   <br>  snippet. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|title|string <br>  Titre de la vidéo. La valeur de la propriété ne doit pas dépasser 100 caractères et peut contenir tous les caractères UTF-8 valides, à l'exception de  et . Vous devez définir une valeur pour cette propriété si vous appelez la méthode  et mettez à jour la partie  d'une ressource . <br>  videos.update <br>  snippet. <br>  < <br>  >snippet <br>  video <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|description|string <br>  Description de la vidéo La valeur de la propriété ne doit pas dépasser 5 000 octets et peut contenir tous les caractères UTF-8 valides, à l'exception de  et . <br>   <br>  snippet. <br>  < <br>  > <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|thumbnails|object <br>  Carte des vignettes associées à la vidéo. Pour chaque objet de la carte, la clé correspond au nom de la vignette, tandis que la valeur est un objet contenant d'autres informations sur la vignette. <br>   <br>  snippet. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|(key)|object <br>  Les clés-valeurs valides sont les suivantes: <br>   <br>  snippet.thumbnails. <br>  : vignette par défaut. La miniature par défaut d'une vidéo (ou d'une ressource faisant référence à une vidéo, comme un élément de playlist ou un résultat de recherche) mesure 120 pixels de large sur 90 pixels de haut. Par défaut, la miniature d'une chaîne mesure 88 pixels de large sur 88 pixels de haut. <br>  : version haute résolution de l'image de la miniature. Pour une vidéo (ou une ressource faisant référence à une vidéo), cette image mesure 320 pixels de large sur 180 pixels de haut. Pour une chaîne, cette image mesure 240 pixels de large sur 240 pixels de haut.default <br>  medium <br>  : version haute résolution de l'image de la miniature. Pour une vidéo (ou une ressource faisant référence à une vidéo), cette image mesure 480 pixels de large sur 360 pixels de haut. Pour une chaîne, cette image mesure 800 pixels de large sur 800 pixels de haut. <br>  high <br>  : version encore plus élevée de l'image miniature que celle de la résolution . Cette image est disponible pour certaines vidéos et d'autres ressources qui font référence à des vidéos, comme des éléments de playlist ou des résultats de recherche. Cette image mesure 640 pixels de large sur 480 pixels de haut. <br>  standard <br>  high <br>  : version avec la résolution la plus élevée de l'image de la miniature. Cette taille d'image est disponible pour certaines vidéos et d'autres ressources qui se rapportent à des vidéos, comme des éléments de playlist ou des résultats de recherche. Cette image mesure 1 280 pixels de large sur 720 pixels de haut. <br>  maxres <br>   <br>   <br>   <br>|
|url|string <br>  URL de l'image. <br>   <br>  snippet.thumbnails.(key). <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|width|unsigned integer <br>  Largeur de l'image. <br>   <br>  snippet.thumbnails.(key). <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|height|unsigned integer <br>  Hauteur de l'image. <br>   <br>  snippet.thumbnails.(key). <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|channelTitle|string <br>  Titre de la chaîne à laquelle appartient la vidéo. <br>   <br>  snippet. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|tags[]|list <br>  Liste de tags de mots clés associés à la vidéo. Les balises peuvent contenir des espaces. La valeur de la propriété ne doit pas dépasser 500 caractères. Tenez compte des règles suivantes concernant le calcul de la limite de caractères: <br>  Foo-Baz <br>  snippet. <br>  La valeur de la propriété est une liste, et les virgules entre les éléments de la liste sont comptabilisées dans la limite. <br>  Si un tag contient un espace, le serveur d'API traite la valeur du tag comme si elle était entre guillemets. Ces derniers sont alors comptabilisés dans le nombre maximal de caractères. Ainsi, dans le cadre du nombre maximal de caractères, la balise  contient sept caractères, tandis que la balise  en contient neuf. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|categoryId|string <br>  YouTube associée à la vidéo. Vous devez définir une valeur pour cette propriété si vous appelez la méthode  et mettez à jour la partie  d'une ressource . <br>  videos.update <br>  snippet. <br>   <br>  snippet <br>  video <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|liveBroadcastContent|string <br>  Indique si la vidéo est une diffusion en direct à venir/active. Il prend également la valeur "none " si la vidéo n'est pas une diffusion en direct à venir/active.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  snippet. <br>   <br>  live <br>  none <br>   <br>  upcoming <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|defaultLanguage|string <br>  Langue du texte dans les propriétés  et  de la ressource . <br>  snippet.title <br>  snippet. <br>   <br>   <br>  video <br>   <br>   <br>   <br>   <br>  snippet.description <br>   <br>   <br>   <br>   <br>   <br>|
|localized|object <br>  L'objet  contient soit un titre et une description localisés pour la vidéo, soit le titre dans la  des métadonnées de la vidéo.  La propriété contient une valeur en lecture seule. Utilisez l'objet  pour ajouter, mettre à jour ou supprimer des titres localisés. <br>  snippet.localized <br>  snippet. <br>  Le texte localisé est renvoyé dans l'extrait de ressource si la requête  a utilisé le paramètre  pour spécifier une langue pour laquelle le texte localisé doit être renvoyé  que le texte localisé est disponible dans cette langue. <br>  Les métadonnées pour la langue par défaut sont renvoyées si aucune valeur de paramètre  n'est spécifiée  si une valeur est spécifiée, mais que les métadonnées localisées ne sont pas disponibles pour la langue spécifiée.localizations <br>  hl <br>  videos.list <br>  hl <br>  et <br>  ou <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|title|string <br>  Titre localisé de la vidéo. <br>   <br>  snippet.localized. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|description|string <br>  Description localisée de la vidéo. <br>   <br>  snippet.localized. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|defaultAudioLanguage|string <br>  La propriété  spécifie la langue parlée dans la piste audio par défaut de la vidéo. <br>  default_audio_language <br>  snippet. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|contentDetails|object <br>  L'objet  contient des informations sur le contenu vidéo, y compris sa durée et une indication si des sous-titres sont disponibles pour celle-ci. <br>  contentDetails <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|duration|string <br>  Durée de la vidéo. La valeur de la propriété est une durée . Par exemple, pour une vidéo d'au moins une minute et de moins d'une heure, la durée est au format . Les lettres  indiquent que la valeur spécifie une période, et les lettres  et  désignent la durée en minutes et en secondes, respectivement. Les caractères  précédant les lettres  et  sont des entiers qui indiquent le nombre de minutes (ou de secondes) de la vidéo. Par exemple, la valeur  indique que la vidéo dure 15 minutes et 33 secondes. Si la vidéo dure au moins une heure, la durée est au format . L'élément  qui précède la lettre  indique la durée de la vidéo en heures, et tous les autres détails sont identiques à ceux décrits ci-dessus. Si la vidéo dure au moins une journée, les lettres  et  sont séparées, et le format de la valeur est . Pour plus d'informations, veuillez consulter la spécification ISO 8601. <br>  PT#M#S <br>  contentDetails. <br>  S <br>  #M <br>  M <br>  S <br>  PT15M33S <br>  PT#H#M#S <br>  # <br>  PT <br>  H <br>  P <br>  T <br>  P#DT#H#M#S <br>   <br>|
|dimension|string <br>  Indique si la vidéo est disponible en 3D ou en 2D. <br>   <br>  contentDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|definition|string <br>  Indique si la vidéo est disponible en haute définition () ou uniquement en définition standard.Les valeurs acceptées pour cette propriété sont les suivantes : <br>  HD <br>  contentDetails. <br>   <br>  hd <br>  sd <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|caption|string <br>  Indique si des sous-titres sont disponibles pour la vidéo.Les valeurs valides pour cette propriété sont les suivantes : <br>   <br>  contentDetails. <br>   <br>  false <br>  true <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|licensedContent|boolean <br>  Indique si la vidéo représente du contenu sous licence, c'est-à-dire si le contenu a été mis en ligne sur une chaîne associée à un partenaire de contenu YouTube, puis revendiqué par ce partenaire. <br>   <br>  contentDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|regionRestriction|object <br>  L'objet  contient des informations sur les pays dans lesquels une vidéo est (ou non) visible. L'objet contiendra la propriété  ou . <br>  regionRestriction <br>  contentDetails. <br>   <br>   <br>  contentDetails.regionRestriction.blocked <br>   <br>   <br>   <br>   <br>  contentDetails.regionRestriction.allowed <br>   <br>   <br>   <br>   <br>   <br>|
|allowed[]|list <br>  Liste de codes régionaux qui identifient les pays dans lesquels la vidéo est disponible. Si cette propriété est présente et qu'un pays ne figure pas dans sa valeur, la diffusion de la vidéo est bloquée dans ce pays. Si cette propriété est présente et qu'elle contient une liste vide, la vidéo est bloquée dans tous les pays. <br>   <br>  contentDetails.regionRestriction. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|blocked[]|list <br>  Liste de codes régionaux qui identifient les pays dans lesquels la vidéo est bloquée. Si cette propriété est présente et qu'un pays ne figure pas dans sa valeur, cela signifie que la vidéo est disponible dans ce pays. Si cette propriété est présente et qu'elle contient une liste vide, la vidéo est visible dans tous les pays. <br>   <br>  contentDetails.regionRestriction. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|contentRating|object <br>  Spécifie les notes que la vidéo a reçues selon différents systèmes de classification. <br>   <br>  contentDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|acbRating|string <br>  Classification de la vidéo : l'ACB (Australian Classification Board) ou l'ACMA (Australian Communications and Media Authority) Les classifications ACMA sont utilisées pour classer les programmes télévisés pour enfants.Les valeurs valides pour cette propriété sont les suivantes : <br>  – PG <br>  contentDetails.contentRating. <br>  : programmes classés dans la catégorie  par l'Australian Communications and Media Authority. Ces programmes sont destinés aux moins de 14 ans (sauf tout-petits). <br>  – EacbC <br>  C <br>  – V <br>  acbE <br>  – M <br>  acbG <br>  acbM <br>  – MA15+ <br>  acbMa15plus <br>  : programmes classés dans la catégorie  par l'Australian Communications and Media Authority. Ces programmes sont destinés aux tout-petits. <br>  acbP <br>   <br>|
|agcomRating|string <br>  Classification de la vidéo par l'Autorità per le Garanzie nelle Comunicazioni (AGCOM) en Italie.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – T <br>  agcomT <br>  agcomUnrated <br>  – VM14 <br>  agcomVm14 <br>  – VM18 <br>  agcomVm18 <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|anatelRating|string <br>  Classification Anatel (Asociación Nacional de Televisión) de la vidéo pour la télévision chilienne.Les valeurs acceptées pour cette propriété sont les suivantes : <br>  – D <br>  contentDetails.contentRating. <br>  – A <br>  – VanatelA <br>  anatelF <br>  – I <br>  anatelI <br>  – I-10 <br>  anatelI10 <br>  anatelI12 <br>  – I-12 <br>  anatelI7 <br>  – I-7 <br>  anatelR <br>   <br>|
|bbfcRating|string <br>  Classification du British Board of Film Classification (BBFC) de la vidéo.Les valeurs acceptées pour cette propriété sont les suivantes : <br>  – U <br>  contentDetails.contentRating. <br>  – 12 <br>  – 12:00bbfc12 <br>  bbfc12a <br>  – 15 <br>  bbfc15 <br>  – 18 <br>  bbfc18 <br>  bbfcPg <br>  – PG <br>  bbfcR18 <br>  – R18 <br>  bbfcU <br>   <br>|
|bfvcRating|string <br>  Classification de la vidéo par le Board of Film and Video Censors de Thaïlande.Les valeurs acceptées pour cette propriété sont les suivantes : <br>  – V <br>  contentDetails.contentRating. <br>  – 13 <br>  – 15bfvc13 <br>  bfvc15 <br>  – 18 <br>  bfvc18 <br>  – 20 <br>  bfvc20 <br>  bfvcB <br>  – o <br>  bfvcE <br>  – E <br>  bfvcG <br>   <br>|
|bmukkRating|string <br>  Classification de la vidéo par le Conseil autrichien de classification des médias (Bundesministerium für Unterricht, Kunst und Kultur).Les valeurs acceptées pour cette propriété sont les suivantes : <br>  – Pas de restriction <br>  contentDetails.contentRating. <br>  – 10 et plus <br>  – 12 et plusbmukk10 <br>  bmukk12 <br>  – 14 et plus <br>  bmukk14 <br>  – 16 et plus <br>  bmukk16 <br>  bmukk6 <br>  – Plus de 6 <br>  bmukk8 <br>  – Plus de 8 ans <br>  bmukkAa <br>   <br>|
|catvRating|string <br>  Système de classification de la TV canadienne - Système de classification de la télévision canadienne Classification de la vidéo émise par la Commission canadienne de la radiotélévision et des télécommunications (CRTC) pour les émissions diffusées en langue canadienne anglophone. Pour en savoir plus, consultez le site Web du .Les valeurs valides pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 14 et plus <br>  – 18 ans et pluscatv14plus <br>  catv18plus <br>  – C <br>  catvC <br>  – C8 <br>  catvC8 <br>  catvG <br>  – V <br>  catvPg <br>  – PG <br>  catvUnrated <br>   <br>|
|catvfrRating|string <br>  Classification de la vidéo par la Commission canadienne de la radiotélévision et des télécommunications (CRTC) pour les émissions canadiennes francophones. Pour en savoir plus, consultez le site Web du .Les valeurs valides pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 13 et plus <br>  – 16 et pluscatvfr13plus <br>  catvfr16plus <br>  – 18 ans et plus <br>  catvfr18plus <br>  – Plus de 8 ans <br>  catvfr8plus <br>  catvfrG <br>  – V <br>  catvfrUnrated <br>   <br>   <br>   <br>|
|cbfcRating|string <br>  Classification du Central Board of Film Certification (CBFC – Inde) de la vidéo.Les valeurs acceptées pour cette propriété sont les suivantes : <br>  – U/A <br>  contentDetails.contentRating. <br>  – A <br>  – ScbfcA <br>  cbfcS <br>  – U <br>  cbfcU <br>   <br>  cbfcUnrated <br>  cbfcUA <br>  – U/A <br>  cbfcUA7plus <br>  – U/A <br>  cbfcUA13plus <br>   <br>|
|cccRating|string <br>  Note de la vidéo Consejo de Calificación Cinematográfica (Chili).Les valeurs acceptées pour cet établissement sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 14 et plus <br>  – 18 ans et plusccc14 <br>  ccc18 <br>  – 18 ans et plus (contenido pornográfico) <br>  ccc18s <br>  – 18+ – contenido excesivamente violento <br>  ccc18v <br>  ccc6 <br>  – 6 ans et plus – Dérangement pour une pause de sept ans <br>  cccTe <br>  – Espectador de tâches <br>  cccUnrated <br>   <br>|
|cceRating|string <br>  Classification de la vidéo, émise par la Comissão de Classificação de Espect 'culos au Portugal.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 12 <br>  – 14cceM12 <br>  cceM14 <br>  – 16 <br>  cceM16 <br>  – 18 <br>  cceM18 <br>  cceM4 <br>  - 4 <br>  cceM6 <br>  - 6 <br>  cceUnrated <br>   <br>|
|chfilmRating|string <br>  Classification de la vidéo en Suisse.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 0 <br>  – 12chfilm0 <br>  chfilm12 <br>  – 16 <br>  chfilm16 <br>  – 18 <br>  chfilm18 <br>  chfilm6 <br>  - 6 <br>  chfilmUnrated <br>   <br>   <br>   <br>|
|chvrsRating|string <br>  Classification CHVRS (Canada Home Video Rating System) de la vidéo.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 14:00 <br>  – 18:00chvrs14a <br>  chvrs18a <br>  – E <br>  chvrsE <br>  – V <br>  chvrsG <br>  chvrsPg <br>  – PG <br>  chvrsR <br>  – D <br>  chvrsUnrated <br>   <br>|
|cicfRating|string <br>  Classification de la vidéo par la Commission de Contrôle des Films (Belgique).Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – E <br>  – KNT/ENAcicfE <br>  cicfKntEna <br>  – KT/EA <br>  cicfKtEa <br>   <br>  cicfUnrated <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|cnaRating|string <br>  Classification de la vidéo par le CONSILIUL NATIONAL AL AUDIOVIZUALULUI (CNA) de Roumanie.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 12 <br>  – 15cna12 <br>  cna15 <br>  – 18 <br>  cna18 <br>  – 18 ans et plus <br>  cna18plus <br>  cnaAp <br>  – AP <br>  cnaUnrated <br>   <br>   <br>   <br>|
|cncRating|string <br>  Système de classification en France - Commission de classification cinématographiqueLes valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 10 <br>  – 12cnc10 <br>  cnc12 <br>  – 16 <br>  cnc16 <br>  – 18 <br>  cnc18 <br>  cncE <br>  – E <br>  cncT <br>  – T <br>  cncUnrated <br>   <br>|
|csaRating|string <br>  Classification de la vidéo fournie par le Conseil supérieur de l'audiovisuel français, qui évalue le contenu diffusé.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 10 <br>  – 12csa10 <br>  csa12 <br>  – 16 <br>  csa16 <br>  – 18 <br>  csa18 <br>  csaInterdiction <br>  – Interdiction <br>  csaT <br>  – T <br>  csaUnrated <br>   <br>|
|cscfRating|string <br>  Classification de la vidéo, émise par la Commission de surveillance de la classification des films (CSCF) du Luxembourg.Les valeurs acceptées pour cette propriété sont les suivantes : <br>  – Alabama <br>  contentDetails.contentRating. <br>  – 12 <br>  – 16cscf12 <br>  cscf16 <br>  – 18 <br>  cscf18 <br>  - 6 <br>  cscf6 <br>  cscf9 <br>  - 9 <br>  cscfA <br>  – A <br>  cscfAl <br>   <br>|
|czfilmRating|string <br>  Classification de la vidéo en République tchèque.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 12 <br>  – 14czfilm12 <br>  czfilm14 <br>  – 18 <br>  czfilm18 <br>  – U <br>  czfilmU <br>  czfilmUnrated <br>   <br>   <br>   <br>   <br>   <br>|
|djctqRating|string <br>  Note de la vidéo "Departamento de Justiça, Classificação, Qualificação e Títulos (DJCQT – Brésil).Les valeurs acceptées pour cet établissement sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 10 <br>  – 12djctq10 <br>  djctq12 <br>  – 14 <br>  djctq14 <br>  – 16 <br>  djctq16 <br>  djctq18 <br>  – 18 <br>  djctqL <br>  – G <br>  djctqUnrated <br>   <br>|
|djctqRatingReasons[]|list <br>  Raisons pour lesquelles la vidéo a été classée par le DJCQT (Brésil). <br>   <br>  contentDetails.contentRating. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|ecbmctRating|string <br>  Système de classification en Turquie - Comité d'évaluation et de classification du ministère de la Culture et du TourismeLes valeurs acceptées pour cet établissement sont les suivantes : <br>  – 7 et plus <br>  contentDetails.contentRating. <br>  – 13:00 <br>  – 13 et plusecbmct13a <br>  ecbmct13plus <br>  – 15 heures <br>  ecbmct15a <br>  – 15 et plus <br>  ecbmct15plus <br>  ecbmct18plus <br>  – 18 ans et plus <br>  ecbmct7a <br>  – 7:00 <br>  ecbmct7plus <br>   <br>|
|eefilmRating|string <br>  Note de la vidéo en Estonie.Les valeurs valides pour cette propriété sont les suivantes : <br>  – MS-6 <br>  contentDetails.contentRating. <br>  – Enseignement primaire et secondaire <br>  – Enseignement primaire et secondaireeefilmK12 <br>  eefilmK14 <br>  – Enseignement primaire et secondaire <br>  eefilmK16 <br>  – Enseignement primaire et secondaire <br>  eefilmK6 <br>  eefilmL <br>  – G <br>  eefilmMs12 <br>  – MS-12 <br>  eefilmMs6 <br>   <br>|
|egfilmRating|string <br>  Classification de la vidéo en Égypte.Les valeurs valides pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 18 <br>  – BNegfilm18 <br>  egfilmBn <br>  – GN <br>  egfilmGn <br>   <br>  egfilmUnrated <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|eirinRating|string <br>  Note Eirin (映倫) de la vidéo Eirin est le système de classification japonais.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – V <br>  – PG-12eirinG <br>  eirinPg12 <br>  – Interdit aux moins de 15 ans <br>  eirinR15plus <br>  – Interdit aux moins de 18 ans <br>  eirinR18plus <br>  eirinUnrated <br>   <br>   <br>   <br>   <br>   <br>|
|fcbmRating|string <br>  Classification de la vidéo par le Conseil malaisien de censure des films.Les valeurs acceptées pour cette propriété sont les suivantes : <br>  – 18SX <br>  contentDetails.contentRating. <br>  – 13 <br>  – 16fcbm13 <br>  fcbm16 <br>  – 18 <br>  fcbm18 <br>  – 18PA <br>  fcbm18pa <br>  fcbm18pl <br>  – 18PL <br>  fcbm18sg <br>  – 18SG <br>  fcbm18sx <br>   <br>|
|fcoRating|string <br>  Classification de la vidéo par l'Office for Film, Newspaper and Article Administration de Hong Kong.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – I <br>  – IIfcoI <br>  fcoIi <br>  – IIA <br>  fcoIia <br>  – IIB <br>  fcoIib <br>  fcoIii <br>  – III <br>  fcoUnrated <br>   <br>   <br>   <br>|
|fmocRating|string <br>  Classification de la vidéo (Centre national du cinéma et de l'image animée) du ministère de la Culture français.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 10 <br>  – 12fmoc10 <br>  fmoc12 <br>  – 16 <br>  fmoc16 <br>  – 18 <br>  fmoc18 <br>  fmocE <br>  – E <br>  fmocU <br>  – U <br>  fmocUnrated <br>   <br>|
|fpbRating|string <br>  Classification de la vidéo par le Film and Publication Board d'Afrique du Sud.Les valeurs acceptées pour cette propriété sont les suivantes : <br>  – A <br>  contentDetails.contentRating. <br>  – 10 <br>  – 10-12PGfpb10 <br>  fpb1012Pg <br>  – 13 <br>  fpb13 <br>  – 16 <br>  fpb16 <br>  fpb18 <br>  – 18 <br>  fpb79Pg <br>  – 7-9PG <br>  fpbA <br>   <br>|
|fpbRatingReasons[]|list <br>  Raisons pour lesquelles la vidéo a été classée FPB (Afrique du Sud). <br>   <br>  contentDetails.contentRating. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|fskRating|string <br>  Note "Freiwillige Selbstkontrolle der Filmwirtschaft (FSK – Allemagne)" de la vidéo.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – FSK 0 <br>  – FSK 12fsk0 <br>  fsk12 <br>  – FSK 16 <br>  fsk16 <br>  – FSK 18 <br>  fsk18 <br>  fsk6 <br>  – FSK 6 <br>  fskUnrated <br>   <br>   <br>   <br>|
|grfilmRating|string <br>  Classification de la vidéo en Grèce.Les valeurs acceptées pour cette propriété sont les suivantes : <br>  – Enseignement primaire et secondaire <br>  contentDetails.contentRating. <br>  – E <br>  – KgrfilmE <br>  grfilmK <br>  – Enseignement primaire et secondaire <br>  grfilmK12 <br>  – Enseignement primaire et secondaire <br>  grfilmK13 <br>  grfilmK15 <br>  – Enseignement primaire et secondaire <br>  grfilmK17 <br>  – Enseignement primaire et secondaire <br>  grfilmK18 <br>   <br>|
|icaaRating|string <br>  Classification ICAA – Espagne (Instituto de la Cinematografía y de las Artes Audiovisuales) de la vidéo.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 12 <br>  – 13icaa12 <br>  icaa13 <br>  – 16 <br>  icaa16 <br>  – 18 <br>  icaa18 <br>  icaa7 <br>  – 7 <br>  icaaApta <br>  – APTA <br>  icaaUnrated <br>   <br>|
|ifcoRating|string <br>  Classification irlandaise (IFCO) de la vidéo Pour en savoir plus, consultez le site Web .Les valeurs valides pour cette propriété sont les suivantes : <br>  – V <br>  contentDetails.contentRating. <br>  – 12 <br>  – 12:00ifco12 <br>  ifco12a <br>  – 15 <br>  ifco15 <br>  – 15 heures <br>  ifco15a <br>  ifco16 <br>  – 16 <br>  ifco18 <br>  – 18 <br>  ifcoG <br>   <br>|
|ilfilmRating|string <br>  Classification de la vidéo en Israël.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 12 <br>  – 16ilfilm12 <br>  ilfilm16 <br>  – 18 <br>  ilfilm18 <br>  – AA <br>  ilfilmAa <br>  ilfilmUnrated <br>   <br>   <br>   <br>   <br>   <br>|
|incaaRating|string <br>  Note INCAA (Instituto Nacional de Cine y Artes Audiovisuales – Argentine) associée à la vidéo.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – ATP (Apta para todo publico) <br>  – X (Solo apta para mayores de 18 años, de exhibición condicionada)incaaAtp <br>  incaaC <br>  – 13 (Solo apta para mayores de 13 años) <br>  incaaSam13 <br>  – 16 (Solo apta para mayores de 16 ans) <br>  incaaSam16 <br>  incaaSam18 <br>  – 18 (Solo apta para mayores de 18 ans) <br>  incaaUnrated <br>   <br>   <br>   <br>|
|kfcbRating|string <br>  Classification de la vidéo par le Kenya Film Classification Board.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 16 <br>  – GEkfcb16plus <br>  kfcbG <br>  – PG <br>  kfcbPg <br>  – 18 <br>  kfcbR <br>  kfcbUnrated <br>   <br>   <br>   <br>   <br>   <br>|
|kijkwijzerRating|string <br>  voor de Classificatie van Audiovisuele Media (Pays-Bas).Les valeurs acceptées pour cette propriété sont les suivantes : <br>  – Alabama <br>  contentDetails.contentRating. <br>  – 12 <br>  – 14kijkwijzer12 <br>  kijkwijzer14 <br>  – 16 <br>  kijkwijzer16 <br>  – 18 <br>  kijkwijzer18 <br>  kijkwijzer6 <br>  - 6 <br>  kijkwijzer9 <br>  - 9 <br>  kijkwijzerAl <br>   <br>|
|kmrbRating|string <br>  La note du Korea Media Rating Board (영상물등미플원회) de la vidéo. Le KMRB évalue les vidéos en Corée du Sud.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 12세 이상 관람사 <br>  – 15세 이상 관람kmrb12plus <br>  kmrb15plus <br>  - 에체관람型 <br>  kmrbAll <br>  – 청소년 관람불가 <br>  kmrbR <br>  kmrbTeenr <br>   <br>  kmrbUnrated <br>   <br>   <br>   <br>|
|lsfRating|string <br>  Note de la vidéo tirée du film indonesia Lembaga Sensor.Les valeurs acceptées pour cette propriété sont les suivantes : <br>  – D <br>  contentDetails.contentRating. <br>  – 13 <br>  – 17lsf13 <br>  lsf17 <br>  – 21 <br>  lsf21 <br>  – A <br>  lsfA <br>  lsfBo <br>  – BO <br>  lsfD <br>  – D <br>  lsfR <br>   <br>|
|mccaaRating|string <br>  Classification de la vidéo par le Film Age-Classification Board de Malte.Les valeurs acceptées pour cette propriété sont les suivantes : <br>  – PG <br>  contentDetails.contentRating. <br>  – 12 <br>  – 12:00mccaa12 <br>  mccaa12a <br>  – 14 : cette classification a été supprimée de la nouvelle structure de classification introduite en 2013. <br>  mccaa14 <br>  – 15 <br>  mccaa15 <br>  mccaa16 <br>  – 16 : cette classification a été supprimée de la nouvelle structure de classification introduite en 2013. <br>  mccaa18 <br>  – 18 <br>  mccaaPg <br>   <br>|
|mccypRating|string <br>  Classification de la vidéo par le Media Council for Children and Young People de l'Institut danois du film (Det Danske Filminstitut).Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 11 <br>  – 15mccyp11 <br>  mccyp15 <br>  – 7 <br>  mccyp7 <br>  – A <br>  mccypA <br>  mccypUnrated <br>   <br>   <br>   <br>   <br>   <br>|
|mcstRating|string <br>  Système de classification de la vidéo pour le Vietnam – MCSTLes valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 0 <br>  – 16 et plusmcst0 <br>  mcst16plus <br>  – C13 <br>  mcstC13 <br>  – C16 <br>  mcstC16 <br>  mcstC18 <br>  – C18 <br>  mcstP <br>  – P <br>  mcstUnrated <br>   <br>|
|mdaRating|string <br>  Classification de la vidéo établie par la Media Development Authority (MDA) de Singapour et plus précisément par le Board of Film Censors (BFC).Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – V <br>  – M18mdaG <br>  mdaM18 <br>  – NC16 <br>  mdaNc16 <br>  – PG <br>  mdaPg <br>  mdaPg13 <br>  – PG13 <br>  mdaR21 <br>  – R21 <br>  mdaUnrated <br>   <br>|
|medietilsynetRating|string <br>  Note de la vidéo fournie par Medietilsynet, la Norwegian Media Authority.Les valeurs valides pour cette propriété sont les suivantes : <br>  - 9 <br>  contentDetails.contentRating. <br>  – 11 <br>  – 12medietilsynet11 <br>  medietilsynet12 <br>  – 15 <br>  medietilsynet15 <br>  – 18 <br>  medietilsynet18 <br>  medietilsynet6 <br>  - 6 <br>  medietilsynet7 <br>  – 7 <br>  medietilsynet9 <br>   <br>|
|mekuRating|string <br>  Classification de la vidéo par l'organisme finlandais Kansallinen Audiovisuaalinen Instituutti (Institut national de l'audiovisuel).Les valeurs valides pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 12 <br>  – 16meku12 <br>  meku16 <br>  – 18 <br>  meku18 <br>  – 7 <br>  meku7 <br>  mekuS <br>  – S <br>  mekuUnrated <br>   <br>   <br>   <br>|
|mibacRating|string <br>  Classification de la vidéo par le Ministero dei Beni e delle Attività Culturali e del Turismo (Italie).Les valeurs acceptées pour cet établissement sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>   <br>  mibacT <br>  mibacUnrated <br>   <br>  mibacVap <br>   <br>  mibacVm6 <br>  mibacVm12 <br>   <br>  mibacVm14 <br>   <br>  mibacVm18 <br>   <br>|
|mocRating|string <br>  Classification de la vidéo par Ministerio de Cultura (Colombie).Les valeurs acceptées pour cette propriété sont les suivantes : <br>  – T <br>  contentDetails.contentRating. <br>  – 12 <br>  – 15moc12 <br>  moc15 <br>  – 18 <br>  moc18 <br>  – 7 <br>  moc7 <br>  mocBanned <br>  – Exclu <br>  mocE <br>  – E <br>  mocT <br>   <br>|
|moctwRating|string <br>  Note de la vidéo fournie par le ministère taïwanais de la Culture (文化部).Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – V <br>  – PmoctwG <br>  moctwP <br>  – PG <br>  moctwPg <br>  – D <br>  moctwR <br>  moctwR12 <br>  – Interdit aux moins de 12 ans <br>  moctwR15 <br>  – R-15 <br>  moctwUnrated <br>   <br>|
|mpaaRating|string <br>  Classification MPAA (Motion Picture Association of America) de la vidéo.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – V <br>  – NC-17mpaaG <br>  mpaaNc17 <br>  – PG <br>  mpaaPg <br>  – PG-13 <br>  mpaaPg13 <br>  mpaaR <br>  – D <br>  mpaaUnrated <br>   <br>   <br>   <br>|
|mpaatRating|string <br>  Classification de la Motion Picture Association of America pour les bandes-annonces et les aperçus de films.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – Go (bande verte – Approuvé pour tous les publics) <br>  – RB (bande rouge – recommandé pour les utilisateurs âgés de 17 ans et plus)mpaatGb <br>  mpaatRb <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|mtrcbRating|string <br>  Classification de la vidéo par le Movie and Television Review and Classification Board (Philippines).Les valeurs acceptées pour cette propriété sont les suivantes : <br>  – X <br>  contentDetails.contentRating. <br>  – V <br>  – PGmtrcbG <br>  mtrcbPg <br>  – Interdit aux moins de 13 ans <br>  mtrcbR13 <br>  – Interdit aux moins de 16 ans <br>  mtrcbR16 <br>  mtrcbR18 <br>  – R-18 <br>  mtrcbUnrated <br>   <br>  mtrcbX <br>   <br>|
|nbcRating|string <br>  Classification de la vidéo par le Bureau national de classification des Maldives.Les valeurs acceptées pour cette propriété sont les suivantes : <br>  – PU <br>  contentDetails.contentRating. <br>  – 12 et plus <br>  – 15 et plusnbc12plus <br>  nbc15plus <br>  – 18 ans et plus <br>  nbc18plus <br>  – 18 ans et plus <br>  nbc18plusr <br>  nbcG <br>  – V <br>  nbcPg <br>  – PG <br>  nbcPu <br>   <br>|
|nfrcRating|string <br>  Classification de la vidéo provenant du .Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – A <br>  – onfrcA <br>  nfrcB <br>  – C <br>  nfrcC <br>  – D <br>  nfrcD <br>  nfrcUnrated <br>   <br>  nfrcX <br>  – X <br>   <br>   <br>|
|nfvcbRating|string <br>  Classification de la vidéo par le National Film and Video Censors Board du Nigeria.Les valeurs acceptées pour cette propriété sont les suivantes : <br>  – RE <br>  contentDetails.contentRating. <br>  – 12 <br>  – 12:00nfvcb12 <br>  nfvcb12a <br>  – 15 <br>  nfvcb15 <br>  – 18 <br>  nfvcb18 <br>  nfvcbG <br>  – V <br>  nfvcbPg <br>  – PG <br>  nfvcbRe <br>   <br>|
|nkclvRating|string <br>  Classification de la vidéo par le Nacionãlais Kino centrs (Centre national du film de Lettonie).Les valeurs acceptées pour cet établissement sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 12 et plus <br>  – 18 ans et plusnkclv12plus <br>  nkclv18plus <br>  – 7 et plus <br>  nkclv7plus <br>  – U <br>  nkclvU <br>  nkclvUnrated <br>   <br>   <br>   <br>   <br>   <br>|
|oflcRating|string <br>  Classification Office of Film and Literature Classification (OFLC, Nouvelle-Zélande) de la vidéo.Les valeurs acceptées pour cette propriété sont les suivantes : <br>  – R18 <br>  contentDetails.contentRating. <br>  – V <br>  – MoflcG <br>  oflcM <br>  – PG <br>  oflcPg <br>  – R13 <br>  oflcR13 <br>  oflcR15 <br>  – R15 <br>  oflcR16 <br>  – R16 <br>  oflcR18 <br>   <br>|
|pefilmRating|string <br>  Classification de la vidéo au Pérou.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 14 <br>  – 18pefilm14 <br>  pefilm18 <br>  – PG <br>  pefilmPg <br>  – PT <br>  pefilmPt <br>  pefilmUnrated <br>   <br>   <br>   <br>   <br>   <br>|
|resorteviolenciaRating|string <br>  Classification de la vidéo au Venezuela.Les valeurs valides pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – A <br>  – oresorteviolenciaA <br>  resorteviolenciaB <br>  – C <br>  resorteviolenciaC <br>  – D <br>  resorteviolenciaD <br>  resorteviolenciaE <br>  – E <br>  resorteviolenciaUnrated <br>   <br>   <br>   <br>|
|rtcRating|string <br>  Classification de la vidéo (General Directorate of Radio, Television and Cinematography (Mexique)).Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – A <br>  – AArtcA <br>  rtcAa <br>  – o <br>  rtcB <br>  – B15 <br>  rtcB15 <br>  rtcC <br>  – C <br>  rtcD <br>  – D <br>  rtcUnrated <br>   <br>|
|rteRating|string <br>  Note de l'éditeur irlandais Raidió Teilifís Éireann.Les valeurs acceptées pour cet établissement sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – CH <br>  – GArteCh <br>  rteGa <br>  – MA <br>  rteMa <br>  – PS <br>  rtePs <br>  rteUnrated <br>   <br>   <br>   <br>   <br>   <br>|
|russiaRating|string <br>  Classification MKRF (Russie) de la vidéo au Registre national des films de la Fédération de Russie.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 0 ou plus <br>  – 12 et plusrussia0 <br>  russia12 <br>  – 16 et plus <br>  russia16 <br>  – 18 ans et plus <br>  russia18 <br>  russia6 <br>  – Plus de 6 <br>  russiaUnrated <br>   <br>   <br>   <br>|
|skfilmRating|string <br>  Classification de la vidéo en Slovaquie.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – V <br>  – P2skfilmG <br>  skfilmP2 <br>  – P5 <br>  skfilmP5 <br>  – P8 <br>  skfilmP8 <br>  skfilmUnrated <br>   <br>   <br>   <br>   <br>   <br>|
|smaisRating|string <br>  Classification de la vidéo en Islande.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 12 <br>  – 14smais12 <br>  smais14 <br>  – 16 <br>  smais16 <br>  – 18 <br>  smais18 <br>  smais7 <br>  – 7 <br>  smaisL <br>  – G <br>  smaisUnrated <br>   <br>|
|smsaRating|string <br>  Classification de la vidéo par Statens medieråd (Conseil national des médias de Suède).Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>  – 11 <br>  – 15smsa11 <br>  smsa15 <br>  – 7 <br>  smsa7 <br>  – Tous les âges <br>  smsaA <br>  smsaUnrated <br>   <br>   <br>   <br>   <br>   <br>|
|tvpgRating|string <br>  Classification de la vidéo selon le règlement parental (TVPG).Les valeurs acceptées pour cette propriété sont les suivantes : <br>  – TV-Y7-FV <br>  contentDetails.contentRating. <br>  – TV-G <br>  – TV-MAtvpgG <br>  tvpgMa <br>  – TV-PG <br>  tvpgPg <br>   <br>  tvpgUnrated <br>  tvpgY <br>  – TV-Y <br>  tvpgY7 <br>  – TV-Y7 <br>  tvpgY7Fv <br>   <br>|
|ytRating|string <br>  Classification utilisée par YouTube pour identifier les contenus soumis à une limite d'âge.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  contentDetails.contentRating. <br>   <br>  ytAgeRestricted <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|projection|string <br>  Spécifie le format de projection de la vidéo.Les valeurs valides pour cette propriété sont les suivantes : <br>   <br>  contentDetails. <br>   <br>  360 <br>  rectangular <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|hasCustomThumbnail|boolean <br>  Indique si l'utilisateur ayant mis en ligne la vidéo a fourni une miniature personnalisée pour la vidéo. Cette propriété n'est visible que par l'utilisateur qui a mis en ligne la vidéo. <br>   <br>  contentDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|status|object <br>  L'objet  contient des informations sur l'état d'importation, de traitement et de confidentialité de la vidéo. <br>  status <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|uploadStatus|string <br>  État de la vidéo mise en ligne.Les valeurs valides pour cette propriété sont les suivantes : <br>   <br>  status. <br>   <br>  deleted <br>  failed <br>   <br>  processed <br>   <br>  rejected <br>  uploaded <br>   <br>   <br>   <br>   <br>   <br>|
|failureReason|string <br>  Cette valeur indique pourquoi une vidéo n'a pas pu être mise en ligne. Cette propriété n'est présente que si la propriété  indique que l'importation a échoué.Les valeurs valides pour cette propriété sont les suivantes : <br>  uploadStatus <br>  status. <br>   <br>  codec <br>  conversion <br>   <br>  emptyFile <br>   <br>  invalidFile <br>  tooSmall <br>   <br>  uploadAborted <br>   <br>   <br>   <br>|
|rejectionReason|string <br>  Cette valeur explique pourquoi YouTube a refusé une vidéo mise en ligne. Cette propriété n'est présente que si la propriété  indique que l'importation a été refusée.Les valeurs valides pour cette propriété sont les suivantes : <br>  uploadStatus <br>  status. <br>   <br>  claim <br>  copyright <br>   <br>  duplicate <br>   <br>  inappropriate <br>  legal <br>   <br>  length <br>   <br>  termsOfUse <br>   <br>|
|privacyStatus|string <br>  État de confidentialité de la vidéo.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  status. <br>   <br>  private <br>  public <br>   <br>  unlisted <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|publishAt|datetime <br>  Date et heure de publication prévue de la vidéo. Il ne peut être défini que si l'état de confidentialité de la vidéo est défini sur "Privée". La valeur est spécifiée au format . Notez les deux autres points suivants concernant le comportement de cette propriété: <br>  privacyStatus <br>  status. <br>  Si vous définissez la valeur de cette propriété lorsque vous appelez la méthode , vous devez également définir la valeur de la propriété  sur , même si la vidéo est déjà privée. <br>  Si votre demande programme la publication d'une vidéo par le passé, elle sera publiée immédiatement. Par conséquent, le fait de définir la propriété  sur une date et une heure passées est le même que de faire passer l' de la vidéo de  à .status.privacyStatus <br>   <br>  videos.update <br>  private <br>   <br>  status.publishAt <br>   <br>   <br>  private <br>   <br>  public <br>   <br>|
|license|string <br>  Licence de la vidéo.Les valeurs valides pour cette propriété sont les suivantes : <br>   <br>  status. <br>   <br>  creativeCommon <br>  youtube <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|embeddable|boolean <br>  Cette valeur indique si la vidéo peut être intégrée sur un autre site Web. <br>   <br>  status. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|publicStatsViewable|boolean <br>  Cette valeur indique si les statistiques étendues de la vidéo sur la page de lecture de la vidéo sont visibles publiquement. Par défaut, ces statistiques sont visibles. Des statistiques telles que le nombre de vues et les notes d'une vidéo restent visibles publiquement, même si la valeur de cette propriété est définie sur . <br>  false <br>  status. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|madeForKids|boolean <br>  Cette valeur indique si la vidéo est désignée comme étant destinée aux enfants et indique si cette vidéo est actuellement "conçue pour les enfants". Par exemple, l'état peut être déterminé en fonction de la valeur de la propriété . Consultez le  pour savoir comment définir l'audience de votre chaîne, de vos vidéos ou de vos diffusions. <br>  selfDeclaredMadeForKids <br>  status. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|selfDeclaredMadeForKids|boolean <br>  Dans une demande  ou , cette propriété permet au propriétaire de la chaîne d'indiquer que la vidéo est destinée aux enfants. Dans une requête , la valeur de propriété n'est renvoyée que si le propriétaire de la chaîne a autorisé la requête API. <br>  videos.list <br>  status. <br>  videos.update <br>  videos.insert <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|statistics|object <br>  L'objet  contient des statistiques sur la vidéo. <br>  statistics <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|viewCount|unsigned long <br>  Nombre de fois où la vidéo a été regardée. <br>   <br>  statistics. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|likeCount|unsigned long <br>  Nombre d'utilisateurs ayant indiqué avoir aimé la vidéo. <br>   <br>  statistics. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|dislikeCount|unsigned long <br>  Nombre d'utilisateurs ayant indiqué qu'ils n'ont pas aimé la vidéo. <br>   <br>  statistics. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|favoriteCount|unsigned long <br>  :Cette propriété est obsolète. Cet abandon prendra effet le 28 août 2015. La valeur de la propriété est désormais toujours définie sur . <br>  0 <br>  statistics. <br>  Remarque <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|commentCount|unsigned long <br>  Nombre de commentaires de la vidéo. <br>   <br>  statistics. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|player|object <br>  L'objet  contient des informations que vous pouvez utiliser pour lire la vidéo dans un lecteur intégré. <br>  player <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|embedHtml|string <br>  Balise  intégrant un lecteur qui lit la vidéo. <br>  <iframe> <br>  player. <br>  Si la requête API de récupération de la ressource spécifie une valeur pour les paramètres  et/ou , la taille du lecteur intégré est ajustée pour répondre aux exigences  et/ou . <br>  Si le format de la vidéo n'est pas connu, le format 4:3 est défini par défaut sur le lecteur intégré.maxWidth <br>   <br>  maxHeight <br>  maxHeight <br>   <br>  maxWidth <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|embedHeight|long <br>  Hauteur du lecteur intégré renvoyée dans la propriété . Cette propriété n'est renvoyée que si la requête spécifie une valeur pour les paramètres  et/ou , et que le format de la vidéo est connu. <br>  player.embedHtml <br>  player. <br>  maxWidth <br>  maxHeight <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|embedWidth|long <br>  Largeur du lecteur intégré renvoyée dans la propriété . Cette propriété n'est renvoyée que si la requête spécifie une valeur pour les paramètres  et/ou , et que le format de la vidéo est connu. <br>  player.embedHtml <br>  player. <br>  maxWidth <br>  maxHeight <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|topicDetails|object <br>  L'objet  encapsule les informations sur les thèmes associés à la vidéo. :Consultez les définitions des propriétés  et  ainsi que l' pour en savoir plus sur les modifications à venir liées aux identifiants des thèmes. <br>  topicDetails <br>   <br>  Important <br>   <br>  topicDetails.topicIds[] <br>   <br>   <br>   <br>   <br>  topicDetails.relevantTopicIds[] <br>   <br>   <br>   <br>   <br>   <br>|
|topicIds[]|list <br>   <br>   <br>  topicDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|relevantTopicIds[]|list <br>  Liste des ID de thèmes pertinents pour la vidéo.:En raison de l'abandon de Freebase et de l'API Freebase, les ID de thèmes fonctionnent différemment depuis le 27 février 2017. À ce moment-là, YouTube a commencé à renvoyer un petit ensemble d'identifiants de thèmes sélectionnés. <br>   <br>  topicDetails. <br>  Important <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|topicCategories[]|list <br>  Liste d'URL de Wikipédia fournissant une description détaillée du contenu de la vidéo. <br>   <br>  topicDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|recordingDetails|object <br>  L'objet  encapsule les informations sur le lieu, la date et l'adresse où la vidéo a été enregistrée. <br>  recordingDetails <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|locationDescription|string <br>  Description du lieu où la vidéo a été enregistrée. <br>   <br>  recordingDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|location|object <br>  Informations de géolocalisation associées à la vidéo. Notez que les valeurs de la propriété enfant indiquent le lieu que le propriétaire de la vidéo souhaite associer à celle-ci. Cette valeur peut être modifiée, incluse dans l'index de recherche pour les vidéos publiques, et peut être visible par les utilisateurs dans le cas de vidéos publiques. <br>   <br>  recordingDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|latitude|double <br>  Latitude en degrés. <br>   <br>  recordingDetails.location. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|longitude|double <br>  Longitude en degrés. <br>   <br>  recordingDetails.location. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|altitude|double <br>  Altitude au-dessus de l'ellipsoïde de référence, en mètres. <br>   <br>  recordingDetails.location. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|recordingDate|datetime <br>  Date et heure d'enregistrement de la vidéo. La valeur est spécifiée au format  (). <br>  YYYY-MM-DDThh:mm:ss.sssZ <br>  recordingDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|fileDetails|object <br>  L'objet  encapsule les informations sur le fichier vidéo importé sur YouTube, y compris la résolution du fichier, sa durée, ses codecs audio et vidéo, les débits de flux, etc. Seul le propriétaire de la vidéo peut récupérer ces données. L'objet  n'est renvoyé que si la propriété  a la valeur . <br>  fileDetails <br>   <br>  available <br>  processingDetails.fileAvailability <br>   <br>   <br>   <br>   <br>   <br>  fileDetails <br>   <br>   <br>   <br>   <br>   <br>|
|fileName|string <br>  Nom du fichier importé. Ce champ est disponible, qu'un fichier vidéo ou un autre type de fichier ait été importé. <br>   <br>  fileDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|fileSize|unsigned long <br>  Taille du fichier importé, en octets. Ce champ est disponible, qu'un fichier vidéo ou un autre type de fichier ait été importé. <br>   <br>  fileDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|fileType|string <br>  Type du fichier importé tel qu'il est détecté par le moteur de traitement vidéo de YouTube. Actuellement, YouTube ne traite que les fichiers vidéo, mais ce champ est présent, qu'un fichier vidéo ou un autre type ait été importé.Les valeurs acceptées pour cette propriété sont les suivantes : <br>  : le fichier est un type de fichier vidéo connu, comme un fichier .mp4. <br>  fileDetails. <br>  : le fichier est un fichier d'archive, comme une archive .zip. <br>  : le fichier est un type de fichier audio connu, par exemple un fichier .mp3.archive <br>  audio <br>  : le fichier est un document ou un fichier texte, par exemple un document MS Word. <br>  document <br>  : le fichier est un fichier image, par exemple une image .jpeg. <br>  image <br>  other <br>  : le fichier n'est pas de type vidéo. <br>  project <br>  : fichier de projet vidéo (un projet Microsoft Windows Movie Maker, par exemple) qui ne contient pas de données vidéo réelles. <br>  video <br>   <br>|
|container|string <br>  Format du conteneur du fichier vidéo importé. <br>   <br>  fileDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|videoStreams[]|list <br>  Liste des flux vidéo contenus dans le fichier vidéo mis en ligne. Chaque élément de la liste contient des métadonnées détaillées relatives à un flux vidéo. <br>   <br>  fileDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|widthPixels|unsigned integer <br>  Largeur du contenu vidéo encodé en pixels. Vous pouvez calculer le format d'encodage de la vidéo comme suit :  / . <br>  width_pixels <br>  fileDetails.videoStreams[]. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>  height_pixels <br>   <br>   <br>   <br>   <br>   <br>|
|heightPixels|unsigned integer <br>  Hauteur du contenu vidéo encodé en pixels. <br>   <br>  fileDetails.videoStreams[]. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|frameRateFps|double <br>  Fréquence d'images du flux vidéo, exprimée en frames par seconde. <br>   <br>  fileDetails.videoStreams[]. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|aspectRatio|double <br>  Format d'affichage du contenu vidéo, qui spécifie le format dans lequel la vidéo doit être affichée. <br>   <br>  fileDetails.videoStreams[]. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|codec|string <br>  Codec vidéo utilisé par le flux. <br>   <br>  fileDetails.videoStreams[]. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|bitrateBps|unsigned long <br>  Débit du flux vidéo, exprimé en bits par seconde. <br>   <br>  fileDetails.videoStreams[]. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|rotation|string <br>  Quantité nécessaire à YouTube pour faire pivoter le contenu source d'origine afin d'afficher correctement la vidéo.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  fileDetails.videoStreams[]. <br>  : la vidéo doit être pivotée de 90 degrés dans le sens des aiguilles d'une montre. <br>  : la vidéo doit faire l'objet d'une rotation de 90 degrés dans le sens inverse des aiguilles d'une montre.clockwise <br>  counterClockwise <br>  : la vidéo n'a pas besoin d'être pivotée. <br>  none <br>  : la rotation de la vidéo doit être effectuée d'une autre manière. <br>  other <br>  upsideDown <br>  : la vidéo doit être retournée. <br>   <br>   <br>   <br>   <br>|
|vendor|string <br>  Valeur qui identifie de manière unique un fournisseur vidéo. Généralement, la valeur est un code fournisseur à quatre lettres. <br>   <br>  fileDetails.videoStreams[]. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|audioStreams[]|list <br>  Liste des flux audio contenus dans le fichier vidéo importé. Chaque élément de la liste contient des métadonnées détaillées sur un flux audio. <br>   <br>  fileDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|channelCount|unsigned integer <br>  Nombre de canaux audio contenus dans le flux. <br>   <br>  fileDetails.audioStreams[]. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|codec|string <br>  Codec audio utilisé par le flux. <br>   <br>  fileDetails.audioStreams[]. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|bitrateBps|unsigned long <br>  Débit du flux audio, en bits par seconde. <br>   <br>  fileDetails.audioStreams[]. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|vendor|string <br>  Valeur qui identifie de manière unique un fournisseur vidéo. Généralement, la valeur est un code fournisseur à quatre lettres. <br>   <br>  fileDetails.audioStreams[]. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|durationMs|unsigned long <br>  Durée de la vidéo mise en ligne en millisecondes. <br>   <br>  fileDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|bitrateBps|unsigned long <br>  Débit combiné (vidéo et audio) du fichier vidéo mis en ligne, en bits par seconde. <br>   <br>  fileDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|creationTime|string <br>  Date et heure de création du fichier vidéo mis en ligne. La valeur est spécifiée au format . Actuellement, les formats ISO 8601 suivants sont acceptés: <br>   <br>  fileDetails. <br>  Date uniquement: <br>  Temps naïf:YYYY-MM-DD <br>  YYYY-MM-DDTHH:MM:SS <br>  Heure avec le fuseau horaire: <br>  YYYY-MM-DDTHH:MM:SS+HH:MM <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|processingDetails|object <br>  L'objet  encapsule les informations sur la progression de YouTube dans le traitement du fichier vidéo importé. Les propriétés de l'objet indiquent l'état actuel du traitement et une estimation du temps restant avant que YouTube n'ait fini de traiter la vidéo. Elle indique également si différents types de données ou de contenus, tels que des détails de fichier ou des vignettes, sont disponibles pour la vidéo. L'objet  est conçu pour être interrogé afin que la vidéo mise en ligne puisse suivre la progression du traitement du fichier vidéo mis en ligne par YouTube. Seul le propriétaire de la vidéo peut récupérer ces données. <br>  processingDetails <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>  processingProgress <br>   <br>   <br>   <br>   <br>   <br>|
|processingStatus|string <br>  État de traitement de la vidéo. Cette valeur indique si YouTube a pu traiter la vidéo ou si elle est toujours en cours de traitement.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  processingDetails. <br>  : échec du traitement de la vidéo. Consultez "ProcessFailureReason". <br>  : la vidéo est en cours de traitement. Consultez "ProcessProgress".failed <br>  processing <br>  : la vidéo a bien été traitée. <br>  succeeded <br>  : les informations sur le traitement ne sont plus disponibles. <br>  terminated <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|processingProgress|object <br>  L'objet  contient des informations sur la progression de YouTube dans le traitement de la vidéo. Ces valeurs ne sont pertinentes que si l'état de traitement de la vidéo est . <br>  processingProgress <br>  processingDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>  processing <br>   <br>   <br>   <br>   <br>   <br>|
|partsTotal|unsigned long <br>  Estimation du nombre total de parties à traiter pour la vidéo. Ce nombre peut être mis à jour avec des estimations plus précises pendant que YouTube traite la vidéo. <br>   <br>  processingDetails.processingProgress. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|partsProcessed|unsigned long <br>  Nombre de parties de la vidéo déjà traitées par YouTube. Vous pouvez estimer le pourcentage de la vidéo déjà traité par YouTube en calculant ce qui suit:  Notez que l'estimation du nombre de parties pouvant augmenter sans augmentation correspondante du nombre de parties déjà traitées, il est possible que la progression calculée diminue périodiquement pendant que YouTube traite une vidéo. <br>  100 * parts_processed / parts_total <br>  processingDetails.processingProgress. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|timeLeftMs|unsigned long <br>  Estimation du temps (en millisecondes) nécessaire au traitement de la vidéo par YouTube. <br>   <br>  processingDetails.processingProgress. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|processingFailureReason|string <br>  Motif pour lequel YouTube n'a pas pu traiter la vidéo. Cette propriété n'aura une valeur que si celle de la propriété  est .Les valeurs valides pour cette propriété sont les suivantes : <br>  processingStatus <br>  processingDetails. <br>  : un autre élément de traitement a échoué. <br>  : impossible d'envoyer la vidéo aux streamers.other <br>  streamingFailed <br>  : échec du transcodage du contenu. <br>  transcodeFailed <br>  : échec de l'envoi du fichier <br>  uploadFailed <br>  failed <br>   <br>   <br>   <br>   <br>   <br>|
|fileDetailsAvailability|string <br>  Cette valeur indique si les détails du fichier sont disponibles pour la vidéo mise en ligne. Vous pouvez récupérer les détails d'un fichier vidéo en demandant la partie  dans votre requête . <br>  fileDetails <br>  processingDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>  videos.list() <br>   <br>   <br>   <br>   <br>   <br>|
|processingIssuesAvailability|string <br>  Cette valeur indique si le moteur de traitement vidéo a généré des suggestions susceptibles d'améliorer la capacité de YouTube à traiter la vidéo, des avertissements qui expliquent les problèmes de traitement des vidéos ou des erreurs qui entravent les problèmes de traitement des vidéos. Vous pouvez récupérer ces suggestions en demandant la partie  dans votre requête . <br>  suggestions <br>  processingDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>  videos.list() <br>   <br>   <br>   <br>   <br>   <br>|
|tagSuggestionsAvailability|string <br>  Cette valeur indique si des suggestions de mots clés (tags) sont disponibles pour la vidéo. Vous pouvez ajouter des tags aux métadonnées d'une vidéo afin que les autres utilisateurs la trouvent plus facilement. Vous pouvez récupérer ces suggestions en demandant la partie  dans votre requête . <br>  suggestions <br>  processingDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>  videos.list() <br>   <br>   <br>   <br>   <br>   <br>|
|editorSuggestionsAvailability|string <br>  Cette valeur indique si des suggestions de retouche vidéo, qui peuvent améliorer la qualité vidéo ou l'expérience de lecture, sont disponibles pour la vidéo. Vous pouvez récupérer ces suggestions en demandant la partie  dans votre requête . <br>  suggestions <br>  processingDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>  videos.list() <br>   <br>   <br>   <br>   <br>   <br>|
|thumbnailsAvailability|string <br>  Cette valeur indique si des vignettes ont été générées pour la vidéo. <br>   <br>  processingDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|suggestions|object <br>  L'objet  encapsule les suggestions qui identifient les possibilités d'amélioration de la qualité vidéo ou des métadonnées de la vidéo mise en ligne. Seul le propriétaire de la vidéo peut récupérer ces données.  L'objet  n'est renvoyé que si la propriété  ou  a la valeur . <br>  suggestions <br>   <br>  processingDetails.editorSuggestionsAvailability <br>  availableprocessingDetails.tagSuggestionsAvailability <br>   <br>   <br>   <br>   <br>   <br>  suggestions <br>   <br>   <br>   <br>   <br>   <br>|
|processingErrors[]|list <br>  Liste d'erreurs qui empêcheront YouTube de traiter la vidéo mise en ligne. Ces erreurs indiquent que, quel que soit l' actuel de la vidéo, cet état sera presque certainement .Les valeurs valides pour cette propriété sont les suivantes : <br>  failed <br>  suggestions. <br>  : fichier d'archive (une archive ZIP, par exemple). <br>  : fichier ne contenant que du son (par exemple, un fichier MP3)archiveFile <br>  audioFile <br>  : document ou fichier texte (par exemple, document MS Word). <br>  docFile <br>  : fichier image (par exemple, une image JPEG). <br>  imageFile <br>  notAVideoFile <br>  : autre fichier autre qu'une vidéo. <br>  projectFile <br>  : fichier de projet de film (par exemple, projet Microsoft Windows Movie Maker). <br>   <br>   <br>|
|processingWarnings[]|list <br>  Liste des raisons pour lesquelles YouTube peut avoir des difficultés à transcoder la vidéo mise en ligne ou générer des erreurs de transcodage. Ces avertissements sont générés avant que YouTube ne traite réellement le fichier vidéo mis en ligne. De plus, ils identifient des problèmes qui n'indiquent pas nécessairement que le traitement de la vidéo échouera, mais qui pourraient tout de même être à l'origine de problèmes de synchronisation, d'artefacts vidéo ou d'absence de piste audio.Les valeurs acceptées pour cette propriété sont les suivantes : <br>  : codec vidéo non reconnu, le transcodage risque d'échouer. <br>  suggestions. <br>  : les listes de modifications ne sont pas acceptées pour l'instant. <br>  : résolutions de conteneur et de flux en conflit.hasEditlist <br>  inconsistentResolution <br>  : codec audio connu pour causer des problèmes a été utilisé. <br>  problematicAudioCodec <br>  : codec vidéo connu pour causer des problèmes a été utilisé. <br>  problematicVideoCodec <br>  unknownAudioCodec <br>  : codec audio non reconnu, le transcodage risque d'échouer. <br>  unknownContainer <br>  : format de fichier non reconnu. Le transcodage risque d'échouer. <br>  unknownVideoCodec <br>   <br>|
|processingHints[]|list <br>  Liste de suggestions susceptibles d'améliorer la capacité de YouTube à traiter la vidéo.Les valeurs valides pour cette propriété sont les suivantes : <br>   <br>  suggestions. <br>  : le fichier MP4 ne peut pas être diffusé en streaming, ce qui ralentit le traitement. <br>  : il existe probablement une version de meilleure qualité de la vidéo.nonStreamableMov <br>  sendBestQualityVideo <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|tagSuggestions[]|list <br>  Liste de tags de mots clés pouvant être ajoutés aux métadonnées de la vidéo pour augmenter la probabilité que les utilisateurs la trouvent lorsqu'ils effectuent une recherche ou naviguent sur YouTube. <br>   <br>  suggestions. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|tag|string <br>  Balise de mot clé suggérée pour la vidéo. <br>   <br>  suggestions.tagSuggestions[]. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|categoryRestricts[]|list <br>  Ensemble de catégories de vidéos pour lesquelles le tag est pertinent. Vous pouvez utiliser ces informations pour afficher des suggestions de tags adaptées à la catégorie que l'utilisateur ayant mis en ligne la vidéo associe à celle-ci. Par défaut, les suggestions de tags sont pertinentes pour toutes les catégories si aucune restriction n'a été définie pour le mot clé. <br>   <br>  suggestions.tagSuggestions[]. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|editorSuggestions[]|list <br>  Liste d'opérations de montage vidéo susceptibles d'améliorer la qualité vidéo ou l'expérience de lecture de la vidéo mise en ligne.Les valeurs acceptées pour cette propriété sont les suivantes : <br>   <br>  suggestions. <br>  : la piste audio semble silencieuse et pourrait être remplacée par une piste de meilleure qualité. <br>  : les niveaux de luminosité de l'image semblent incorrects et peuvent être corrigés.audioQuietAudioSwap <br>  videoAutoLevels <br>  : les marges détectées autour de la photo peuvent être recadrées. <br>  videoCrop <br>  : la vidéo semble tremblante et pourrait être stabilisée. <br>  videoStabilize <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|liveStreamingDetails|object <br>  L'objet  contient des métadonnées sur une diffusion vidéo en direct. L'objet ne sera présent dans une ressource  que s'il s'agit d'une diffusion en direct à venir, en direct ou terminée. <br>  liveStreamingDetails <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>  video <br>   <br>   <br>   <br>   <br>   <br>|
|actualStartTime|datetime <br>  Heure à laquelle la diffusion a réellement commencé. La valeur est spécifiée au format . Cette valeur ne sera pas disponible avant le début de la diffusion. <br>   <br>  liveStreamingDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|actualEndTime|datetime <br>  Heure à laquelle la diffusion s'est terminée. La valeur est spécifiée au format . Cette valeur ne sera disponible qu'une fois la diffusion terminée. <br>   <br>  liveStreamingDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|scheduledStartTime|datetime <br>  Heure de début prévue de la diffusion. La valeur est spécifiée au format . <br>   <br>  liveStreamingDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|scheduledEndTime|datetime <br>  Heure de fin prévue de la diffusion. La valeur est spécifiée au format . Si la valeur est vide ou si la propriété n'est pas présente, la diffusion est programmée pour se poursuivre indéfiniment. <br>   <br>  liveStreamingDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|concurrentViewers|unsigned long <br>  Nombre de spectateurs qui regardent actuellement la diffusion. La propriété et sa valeur sont présentes si la diffusion a des spectateurs actuels et si le propriétaire de la diffusion n'a pas masqué le nombre de vues de la vidéo. Sachez que YouTube arrête de suivre le nombre de spectateurs simultanés pour une diffusion à la fin de celle-ci. Ainsi, cette propriété n'identifie pas le nombre de spectateurs qui regardent la vidéo archivée d'une diffusion en direct déjà terminée. <br>   <br>  liveStreamingDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|activeLiveChatId|string <br>  ID du chat en direct actuellement actif joint à cette vidéo. Ce champ n'est renseigné que si la vidéo est une diffusion en direct qui inclut un chat en direct. Ce champ sera supprimé une fois la diffusion terminée et le chat en direct fermé. Pour les diffusions persistantes, l'ID du chat en direct ne sera plus associé à cette vidéo, mais à la nouvelle vidéo affichée sur la page persistante. <br>   <br>  liveStreamingDetails. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|localizations|object <br>  L'objet  contient des traductions des métadonnées de la vidéo. <br>  localizations <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|(key)|object <br>  Langue du texte localisé associé à la clé-valeur. Cette valeur est une chaîne contenant un code de langue . <br>   <br>  localizations. <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|title|string <br>  Titre localisé de la vidéo. <br>   <br>  localizations.(key). <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|
|description|string <br>  Description localisée de la vidéo. <br>   <br>  localizations.(key). <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>|