# Analyse des vues :

1er vue - liste des chaines de youtubeurs avec leurs nbr abonnées et leurs vues et nbr vidéos (Totaux de stats)
2ème vue - pour chaque chaine de youtubeur, affichage en fonction de la date de création et d'aujourd'hui avec les dates de publication des vidéos -> (dates + count + thèmes)

## 1ère vue:
1er vue - liste des chaines de youtubeurs avec leurs nbr abonnées et leurs vues et nbr vidéos (Totaux de stats)  
[Visualisation I - 1ère Vue](Analyse/visualisation_first_vue.md)  

## 2ème vue:
2ème vue - pour chaque chaine de youtubeur, affichage en fonction de la date de création et d'aujourd'hui avec les dates de publication des vidéos -> (dates + count + thèmes)  
[Visualisation II - 2ème Vue](Analyse/visualisation_second_vue.md)  

## Bilan:
Conclusion  
[Visualisation III - Bilan](Analyse/visualisation_bilan.md)  