# Extraction  
---

## Requète http pour API Youtube Data  

### Modèle des requètes http:
Informations des liens http:

#### Search:  

**Description:** Liste de chaine  
**URL type:** https://youtube.googleapis.com/youtube/v3/search?part=snippet&regionCode=FR&key=[YOUR_API_KEY]

#### Channel:

**Description:** Data des chaines  
**ID exemple:** UC4ii4_aeS8iOFzsHuhJTq2w  
**URL type:** https://youtube.googleapis.com/youtube/v3/channels?part=auditDetails&part=brandingSettings
&part=contentDetails&part=contentOwnerDetails&part=id&part=localizations&part=snippet
&part=statistics&part=status&part=topicDetails&id=GkmIjfaBh78&key=[YOUR_API_KEY]

#### Playlist:

**Description:** Liste de vidéo d'une chaine  
**ID exemple:** UC4ii4_aeS8iOFzsHuhJTq2w  
**URL type:** https://youtube.googleapis.com/youtube/v3/playlists?part=snippet&channelId=UC4ii4_aeS8iOFzsHuhJTq2w
&key=[YOUR_API_KEY]

#### Vidéo:

**Description:** Data des vidéos  
**ID exemple:** PL_PIQFriATiNkSc5kMbJTllZBp3e8NDXP  
**URL type:** https://youtube.googleapis.com/youtube/v3/channels?part=contentDetails&part=id&part=snippet
&id=@StargateCommunityFR&key=[YOUR_API_KEY]

---

### Modèle des sources:
Informations du JSON modèle:

#### Search
Apposer un statut (à conserver ou jeter ou à vérifier)  
[Extraction I - Search](Analyse/extraction_search.md)  

#### Channel:
Apposer un statut (à conserver ou jeter ou à vérifier)  
[Extraction II - Channel](Analyse/extraction_channel.md)  

#### Vidéo:
Apposer un statut (à conserver ou jeter ou à vérifier)  
[Extraction III - Video](Analyse/extraction_video.md)  

#### Playlist:
Apposer un statut (à conserver ou jeter ou à vérifier)  
[Extraction IV - playlist](Analyse/extraction_playlist.md)

#### PlaylistItem:
Apposer un statut (à conserver ou jeter ou à vérifier)  
[Extraction V - playlistItem](Analyse/extraction_playlistItem.md)

#### Bilan:
Conclusion sur les dimensions à extraire  
[Extraction VI - Bilan](Analyse/extraction_bilan.md)  


## Requète http pour API Youtube Analytiques  

### view
Apposer un statut (à conserver ou jeter ou à vérifier)  
[Extraction I - Search](Analyse/extraction_search.md)  