# Ici les notes diverses du projet:
Notes diverses rescencer au fil du projet

# Notes:


## Vues -> Répartion par 1M d'abonné et par top de millions d'abonnées "max"

Faire des stats simples à partir de fonction (min, max, moyenne) sur les graphiques:
    Afin d'obtenir les plages de fontionnement des youtubeurs (vues, nb de vidéo, abonnée par période)

Types de graphique par youtubeur:
    Les vues sur une vidéo:
        - Nb de vue par vidéo + (min, max, moyenne)
        - Nb de vue par youtubeur + (min, max, moyenne)
    Les nb de vidéo:
        - Nb de vue par youtubeur + (min, max, moyenne)
    Les abonnée par période (à définir sur ce qui est possible avec l'API - Extract)
        - Nb de vue par youtubeur + (min, max, moyenne)

Faire une stats complexes à partir de courbes "tendance" (à la baisse, à la hausse) sur les graphiques:
    Afin d'obtenir les "tendances" globales par répartition, pour analyser leurs caractéristiques

Types de graphique par pays:
    Les vues sur une vidéo:
        - Nb de vue par vidéo + (min, max, moyenne)
        - Nb de vue par pays + (min, max, moyenne)
    Les nb de vidéo:
        - Nb de vue par pays + (min, max, moyenne)
    Les abonnée par période (à définir sur ce qui est possible avec l'API - Extract)
        - Nb de vue par pays + (min, max, moyenne)

Optionnel: regrouper ces "tendances" (à la baisse, à la hausse) dans la BDD ou en direct (vues):
    Afin vérifier si une dimension (vues, nb de vidéo, abonnée par période) influe sur le nombre d'abonnée Total du youtubeur.